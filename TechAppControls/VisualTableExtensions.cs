﻿using System;
using System.Collections.Generic;
using System.Linq;
using Correlator;
using Correlator.CalibrationCorrection;

namespace TechAppControls
{
    public static class VisualTableExtensions
    {
        private static IVisualTable TryLoadTable<T>(Func<string, T> constructor, string filename) where T : IVisualTable
        {
            try
            {
                return constructor(filename);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static readonly List<Func<string, IVisualTable>> _tableConstructors = new List<Func<string, IVisualTable>>
        {
            filename => new RadioPathTable(filename),
            filename => new CorrectionTable(filename, Config.Instance.BandSettings.BandCount).ToVisualTable(),
            filename => new TheoreticalTable(filename).ToVisualTable(),
            filename => new ExternalCalibrationVisualTable(filename),
        };

        public static IVisualTable LoadTable(string filename)
        {
            return _tableConstructors
                .Select(constructor => TryLoadTable(constructor, filename))
                .FirstOrDefault(table => table != null);
        }
    }
}
