﻿namespace TechAppControls
{
    partial class CalibrationDifferenceControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ButtonsPanel = new System.Windows.Forms.Panel();
            this.GraphModeButton = new System.Windows.Forms.Button();
            this.LoadTheoreticalTable = new System.Windows.Forms.Button();
            this.AngleLabel = new System.Windows.Forms.Label();
            this.AngleTextBox = new System.Windows.Forms.TextBox();
            this.PhaseLabel10 = new System.Windows.Forms.Label();
            this.PhaseLabel9 = new System.Windows.Forms.Label();
            this.PhaseLabel8 = new System.Windows.Forms.Label();
            this.PhaseLabel7 = new System.Windows.Forms.Label();
            this.PhaseLabel6 = new System.Windows.Forms.Label();
            this.PhaseLabel5 = new System.Windows.Forms.Label();
            this.PhaseLabel4 = new System.Windows.Forms.Label();
            this.PhaseLabel3 = new System.Windows.Forms.Label();
            this.PhaseLabel2 = new System.Windows.Forms.Label();
            this.PhaseLabel1 = new System.Windows.Forms.Label();
            this.OpenCalibrationButton1 = new System.Windows.Forms.Button();
            this.OpenCalibrationButton2 = new System.Windows.Forms.Button();
            this.LeftPanel = new System.Windows.Forms.Panel();
            this.TwoGraphPanelMode = new System.Windows.Forms.TableLayoutPanel();
            this.OneGraphModePanel = new System.Windows.Forms.Panel();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.waveformPlot1 = new NationalInstruments.UI.WaveformPlot();
            this.xAxis1 = new NationalInstruments.UI.XAxis();
            this.yAxis1 = new NationalInstruments.UI.YAxis();
            this.RadiusLabel1 = new System.Windows.Forms.Label();
            this.RadiusTextBox1 = new System.Windows.Forms.TextBox();
            this.RadiusLabel2 = new System.Windows.Forms.Label();
            this.RadiusTextBox2 = new System.Windows.Forms.TextBox();
            this.RadiusLabel3 = new System.Windows.Forms.Label();
            this.RadiusTextBox3 = new System.Windows.Forms.TextBox();
            this.LedGroup = new TechAppControls.LedGroup();
            this._techAppGraphControl2 = new TechAppControls.TechAppGraphControl();
            this._techAppGraphControl1 = new TechAppControls.TechAppGraphControl();
            this._differencesTechAppGraphControl = new TechAppControls.TechAppGraphControl();
            this.MainTableLayoutPanel.SuspendLayout();
            this.ButtonsPanel.SuspendLayout();
            this.LeftPanel.SuspendLayout();
            this.TwoGraphPanelMode.SuspendLayout();
            this.OneGraphModePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTableLayoutPanel
            // 
            this.MainTableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainTableLayoutPanel.AutoScroll = true;
            this.MainTableLayoutPanel.ColumnCount = 2;
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MainTableLayoutPanel.Controls.Add(this.ButtonsPanel, 0, 0);
            this.MainTableLayoutPanel.Controls.Add(this.LeftPanel, 1, 0);
            this.MainTableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.MainTableLayoutPanel.Name = "MainTableLayoutPanel";
            this.MainTableLayoutPanel.RowCount = 1;
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MainTableLayoutPanel.Size = new System.Drawing.Size(1049, 583);
            this.MainTableLayoutPanel.TabIndex = 0;
            // 
            // ButtonsPanel
            // 
            this.ButtonsPanel.Controls.Add(this.RadiusLabel3);
            this.ButtonsPanel.Controls.Add(this.RadiusTextBox3);
            this.ButtonsPanel.Controls.Add(this.RadiusLabel2);
            this.ButtonsPanel.Controls.Add(this.RadiusTextBox2);
            this.ButtonsPanel.Controls.Add(this.RadiusLabel1);
            this.ButtonsPanel.Controls.Add(this.RadiusTextBox1);
            this.ButtonsPanel.Controls.Add(this.GraphModeButton);
            this.ButtonsPanel.Controls.Add(this.LoadTheoreticalTable);
            this.ButtonsPanel.Controls.Add(this.AngleLabel);
            this.ButtonsPanel.Controls.Add(this.AngleTextBox);
            this.ButtonsPanel.Controls.Add(this.LedGroup);
            this.ButtonsPanel.Controls.Add(this.PhaseLabel10);
            this.ButtonsPanel.Controls.Add(this.PhaseLabel9);
            this.ButtonsPanel.Controls.Add(this.PhaseLabel8);
            this.ButtonsPanel.Controls.Add(this.PhaseLabel7);
            this.ButtonsPanel.Controls.Add(this.PhaseLabel6);
            this.ButtonsPanel.Controls.Add(this.PhaseLabel5);
            this.ButtonsPanel.Controls.Add(this.PhaseLabel4);
            this.ButtonsPanel.Controls.Add(this.PhaseLabel3);
            this.ButtonsPanel.Controls.Add(this.PhaseLabel2);
            this.ButtonsPanel.Controls.Add(this.PhaseLabel1);
            this.ButtonsPanel.Controls.Add(this.OpenCalibrationButton1);
            this.ButtonsPanel.Controls.Add(this.OpenCalibrationButton2);
            this.ButtonsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ButtonsPanel.Location = new System.Drawing.Point(3, 3);
            this.ButtonsPanel.Name = "ButtonsPanel";
            this.ButtonsPanel.Size = new System.Drawing.Size(94, 577);
            this.ButtonsPanel.TabIndex = 7;
            this.ButtonsPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ButtonsPanel_Paint);
            // 
            // GraphModeButton
            // 
            this.GraphModeButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GraphModeButton.Location = new System.Drawing.Point(38, 0);
            this.GraphModeButton.Margin = new System.Windows.Forms.Padding(0);
            this.GraphModeButton.Name = "GraphModeButton";
            this.GraphModeButton.Size = new System.Drawing.Size(53, 23);
            this.GraphModeButton.TabIndex = 22;
            this.GraphModeButton.Text = "Switch";
            this.GraphModeButton.UseVisualStyleBackColor = true;
            this.GraphModeButton.Click += new System.EventHandler(this.GraphModeButton_Click);
            // 
            // LoadTheoreticalTable
            // 
            this.LoadTheoreticalTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LoadTheoreticalTable.Location = new System.Drawing.Point(38, 497);
            this.LoadTheoreticalTable.Margin = new System.Windows.Forms.Padding(0);
            this.LoadTheoreticalTable.Name = "LoadTheoreticalTable";
            this.LoadTheoreticalTable.Size = new System.Drawing.Size(53, 23);
            this.LoadTheoreticalTable.TabIndex = 21;
            this.LoadTheoreticalTable.Text = "Theory";
            this.LoadTheoreticalTable.UseVisualStyleBackColor = true;
            this.LoadTheoreticalTable.Click += new System.EventHandler(this.LoadTheoreticalTable_Click);
            // 
            // AngleLabel
            // 
            this.AngleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AngleLabel.AutoSize = true;
            this.AngleLabel.Location = new System.Drawing.Point(47, 456);
            this.AngleLabel.Margin = new System.Windows.Forms.Padding(0);
            this.AngleLabel.Name = "AngleLabel";
            this.AngleLabel.Size = new System.Drawing.Size(34, 13);
            this.AngleLabel.TabIndex = 20;
            this.AngleLabel.Text = "Angle";
            this.AngleLabel.Click += new System.EventHandler(this.AngleLabel_Click);
            // 
            // AngleTextBox
            // 
            this.AngleTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AngleTextBox.Location = new System.Drawing.Point(38, 474);
            this.AngleTextBox.Name = "AngleTextBox";
            this.AngleTextBox.Size = new System.Drawing.Size(53, 20);
            this.AngleTextBox.TabIndex = 19;
            this.AngleTextBox.TextChanged += new System.EventHandler(this.AngleTextBox_TextChanged);
            // 
            // PhaseLabel10
            // 
            this.PhaseLabel10.AutoSize = true;
            this.PhaseLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhaseLabel10.Location = new System.Drawing.Point(36, 493);
            this.PhaseLabel10.Name = "PhaseLabel10";
            this.PhaseLabel10.Size = new System.Drawing.Size(0, 20);
            this.PhaseLabel10.TabIndex = 17;
            // 
            // PhaseLabel9
            // 
            this.PhaseLabel9.AutoSize = true;
            this.PhaseLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhaseLabel9.Location = new System.Drawing.Point(36, 441);
            this.PhaseLabel9.Name = "PhaseLabel9";
            this.PhaseLabel9.Size = new System.Drawing.Size(0, 20);
            this.PhaseLabel9.TabIndex = 16;
            // 
            // PhaseLabel8
            // 
            this.PhaseLabel8.AutoSize = true;
            this.PhaseLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhaseLabel8.Location = new System.Drawing.Point(36, 391);
            this.PhaseLabel8.Name = "PhaseLabel8";
            this.PhaseLabel8.Size = new System.Drawing.Size(0, 20);
            this.PhaseLabel8.TabIndex = 15;
            this.PhaseLabel8.Click += new System.EventHandler(this.label5_Click);
            // 
            // PhaseLabel7
            // 
            this.PhaseLabel7.AutoSize = true;
            this.PhaseLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhaseLabel7.Location = new System.Drawing.Point(36, 341);
            this.PhaseLabel7.Name = "PhaseLabel7";
            this.PhaseLabel7.Size = new System.Drawing.Size(0, 20);
            this.PhaseLabel7.TabIndex = 14;
            // 
            // PhaseLabel6
            // 
            this.PhaseLabel6.AutoSize = true;
            this.PhaseLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhaseLabel6.Location = new System.Drawing.Point(36, 291);
            this.PhaseLabel6.Name = "PhaseLabel6";
            this.PhaseLabel6.Size = new System.Drawing.Size(0, 20);
            this.PhaseLabel6.TabIndex = 13;
            // 
            // PhaseLabel5
            // 
            this.PhaseLabel5.AutoSize = true;
            this.PhaseLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhaseLabel5.Location = new System.Drawing.Point(36, 241);
            this.PhaseLabel5.Name = "PhaseLabel5";
            this.PhaseLabel5.Size = new System.Drawing.Size(0, 20);
            this.PhaseLabel5.TabIndex = 12;
            // 
            // PhaseLabel4
            // 
            this.PhaseLabel4.AutoSize = true;
            this.PhaseLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhaseLabel4.Location = new System.Drawing.Point(36, 191);
            this.PhaseLabel4.Name = "PhaseLabel4";
            this.PhaseLabel4.Size = new System.Drawing.Size(0, 20);
            this.PhaseLabel4.TabIndex = 11;
            // 
            // PhaseLabel3
            // 
            this.PhaseLabel3.AutoSize = true;
            this.PhaseLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhaseLabel3.Location = new System.Drawing.Point(36, 141);
            this.PhaseLabel3.Name = "PhaseLabel3";
            this.PhaseLabel3.Size = new System.Drawing.Size(0, 20);
            this.PhaseLabel3.TabIndex = 10;
            // 
            // PhaseLabel2
            // 
            this.PhaseLabel2.AutoSize = true;
            this.PhaseLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhaseLabel2.Location = new System.Drawing.Point(36, 91);
            this.PhaseLabel2.Name = "PhaseLabel2";
            this.PhaseLabel2.Size = new System.Drawing.Size(0, 20);
            this.PhaseLabel2.TabIndex = 9;
            // 
            // PhaseLabel1
            // 
            this.PhaseLabel1.AutoSize = true;
            this.PhaseLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhaseLabel1.Location = new System.Drawing.Point(36, 41);
            this.PhaseLabel1.Name = "PhaseLabel1";
            this.PhaseLabel1.Size = new System.Drawing.Size(0, 20);
            this.PhaseLabel1.TabIndex = 8;
            // 
            // OpenCalibrationButton1
            // 
            this.OpenCalibrationButton1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenCalibrationButton1.Location = new System.Drawing.Point(3, 523);
            this.OpenCalibrationButton1.Name = "OpenCalibrationButton1";
            this.OpenCalibrationButton1.Size = new System.Drawing.Size(88, 23);
            this.OpenCalibrationButton1.TabIndex = 7;
            this.OpenCalibrationButton1.Text = "Open calibration 1";
            this.OpenCalibrationButton1.UseVisualStyleBackColor = true;
            this.OpenCalibrationButton1.Click += new System.EventHandler(this.OpenCalibrationButton1_Click);
            // 
            // OpenCalibrationButton2
            // 
            this.OpenCalibrationButton2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenCalibrationButton2.Location = new System.Drawing.Point(3, 552);
            this.OpenCalibrationButton2.Name = "OpenCalibrationButton2";
            this.OpenCalibrationButton2.Size = new System.Drawing.Size(88, 23);
            this.OpenCalibrationButton2.TabIndex = 6;
            this.OpenCalibrationButton2.Text = "Open calibration 2";
            this.OpenCalibrationButton2.UseVisualStyleBackColor = true;
            this.OpenCalibrationButton2.Click += new System.EventHandler(this.OpenCalibrationButton2_Click);
            // 
            // LeftPanel
            // 
            this.LeftPanel.Controls.Add(this.TwoGraphPanelMode);
            this.LeftPanel.Controls.Add(this.OneGraphModePanel);
            this.LeftPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftPanel.Location = new System.Drawing.Point(100, 0);
            this.LeftPanel.Margin = new System.Windows.Forms.Padding(0);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(949, 583);
            this.LeftPanel.TabIndex = 8;
            // 
            // TwoGraphPanelMode
            // 
            this.TwoGraphPanelMode.ColumnCount = 1;
            this.TwoGraphPanelMode.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TwoGraphPanelMode.Controls.Add(this._techAppGraphControl2, 0, 1);
            this.TwoGraphPanelMode.Controls.Add(this._techAppGraphControl1, 0, 0);
            this.TwoGraphPanelMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TwoGraphPanelMode.Location = new System.Drawing.Point(0, 0);
            this.TwoGraphPanelMode.Margin = new System.Windows.Forms.Padding(0);
            this.TwoGraphPanelMode.Name = "TwoGraphPanelMode";
            this.TwoGraphPanelMode.RowCount = 2;
            this.TwoGraphPanelMode.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TwoGraphPanelMode.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TwoGraphPanelMode.Size = new System.Drawing.Size(949, 583);
            this.TwoGraphPanelMode.TabIndex = 1;
            this.TwoGraphPanelMode.Visible = false;
            // 
            // OneGraphModePanel
            // 
            this.OneGraphModePanel.Controls.Add(this._differencesTechAppGraphControl);
            this.OneGraphModePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OneGraphModePanel.Location = new System.Drawing.Point(0, 0);
            this.OneGraphModePanel.Margin = new System.Windows.Forms.Padding(0);
            this.OneGraphModePanel.Name = "OneGraphModePanel";
            this.OneGraphModePanel.Size = new System.Drawing.Size(949, 583);
            this.OneGraphModePanel.TabIndex = 0;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // waveformPlot1
            // 
            this.waveformPlot1.XAxis = this.xAxis1;
            this.waveformPlot1.YAxis = this.yAxis1;
            // 
            // RadiusLabel1
            // 
            this.RadiusLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadiusLabel1.AutoSize = true;
            this.RadiusLabel1.Location = new System.Drawing.Point(39, 44);
            this.RadiusLabel1.Margin = new System.Windows.Forms.Padding(0);
            this.RadiusLabel1.Name = "RadiusLabel1";
            this.RadiusLabel1.Size = new System.Drawing.Size(49, 13);
            this.RadiusLabel1.TabIndex = 24;
            this.RadiusLabel1.Text = "Radius 1";
            // 
            // RadiusTextBox1
            // 
            this.RadiusTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadiusTextBox1.Location = new System.Drawing.Point(38, 60);
            this.RadiusTextBox1.Name = "RadiusTextBox1";
            this.RadiusTextBox1.Size = new System.Drawing.Size(53, 20);
            this.RadiusTextBox1.TabIndex = 23;
            this.RadiusTextBox1.TextChanged += new System.EventHandler(this.RadiusTextBox1_TextChanged);
            // 
            // RadiusLabel2
            // 
            this.RadiusLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadiusLabel2.AutoSize = true;
            this.RadiusLabel2.Location = new System.Drawing.Point(39, 88);
            this.RadiusLabel2.Margin = new System.Windows.Forms.Padding(0);
            this.RadiusLabel2.Name = "RadiusLabel2";
            this.RadiusLabel2.Size = new System.Drawing.Size(49, 13);
            this.RadiusLabel2.TabIndex = 26;
            this.RadiusLabel2.Text = "Radius 2";
            // 
            // RadiusTextBox2
            // 
            this.RadiusTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadiusTextBox2.Location = new System.Drawing.Point(38, 104);
            this.RadiusTextBox2.Name = "RadiusTextBox2";
            this.RadiusTextBox2.Size = new System.Drawing.Size(53, 20);
            this.RadiusTextBox2.TabIndex = 25;
            this.RadiusTextBox2.TextChanged += new System.EventHandler(this.RadiusTextBox2_TextChanged);
            // 
            // RadiusLabel3
            // 
            this.RadiusLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadiusLabel3.AutoSize = true;
            this.RadiusLabel3.Location = new System.Drawing.Point(39, 136);
            this.RadiusLabel3.Margin = new System.Windows.Forms.Padding(0);
            this.RadiusLabel3.Name = "RadiusLabel3";
            this.RadiusLabel3.Size = new System.Drawing.Size(49, 13);
            this.RadiusLabel3.TabIndex = 28;
            this.RadiusLabel3.Text = "Radius 3";
            // 
            // RadiusTextBox3
            // 
            this.RadiusTextBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RadiusTextBox3.Location = new System.Drawing.Point(38, 152);
            this.RadiusTextBox3.Name = "RadiusTextBox3";
            this.RadiusTextBox3.Size = new System.Drawing.Size(53, 20);
            this.RadiusTextBox3.TabIndex = 27;
            this.RadiusTextBox3.TextChanged += new System.EventHandler(this.RadiusTextBox3_TextChanged);
            // 
            // LedGroup
            // 
            this.LedGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.LedGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.LedGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LedGroup.ColorGenerator = null;
            this.LedGroup.Location = new System.Drawing.Point(3, 0);
            this.LedGroup.Margin = new System.Windows.Forms.Padding(0);
            this.LedGroup.MinimumSize = new System.Drawing.Size(32, 2);
            this.LedGroup.Name = "LedGroup";
            this.LedGroup.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.LedGroup.Size = new System.Drawing.Size(32, 520);
            this.LedGroup.TabIndex = 18;
            // 
            // _techAppGraphControl2
            // 
            this._techAppGraphControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._techAppGraphControl2.DataRange = new NationalInstruments.UI.Range(25D, 3025D);
            this._techAppGraphControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._techAppGraphControl2.Location = new System.Drawing.Point(3, 294);
            this._techAppGraphControl2.Name = "_techAppGraphControl2";
            this._techAppGraphControl2.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this._techAppGraphControl2.SelectionEnabled = true;
            this._techAppGraphControl2.SelectionWidth = 1000F;
            this._techAppGraphControl2.Size = new System.Drawing.Size(943, 286);
            this._techAppGraphControl2.StickDataToVisibleRange = false;
            this._techAppGraphControl2.TabIndex = 11;
            this._techAppGraphControl2.VisibleRange = new NationalInstruments.UI.Range(25D, 3025D);
            // 
            // _techAppGraphControl1
            // 
            this._techAppGraphControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._techAppGraphControl1.DataRange = new NationalInstruments.UI.Range(25D, 3025D);
            this._techAppGraphControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._techAppGraphControl1.Location = new System.Drawing.Point(3, 3);
            this._techAppGraphControl1.Name = "_techAppGraphControl1";
            this._techAppGraphControl1.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this._techAppGraphControl1.SelectionEnabled = true;
            this._techAppGraphControl1.SelectionWidth = 1000F;
            this._techAppGraphControl1.Size = new System.Drawing.Size(943, 285);
            this._techAppGraphControl1.StickDataToVisibleRange = false;
            this._techAppGraphControl1.TabIndex = 10;
            this._techAppGraphControl1.VisibleRange = new NationalInstruments.UI.Range(25D, 3025D);
            // 
            // _differencesTechAppGraphControl
            // 
            this._differencesTechAppGraphControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._differencesTechAppGraphControl.DataRange = new NationalInstruments.UI.Range(25D, 3025D);
            this._differencesTechAppGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._differencesTechAppGraphControl.Location = new System.Drawing.Point(0, 0);
            this._differencesTechAppGraphControl.Name = "_differencesTechAppGraphControl";
            this._differencesTechAppGraphControl.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this._differencesTechAppGraphControl.SelectionEnabled = true;
            this._differencesTechAppGraphControl.SelectionWidth = 1000F;
            this._differencesTechAppGraphControl.Size = new System.Drawing.Size(949, 583);
            this._differencesTechAppGraphControl.StickDataToVisibleRange = false;
            this._differencesTechAppGraphControl.TabIndex = 9;
            this._differencesTechAppGraphControl.VisibleRange = new NationalInstruments.UI.Range(25D, 3025D);
            // 
            // CalibrationDifferenceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(500, 500);
            this.Controls.Add(this.MainTableLayoutPanel);
            this.Name = "CalibrationDifferenceControl";
            this.Size = new System.Drawing.Size(1055, 589);
            this.Load += new System.EventHandler(this.CalibrationDifferenceControl_Load);
            this.MainTableLayoutPanel.ResumeLayout(false);
            this.ButtonsPanel.ResumeLayout(false);
            this.ButtonsPanel.PerformLayout();
            this.LeftPanel.ResumeLayout(false);
            this.TwoGraphPanelMode.ResumeLayout(false);
            this.OneGraphModePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MainTableLayoutPanel;
        private System.Windows.Forms.Panel ButtonsPanel;
        private System.Windows.Forms.Button OpenCalibrationButton1;
        private System.Windows.Forms.Button OpenCalibrationButton2;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label PhaseLabel10;
        private System.Windows.Forms.Label PhaseLabel9;
        private System.Windows.Forms.Label PhaseLabel8;
        private System.Windows.Forms.Label PhaseLabel7;
        private System.Windows.Forms.Label PhaseLabel6;
        private System.Windows.Forms.Label PhaseLabel5;
        private System.Windows.Forms.Label PhaseLabel4;
        private System.Windows.Forms.Label PhaseLabel3;
        private System.Windows.Forms.Label PhaseLabel2;
        private System.Windows.Forms.Label PhaseLabel1;
        private NationalInstruments.UI.WaveformPlot waveformPlot1;
        private NationalInstruments.UI.XAxis xAxis1;
        private NationalInstruments.UI.YAxis yAxis1;
        private LedGroup LedGroup;
        private System.Windows.Forms.Label AngleLabel;
        private System.Windows.Forms.TextBox AngleTextBox;
        private System.Windows.Forms.Button LoadTheoreticalTable;
        private System.Windows.Forms.Button GraphModeButton;
        private System.Windows.Forms.Panel LeftPanel;
        private System.Windows.Forms.Panel OneGraphModePanel;
        private TechAppGraphControl _differencesTechAppGraphControl;
        private System.Windows.Forms.TableLayoutPanel TwoGraphPanelMode;
        private TechAppGraphControl _techAppGraphControl2;
        private TechAppGraphControl _techAppGraphControl1;
        private System.Windows.Forms.Label RadiusLabel3;
        private System.Windows.Forms.TextBox RadiusTextBox3;
        private System.Windows.Forms.Label RadiusLabel2;
        private System.Windows.Forms.TextBox RadiusTextBox2;
        private System.Windows.Forms.Label RadiusLabel1;
        private System.Windows.Forms.TextBox RadiusTextBox1;
    }
}
