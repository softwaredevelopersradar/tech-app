﻿namespace TechAppControls
{
    partial class PhaseControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PhaseGauge = new NationalInstruments.UI.WindowsForms.Gauge();
            this.ValueLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.LabelsPanel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.PhaseGauge)).BeginInit();
            this.TableLayoutPanel.SuspendLayout();
            this.LabelsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PhaseGauge
            // 
            this.PhaseGauge.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.PhaseGauge.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PhaseGauge.CaptionVisible = false;
            this.PhaseGauge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PhaseGauge.ForeColor = System.Drawing.SystemColors.Window;
            this.PhaseGauge.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThickNeedle;
            this.PhaseGauge.ImmediateUpdates = true;
            this.PhaseGauge.Location = new System.Drawing.Point(0, 0);
            this.PhaseGauge.Margin = new System.Windows.Forms.Padding(0);
            this.PhaseGauge.Name = "PhaseGauge";
            this.PhaseGauge.PointerColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.PhaseGauge.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.PhaseGauge.ScaleArc = new NationalInstruments.UI.Arc(90F, 360F);
            this.PhaseGauge.Size = new System.Drawing.Size(78, 66);
            this.PhaseGauge.TabIndex = 2;
            // 
            // ValueLabel
            // 
            this.ValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ValueLabel.Location = new System.Drawing.Point(48, 0);
            this.ValueLabel.MaximumSize = new System.Drawing.Size(30, 12);
            this.ValueLabel.MinimumSize = new System.Drawing.Size(30, 12);
            this.ValueLabel.Name = "ValueLabel";
            this.ValueLabel.Size = new System.Drawing.Size(30, 12);
            this.ValueLabel.TabIndex = 3;
            this.ValueLabel.Text = "360";
            this.ValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // NameLabel
            // 
            this.NameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.NameLabel.Location = new System.Drawing.Point(-2, 0);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(60, 13);
            this.NameLabel.TabIndex = 4;
            this.NameLabel.Text = "Name";
            this.NameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TableLayoutPanel
            // 
            this.TableLayoutPanel.ColumnCount = 1;
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel.Controls.Add(this.PhaseGauge, 0, 0);
            this.TableLayoutPanel.Controls.Add(this.LabelsPanel, 0, 1);
            this.TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.TableLayoutPanel.Name = "TableLayoutPanel";
            this.TableLayoutPanel.RowCount = 2;
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel.Size = new System.Drawing.Size(78, 78);
            this.TableLayoutPanel.TabIndex = 5;
            // 
            // LabelsPanel
            // 
            this.LabelsPanel.Controls.Add(this.ValueLabel);
            this.LabelsPanel.Controls.Add(this.NameLabel);
            this.LabelsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelsPanel.Location = new System.Drawing.Point(0, 66);
            this.LabelsPanel.Margin = new System.Windows.Forms.Padding(0);
            this.LabelsPanel.Name = "LabelsPanel";
            this.LabelsPanel.Size = new System.Drawing.Size(78, 12);
            this.LabelsPanel.TabIndex = 3;
            // 
            // PhaseControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.TableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "PhaseControl";
            this.Size = new System.Drawing.Size(78, 78);
            ((System.ComponentModel.ISupportInitialize)(this.PhaseGauge)).EndInit();
            this.TableLayoutPanel.ResumeLayout(false);
            this.LabelsPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label ValueLabel;
        private System.Windows.Forms.Label NameLabel;
        internal NationalInstruments.UI.WindowsForms.Gauge PhaseGauge;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel;
        private System.Windows.Forms.Panel LabelsPanel;
    }
}
