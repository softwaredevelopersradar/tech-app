﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using Correlator;
using DspDataModel;
using NationalInstruments.UI;
using Phases;
using Protocols;
using RadioTables;
using Settings;

namespace TechAppControls
{
    public partial class CorrectionTableControl : UserControl
    {
        private bool _axisFlag = true;
        private int _frequencykHz = 25000;
        private int _angle;
        private IVisualTable _correctionTable;

        private DspClient.DspClient _client;

        private RadioPathTable _radioPathTable;

        public CorrectionTableControl()
        {
            InitializeComponent();

            LedGroup.Initialize(correctionTableGraphControl.Graph.PlotLineColorGenerator, new[]
            {
                "1-2", "1-3", "1-4", "1-5", "2-3", "2-4", "2-5", "3-4", "3-5", "4-5"
            });

            LedGroup.SetValue(false);
            LedGroup.SetEnabled(false);
            LedGroup.LedClickEvent += OnLedClick;

            correctionTableGraphControl.SelectionEnabled = false;
            correctionTableGraphControl.Graph.YAxes[0].Range = new Range(0, 360);
            correctionTableGraphControl.Graph.YAxes[0].Mode = AxisMode.Fixed;

            try
            {
                //_radioPathTable = new RadioPathTable(Config.Instance.RadioPathTableFilename);
            }
            catch (Exception)
            {
                _radioPathTable = new RadioPathTable();
            }
        }

        public void SetUpClient(DspClient.DspClient client)
        {
            this._client = client;
        }

        void OnLedClick(object sender, int index)
        {
            correctionTableGraphControl.Graph.Plots[index].Visible = LedGroup.Leds[index].Value;
        }

        private void OpenCalibrationButton_Click(object sender, EventArgs e)
        {
            var table = OpenCalibration();
            if (table != null)
            {
                OpenCalibrationButton.Text = Path.GetFileName(openFileDialog.FileName);
                ShowTable();
            }
        }

        private IVisualTable OpenCalibration()
        {
            _correctionTable = openFileDialog.ShowDialog() == DialogResult.OK
                ? VisualTableExtensions.LoadTable(openFileDialog.FileName)
                : null;
            if (_correctionTable != null)
            {
                var range = new Range(_correctionTable.MinFrequencyKhz / 1000d, _correctionTable.MaxFrequencyKhz / 1000d);
                correctionTableGraphControl.DataRange = range;
                correctionTableGraphControl.VisibleRange = range;
            }
            return _correctionTable;
        }

        private void GraphModeButton_Click(object sender, EventArgs e)
        {
            if (_axisFlag)
            {
                _axisFlag = false;

                AxisXTextBox.Text = _frequencykHz.ToString();
                AxisXLabel.Text = "Frequency";
                ShowTable();
                return;
            }
            _axisFlag = true;
            AxisXLabel.Text = "Angle";
            AxisXTextBox.Text = _angle.ToString();
            ShowTable();
        }

        private async Task ShowTable()
        {
            if (_axisFlag)
            {
                if (_correctionTable == null)
                {
                    return;
                }
                LedGroup.SetEnabled(true);
                LedGroup.SetValue(true);

                var valuesCount = _correctionTable.ValuesCount;
                var step = (_correctionTable.MaxFrequencyKhz - _correctionTable.MinFrequencyKhz) / (valuesCount - 1);

                var data = new double[10, valuesCount];

                for (var i = 0; i < valuesCount; ++i)
                {
                    var frequency = _correctionTable.MinFrequencyKhz + i * step;
                    var phases = _correctionTable.FrequencyToPhases(frequency);
                    var radioPathPhases = new PhaseArray(new float[]{0,0,0,0,0,0,0,0,0,0});
                    if(checkBoxUseRadioPath.Checked)
                        radioPathPhases = _radioPathTable.GetPhases(frequency);

                    for (var j = 0; j < 10; ++j)
                    {
                        data[j, i] = PhaseMath.NormalizePhase(phases[j] + radioPathPhases.Phases[j]);
                    }
                }
                var range = new Range(_correctionTable.MinFrequencyKhz / 1000, _correctionTable.MaxFrequencyKhz / 1000);
                correctionTableGraphControl.DataRange = range;
                correctionTableGraphControl.VisibleRange = range;
                correctionTableGraphControl.Plot(data);
            }
            else
            {
                if (_correctionTable == null)
                {
                    return;
                }
                Scan[] scans = await GetSpectrum();
                LedGroup.SetEnabled(true);
                LedGroup.SetValue(true);

                int addedDataCount = scans.Length == 5 ? 10 : 0;
                var data = new double[10 + addedDataCount, 360];

                for (var i = 0; i < 360; ++i)
                {
                    _correctionTable.CurrentAngle = i;
                    var phases = _correctionTable.FrequencyToPhases(_frequencykHz);
                    var radioPathPhases = _radioPathTable.GetPhases(_frequencykHz);

                    for (var j = 0; j < 10; ++j)
                    {
                        data[j, i] = PhaseMath.NormalizePhase(phases[j] + radioPathPhases.Phases[j]);
                    }
                }
                var range = new Range(0, 360);
                correctionTableGraphControl.DataRange = range;
                correctionTableGraphControl.VisibleRange = range;
                correctionTableGraphControl.Plot(data);
            }
            
        }

        private void AxisXTextBox_TextChanged(object sender, EventArgs e)
        {
            if (_axisFlag)
            {
                if (_correctionTable == null)
                {
                    return;
                }

                if (!int.TryParse(AxisXTextBox.Text, out _angle))
                {
                    return;
                }
                _angle = PhaseMath.NormalizePhase(_angle);
                _correctionTable.CurrentAngle = _angle;
                ShowTable();
            }
            else
            {
                if (_correctionTable == null)
                {
                    return;
                }

                if (!int.TryParse(AxisXTextBox.Text, out _frequencykHz))
                {
                    return;
                }
                ShowTable();
            }
        }

        private async Task<Scan[]> GetSpectrum()
        {
            if (_client == null)
                return new Scan[0];
            await _client.GetTechAppSpectrum(Utilities.GetBandNumber(_frequencykHz),1); // blank scan to be sure about proper info
            return await _client.GetTechAppSpectrum(Utilities.GetBandNumber(_frequencykHz), 1);
        }
    }
}
