﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TechAppControls
{
    public partial class PhaseBox : UserControl
    {
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                BackColor = IsSelected ? Color.Aqua : Color.Transparent;
            }
        }

        public event EventHandler SelectionEvent;

        public void Select()
        {
            IsSelected = true;
            if (SelectionEvent != null)
            {
                SelectionEvent(this, EventArgs.Empty);
            }
        }

        public void Deselect()
        {
            IsSelected = false;
        }

        public string Number
        {
            get { return NumberLabel.Text; }
            set { NumberLabel.Text = value; }
        }

        private float _phase;
        public float Phase
        {
            get { return _phase; }
            set
            {
                _phase = value;
                PhaseTextBox.Text = _phase.ToString("F0");
            }
        }

        private float _theoryPhase;
        private bool _isSelected;

        public float TheoryPhase
        {
            get { return _theoryPhase; }
            set
            {
                _theoryPhase = value;
                TheoryPhaseTextBox.Text = _theoryPhase.ToString("F0");
            }
        }

        public PhaseBox()
        {
            InitializeComponent();
        }

        private void PhaseBox_Click(object sender, System.EventArgs e)
        {
            Select();
        }
    }
}
