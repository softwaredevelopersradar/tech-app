﻿namespace TechAppControls
{
    partial class PhaseBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PhaseTextBox = new System.Windows.Forms.TextBox();
            this.NumberLabel = new System.Windows.Forms.Label();
            this.TheoryPhaseTextBox = new System.Windows.Forms.TextBox();
            this.PhaseLabel = new System.Windows.Forms.Label();
            this.TheoryPhaseLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PhaseTextBox
            // 
            this.PhaseTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PhaseTextBox.Location = new System.Drawing.Point(34, 27);
            this.PhaseTextBox.Name = "PhaseTextBox";
            this.PhaseTextBox.Size = new System.Drawing.Size(40, 20);
            this.PhaseTextBox.TabIndex = 0;
            this.PhaseTextBox.Text = "350";
            // 
            // NumberLabel
            // 
            this.NumberLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.NumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumberLabel.Location = new System.Drawing.Point(18, 0);
            this.NumberLabel.Name = "NumberLabel";
            this.NumberLabel.Size = new System.Drawing.Size(40, 20);
            this.NumberLabel.TabIndex = 1;
            this.NumberLabel.Text = "5";
            this.NumberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TheoryPhaseTextBox
            // 
            this.TheoryPhaseTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TheoryPhaseTextBox.Location = new System.Drawing.Point(34, 53);
            this.TheoryPhaseTextBox.Name = "TheoryPhaseTextBox";
            this.TheoryPhaseTextBox.Size = new System.Drawing.Size(40, 20);
            this.TheoryPhaseTextBox.TabIndex = 2;
            this.TheoryPhaseTextBox.Text = "20";
            // 
            // PhaseLabel
            // 
            this.PhaseLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PhaseLabel.AutoSize = true;
            this.PhaseLabel.Location = new System.Drawing.Point(3, 30);
            this.PhaseLabel.Name = "PhaseLabel";
            this.PhaseLabel.Size = new System.Drawing.Size(14, 13);
            this.PhaseLabel.TabIndex = 3;
            this.PhaseLabel.Text = "P";
            // 
            // TheoryPhaseLabel
            // 
            this.TheoryPhaseLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TheoryPhaseLabel.AutoSize = true;
            this.TheoryPhaseLabel.Location = new System.Drawing.Point(3, 56);
            this.TheoryPhaseLabel.Name = "TheoryPhaseLabel";
            this.TheoryPhaseLabel.Size = new System.Drawing.Size(24, 13);
            this.TheoryPhaseLabel.TabIndex = 4;
            this.TheoryPhaseLabel.Text = "P-T";
            // 
            // PhaseBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.TheoryPhaseLabel);
            this.Controls.Add(this.PhaseLabel);
            this.Controls.Add(this.TheoryPhaseTextBox);
            this.Controls.Add(this.NumberLabel);
            this.Controls.Add(this.PhaseTextBox);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "PhaseBox";
            this.Size = new System.Drawing.Size(77, 75);
            this.Click += new System.EventHandler(this.PhaseBox_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox PhaseTextBox;
        private System.Windows.Forms.Label NumberLabel;
        private System.Windows.Forms.TextBox TheoryPhaseTextBox;
        private System.Windows.Forms.Label PhaseLabel;
        private System.Windows.Forms.Label TheoryPhaseLabel;
    }
}
