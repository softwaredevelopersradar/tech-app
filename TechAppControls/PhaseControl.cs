﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TechAppControls
{
    public partial class PhaseControl : UserControl
    {
        private float _value;

        public float Value
        {
            get { return _value; }
            set 
            {
                var setRedColor = Math.Abs(value - _value) > 30;
                PhaseGauge.PointerColor = setRedColor ? Color.Red : Color.Black;
                PhaseGauge.Value = 360 - value;

                _value = value;
                ValueLabel.Text = value.ToString("F0");
            }
        }

        public string ControlName
        {
            get { return NameLabel.Text; }
            set { NameLabel.Text = value; }
        }

        public PhaseControl()
        {
            InitializeComponent();
        }
    }
}
