﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Correlator;
//using FastBitmapLib;
using NationalInstruments.UI;
using Phases;
using Settings;

namespace TechAppControls
{
    public partial class CalibrationTabControl : UserControl
    {
        private IVisualTable _table;

        private readonly TextBox[] _phaseTextboxes;

        private float _selectedFrequency;

        public bool IsFrequencyMode { get; private set; }

        private int _selectedAngle;

        public CalibrationTabControl()
        {
            InitializeComponent();
            /*
            if (!Config.IsConfigLoaded())
            {
                return;
            }*/

            IsFrequencyMode = true;

            _phaseTextboxes = new[]
            {
                textBox1, textBox2, textBox3, textBox4, textBox5,
                textBox6, textBox7, textBox8, textBox9, textBox10,
            };

            LedGroup.Initialize(_phasesTechAppGraphControl.Graph.PlotLineColorGenerator,
                new[]
                {
                    "1-2", "1-3", "1-4", "1-5", "2-3", "2-4", "2-5", "3-4", "3-5", "4-5"
                });
            LedGroup.SetValue(false);
            LedGroup.SetEnabled(false);
            LedGroup.LedClickEvent += OnLedClick;

            _phasesTechAppGraphControl.Graph.YAxes[0].Range = new Range(-720, 720);
            _phasesTechAppGraphControl.Graph.YAxes[0].Mode = AxisMode.AutoScaleExact;
            _phasesTechAppGraphControl.SelectionWidth = 0;
            _phasesTechAppGraphControl.SelectionColor = Color.White;
            _phasesTechAppGraphControl.GraphClickEvent += OnTechAppGraphClick;
            _phasesTechAppGraphControl.VisibleRangeChangedEvent += OnPhaseTechAppGraphVisibleRangeChanged;
            _amplitudesTechAppGraphControl.VisibleRangeChangedEvent += OnAmplitudeTechAppGraphVisibleRangeChanged;

            _amplitudesTechAppGraphControl.Graph.YAxes[0].Range = new Range(Constants.ReceiverMinAmplitude, 0);
            _amplitudesTechAppGraphControl.Graph.YAxes[0].Mode = AxisMode.Fixed;

            ModeComboBox.SelectedIndex = 0;
        }

        void OnAmplitudeTechAppGraphVisibleRangeChanged(object sender, EventArgs e)
        {
            if (IsFrequencyMode)
            {
                _phasesTechAppGraphControl.VisibleRange = _amplitudesTechAppGraphControl.VisibleRange;
            }
        }

        void OnPhaseTechAppGraphVisibleRangeChanged(object sender, EventArgs e)
        {
            if (IsFrequencyMode)
            {
                _amplitudesTechAppGraphControl.VisibleRange = _phasesTechAppGraphControl.VisibleRange;
            }
        }

        private void OnTechAppGraphClick(object sender, float frequency)
        {
            if (_table == null)
            {
                return;
            }
            var phases = _table.FrequencyToPhases(frequency);
            var tableFrequency = _table.GetNearestTableFrequency(frequency);
            FrequencyLabel.Text = (tableFrequency / 1000).ToString("F1") + " МГц";
            for (var i = 0; i < phases.Length; ++i)
            {
                _phaseTextboxes[i].Text = phases[i].ToString("F0");
            }
            _selectedFrequency = frequency;
        }

        void OnLedClick(object sender, int index)
        {
            _phasesTechAppGraphControl.Graph.Plots[index].Visible = LedGroup.Leds[index].Value;
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            if (Visible)
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    LoadTable(openFileDialog.FileName);
                    LoadPhaseDeviation();
                }
            }
        }

        public void LoadTable(string filename)
        {
            LedGroup.SetEnabled(true);
            LedGroup.SetValue(true);

            _table = VisualTableExtensions.LoadTable(filename);
            if (_table == null)
            {
                return;
            }
            LoadData();
        }

        private void LoadPhasesGraph()
        {
            if (_table == null)
            {
                return;
            }
            if (IsFrequencyMode)
            {
                var data = new double[10, _table.ValuesCount];
                var step = 1f * (_table.MaxFrequencyKhz - _table.MinFrequencyKhz) / _table.ValuesCount;
                _phasesTechAppGraphControl.DataRange = new Range(_table.MinFrequencyKhz / 1000d, _table.MaxFrequencyKhz / 1000d);

                for (var i = 0; i < _table.ValuesCount; ++i)
                {
                    var frequency = _table.MinFrequencyKhz + i * step;
                    var phases = _table.FrequencyToPhases(frequency);

                    for (var j = 0; j < 10; ++j)
                    {
                        data[j, i] = phases[j];
                    }
                }
                _phasesTechAppGraphControl.Plot(data);
            }
            else
            {
                var data = new double[10, 360];
                for (var i = 0; i < 360; ++i)
                {
                    _table.CurrentAngle = i;
                    var phases = _table.FrequencyToPhases(_selectedFrequency);

                    for (var j = 0; j < 10; ++j)
                    {
                        data[j, i] = phases[j];
                    }
                }
                _phasesTechAppGraphControl.Plot(data);
            }
        }

        private void LoadAmplitudeGraph()
        {
            if (_table == null)
            {
                return;
            }
            var data = new double[5, _table.ValuesCount];
            var step = 1f * (_table.MaxFrequencyKhz - _table.MinFrequencyKhz) / _table.ValuesCount;
            _amplitudesTechAppGraphControl.DataRange = new Range(_table.MinFrequencyKhz / 1000d, _table.MaxFrequencyKhz / 1000d);


            for (var i = 0; i < _table.ValuesCount; ++i)
            {
                var frequency = _table.MinFrequencyKhz + i * step;
                var amplitudes = _table.FrequencyToAmplitudes(frequency);

                for (var j = 0; j < 5; ++j)
                {
                    data[j, i] = amplitudes[j];
                }
            }
            _amplitudesTechAppGraphControl.Plot(data);
        }

        private void LoadPhaseDeviation()
        {
            if (_table == null)
            {
                return;
            }
            for (var i = 0; i < Config.Instance.BandSettings.BandCount; ++i)
            {
                var phaseDeviation = _table.GetPhaseDeviations(i);
                var color = TechTabControl.PhaseDeviationToColor(phaseDeviation);
                BandsControl.SetColor(i, color);
            }
            BandsControl.Invalidate();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            var phases = new float[10];
            for (var i = 0; i < 10; ++i)
            {
                float phase;
                if (!float.TryParse(_phaseTextboxes[i].Text, out phase))
                {
                    return;
                }
                phases[i] = phase;
            }
            if (_table is RadioPathTable)
            {
                var table = _table as RadioPathTable;
                table.UpdatePhases(_selectedFrequency, phases);
            }
        }

        private void LoadTheoreticalTable_Click(object sender, EventArgs e)
        {
            LedGroup.SetEnabled(true);
            LedGroup.SetValue(true);

            _table = new TheoreticalTable(Config.Instance.TheoreticalTableSettings).ToVisualTable();

            LoadData();
        }

        private void LoadData()
        {
            LoadPhasesGraph();
            LoadAmplitudeGraph();
            LoadPhaseDeviation();
        }

        private void AngleTextBox_TextChanged(object sender, EventArgs e)
        {
            int number;
            if (!int.TryParse(AngleTextBox.Text, out number))
            {
                return;
            }
            if (IsFrequencyMode)
            {
                number = PhaseMath.NormalizePhase(number);
                _selectedAngle = number;
                if (_table != null)
                {
                    _table.CurrentAngle = number;
                }
                LoadPhasesGraph();
            }
            else
            {
                if (number * 1000 < Constants.FirstBandMinKhz || number * 1000 > Config.Instance.BandSettings.LastBandMaxKhz)
                {
                    return;
                }
                _selectedFrequency = number * 1000;
                LoadPhasesGraph();
            }
        }

        private void ModeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            IsFrequencyMode = ModeComboBox.SelectedIndex == 0;
            if (IsFrequencyMode)
            {
                if (_table != null)
                {
                    _table.CurrentAngle = _selectedAngle;
                }
                AngleTextBox.Text = _selectedAngle.ToString();
                _phasesTechAppGraphControl.DataRange = new Range(Constants.FirstBandMinKhz / 1000d, Config.Instance.BandSettings.LastBandMaxKhz / 1000d);
                _phasesTechAppGraphControl.VisibleRange = new Range(Constants.FirstBandMinKhz / 1000d, Config.Instance.BandSettings.LastBandMaxKhz / 1000d);
            }
            else
            {
                if (_selectedFrequency == 0)
                {
                    _selectedFrequency = Constants.FirstBandMinKhz;
                }
                AngleTextBox.Text = (int) (_selectedFrequency / 1000) + "";
                _phasesTechAppGraphControl.DataRange = new Range(0, 360);
                _phasesTechAppGraphControl.VisibleRange = new Range(0, 360);
            }
            LoadPhasesGraph();
        }

        private void CalibrationTabControl_Load(object sender, EventArgs e)
        {
            /*
            if (!Config.IsConfigLoaded())
            {
                return;
            }*/
            BandsControl.BandCount = Config.Instance.BandSettings.BandCount;
        }

        private float[][,] CreateTemperatureMaps(int width)
        {
            var step = (_table.MaxFrequencyKhz - _table.MinFrequencyKhz) / (width - 1);
            var maps = new float[10][,];
            for (var i = 0; i < 10; ++i)
            {
                maps[i] = new float[width, 360];
            }
            for (var direction = 0; direction < 360; ++direction)
            {
                _table.CurrentAngle = direction;
                for (var i = 0; i < width; ++i)
                {
                    var frequency = _table.MinFrequencyKhz + step * i;
                    var phases = _table.FrequencyToPhases(frequency);
                    for (var j = 0; j < 10; ++j)
                    {
                        maps[j][i, direction] = phases[j];
                    }
                }
            }
            return maps;
        }
        /*
        private void SaveTemperatureMap(float[,] map, string name)
        {
            var width = map.GetLength(0);
            var height = map.GetLength(1);

            var bitmap = new FastBitmap(width, height);

            for (var i = 0; i < width; ++i)
            {
                for (var j = 0; j < height; ++j)
                {
                    var phase = map[i, j];
                    byte r = 0, g = 0, b = 0;
                    if (phase < 120)
                    {
                        r = (byte)(byte.MaxValue * phase / 120);
                        b = (byte)(byte.MaxValue * (1 - phase / 120));
                    }
                    else if (phase < 240)
                    {
                        var alpha = phase - 120;
                        g = (byte)(byte.MaxValue * alpha / 120);
                        r = (byte)(byte.MaxValue * (1 - alpha / 120));
                    }
                    else
                    {
                        var alpha = phase - 240;
                        b = (byte)(byte.MaxValue * alpha / 120);
                        g = (byte)(byte.MaxValue * (1 - alpha / 120));
                    }
                    
                    var color = new FastColor(r, g, b);
                    bitmap.SetPixel(i, j, color);
                }
            }
            bitmap.Save(name);
        }
        */
        private void SaveTemperatureMapButton_Click(object sender, EventArgs e)
        {
            var maps = CreateTemperatureMaps(2000);
            for (var i = 0; i < maps.Length; i++)
            {
                var map = maps[i];
                //SaveTemperatureMap(map, "phase_" + i + ".bmp");
            }
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            OnVisibleChanged(EventArgs.Empty);
        }
    }
}
