﻿namespace TechAppControls
{
    partial class CalibrationTabControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.LeftPanel = new System.Windows.Forms.Panel();
            this.SaveTemperatureMapButton = new System.Windows.Forms.Button();
            this.ModeComboBox = new System.Windows.Forms.ComboBox();
            this.AngleTextBox = new System.Windows.Forms.TextBox();
            this.LoadTheoreticalTable = new System.Windows.Forms.Button();
            this.FrequencyLabel = new System.Windows.Forms.Label();
            this.SaveButton = new System.Windows.Forms.Button();
            this.LeftTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.LedGroup = new TechAppControls.LedGroup();
            this.RightTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._amplitudesTechAppGraphControl = new TechAppControls.TechAppGraphControl();
            this._phasesTechAppGraphControl = new TechAppControls.TechAppGraphControl();
            this.BandsControl = new TechAppControls.BandArrayControl();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.TableLayoutPanel.SuspendLayout();
            this.LeftPanel.SuspendLayout();
            this.LeftTableLayoutPanel.SuspendLayout();
            this.RightTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TableLayoutPanel
            // 
            this.TableLayoutPanel.ColumnCount = 2;
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel.Controls.Add(this.LeftPanel, 0, 0);
            this.TableLayoutPanel.Controls.Add(this.RightTableLayoutPanel, 1, 0);
            this.TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.TableLayoutPanel.Name = "TableLayoutPanel";
            this.TableLayoutPanel.RowCount = 1;
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel.Size = new System.Drawing.Size(1016, 674);
            this.TableLayoutPanel.TabIndex = 0;
            // 
            // LeftPanel
            // 
            this.LeftPanel.Controls.Add(this.buttonOpen);
            this.LeftPanel.Controls.Add(this.SaveTemperatureMapButton);
            this.LeftPanel.Controls.Add(this.ModeComboBox);
            this.LeftPanel.Controls.Add(this.AngleTextBox);
            this.LeftPanel.Controls.Add(this.LoadTheoreticalTable);
            this.LeftPanel.Controls.Add(this.FrequencyLabel);
            this.LeftPanel.Controls.Add(this.SaveButton);
            this.LeftPanel.Controls.Add(this.LeftTableLayoutPanel);
            this.LeftPanel.Controls.Add(this.LedGroup);
            this.LeftPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftPanel.Margin = new System.Windows.Forms.Padding(0);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(100, 674);
            this.LeftPanel.TabIndex = 0;
            // 
            // SaveTemperatureMapButton
            // 
            this.SaveTemperatureMapButton.Location = new System.Drawing.Point(37, 574);
            this.SaveTemperatureMapButton.Margin = new System.Windows.Forms.Padding(0);
            this.SaveTemperatureMapButton.Name = "SaveTemperatureMapButton";
            this.SaveTemperatureMapButton.Size = new System.Drawing.Size(59, 23);
            this.SaveTemperatureMapButton.TabIndex = 13;
            this.SaveTemperatureMapButton.Text = "2dsave";
            this.SaveTemperatureMapButton.UseVisualStyleBackColor = true;
            this.SaveTemperatureMapButton.Click += new System.EventHandler(this.SaveTemperatureMapButton_Click);
            // 
            // ModeComboBox
            // 
            this.ModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ModeComboBox.Items.AddRange(new object[] {
            "Угол",
            "Частота"});
            this.ModeComboBox.Location = new System.Drawing.Point(40, 624);
            this.ModeComboBox.Name = "ModeComboBox";
            this.ModeComboBox.Size = new System.Drawing.Size(50, 21);
            this.ModeComboBox.TabIndex = 12;
            this.ModeComboBox.SelectedIndexChanged += new System.EventHandler(this.ModeComboBox_SelectedIndexChanged);
            // 
            // AngleTextBox
            // 
            this.AngleTextBox.Location = new System.Drawing.Point(40, 651);
            this.AngleTextBox.Name = "AngleTextBox";
            this.AngleTextBox.Size = new System.Drawing.Size(50, 20);
            this.AngleTextBox.TabIndex = 10;
            this.AngleTextBox.TextChanged += new System.EventHandler(this.AngleTextBox_TextChanged);
            // 
            // LoadTheoreticalTable
            // 
            this.LoadTheoreticalTable.Location = new System.Drawing.Point(37, 597);
            this.LoadTheoreticalTable.Margin = new System.Windows.Forms.Padding(0);
            this.LoadTheoreticalTable.Name = "LoadTheoreticalTable";
            this.LoadTheoreticalTable.Size = new System.Drawing.Size(59, 23);
            this.LoadTheoreticalTable.TabIndex = 4;
            this.LoadTheoreticalTable.Text = "Теория";
            this.LoadTheoreticalTable.UseVisualStyleBackColor = true;
            this.LoadTheoreticalTable.Click += new System.EventHandler(this.LoadTheoreticalTable_Click);
            // 
            // FrequencyLabel
            // 
            this.FrequencyLabel.AutoSize = true;
            this.FrequencyLabel.Location = new System.Drawing.Point(34, 502);
            this.FrequencyLabel.Margin = new System.Windows.Forms.Padding(0);
            this.FrequencyLabel.Name = "FrequencyLabel";
            this.FrequencyLabel.Size = new System.Drawing.Size(65, 13);
            this.FrequencyLabel.TabIndex = 3;
            this.FrequencyLabel.Text = "3025.6 MHz";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(37, 551);
            this.SaveButton.Margin = new System.Windows.Forms.Padding(0);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(59, 23);
            this.SaveButton.TabIndex = 2;
            this.SaveButton.Text = "Сохранить";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // LeftTableLayoutPanel
            // 
            this.LeftTableLayoutPanel.ColumnCount = 1;
            this.LeftTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LeftTableLayoutPanel.Controls.Add(this.textBox10, 0, 9);
            this.LeftTableLayoutPanel.Controls.Add(this.textBox9, 0, 8);
            this.LeftTableLayoutPanel.Controls.Add(this.textBox8, 0, 7);
            this.LeftTableLayoutPanel.Controls.Add(this.textBox7, 0, 6);
            this.LeftTableLayoutPanel.Controls.Add(this.textBox6, 0, 5);
            this.LeftTableLayoutPanel.Controls.Add(this.textBox5, 0, 4);
            this.LeftTableLayoutPanel.Controls.Add(this.textBox4, 0, 3);
            this.LeftTableLayoutPanel.Controls.Add(this.textBox3, 0, 2);
            this.LeftTableLayoutPanel.Controls.Add(this.textBox1, 0, 0);
            this.LeftTableLayoutPanel.Controls.Add(this.textBox2, 0, 1);
            this.LeftTableLayoutPanel.Location = new System.Drawing.Point(37, 3);
            this.LeftTableLayoutPanel.Name = "LeftTableLayoutPanel";
            this.LeftTableLayoutPanel.RowCount = 10;
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.LeftTableLayoutPanel.Size = new System.Drawing.Size(56, 496);
            this.LeftTableLayoutPanel.TabIndex = 1;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(3, 471);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(50, 20);
            this.textBox10.TabIndex = 9;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(3, 418);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(50, 20);
            this.textBox9.TabIndex = 8;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(3, 367);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(50, 20);
            this.textBox8.TabIndex = 7;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(3, 315);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(50, 20);
            this.textBox7.TabIndex = 6;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(3, 263);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(50, 20);
            this.textBox6.TabIndex = 5;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(3, 211);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(50, 20);
            this.textBox5.TabIndex = 4;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(3, 159);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(50, 20);
            this.textBox4.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(3, 107);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(50, 20);
            this.textBox3.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(3, 55);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(50, 20);
            this.textBox2.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(50, 20);
            this.textBox1.TabIndex = 0;
            // 
            // LedGroup
            // 
            this.LedGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.LedGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LedGroup.ColorGenerator = null;
            this.LedGroup.Dock = System.Windows.Forms.DockStyle.Left;
            this.LedGroup.Location = new System.Drawing.Point(0, 0);
            this.LedGroup.Margin = new System.Windows.Forms.Padding(0);
            this.LedGroup.MinimumSize = new System.Drawing.Size(32, 2);
            this.LedGroup.Name = "LedGroup";
            this.LedGroup.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.LedGroup.Size = new System.Drawing.Size(34, 674);
            this.LedGroup.TabIndex = 0;
            // 
            // RightTableLayoutPanel
            // 
            this.RightTableLayoutPanel.ColumnCount = 1;
            this.RightTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.RightTableLayoutPanel.Controls.Add(this._amplitudesTechAppGraphControl, 0, 1);
            this.RightTableLayoutPanel.Controls.Add(this._phasesTechAppGraphControl, 0, 0);
            this.RightTableLayoutPanel.Controls.Add(this.BandsControl, 0, 2);
            this.RightTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RightTableLayoutPanel.Location = new System.Drawing.Point(100, 0);
            this.RightTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.RightTableLayoutPanel.Name = "RightTableLayoutPanel";
            this.RightTableLayoutPanel.RowCount = 3;
            this.RightTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.RightTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.RightTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.RightTableLayoutPanel.Size = new System.Drawing.Size(916, 674);
            this.RightTableLayoutPanel.TabIndex = 1;
            // 
            // _amplitudesTechAppGraphControl
            // 
            this._amplitudesTechAppGraphControl.DataRange = new NationalInstruments.UI.Range(25D, 3025D);
            this._amplitudesTechAppGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._amplitudesTechAppGraphControl.Location = new System.Drawing.Point(0, 455);
            this._amplitudesTechAppGraphControl.Margin = new System.Windows.Forms.Padding(0);
            this._amplitudesTechAppGraphControl.Name = "_amplitudesTechAppGraphControl";
            this._amplitudesTechAppGraphControl.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this._amplitudesTechAppGraphControl.SelectionEnabled = true;
            this._amplitudesTechAppGraphControl.SelectionWidth = 1000F;
            this._amplitudesTechAppGraphControl.Size = new System.Drawing.Size(916, 195);
            this._amplitudesTechAppGraphControl.StickDataToVisibleRange = false;
            this._amplitudesTechAppGraphControl.TabIndex = 3;
            this._amplitudesTechAppGraphControl.VisibleRange = new NationalInstruments.UI.Range(25D, 3025D);
            // 
            // _phasesTechAppGraphControl
            // 
            this._phasesTechAppGraphControl.DataRange = new NationalInstruments.UI.Range(25D, 3025D);
            this._phasesTechAppGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._phasesTechAppGraphControl.Location = new System.Drawing.Point(0, 0);
            this._phasesTechAppGraphControl.Margin = new System.Windows.Forms.Padding(0);
            this._phasesTechAppGraphControl.Name = "_phasesTechAppGraphControl";
            this._phasesTechAppGraphControl.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this._phasesTechAppGraphControl.SelectionEnabled = true;
            this._phasesTechAppGraphControl.SelectionWidth = 1000F;
            this._phasesTechAppGraphControl.Size = new System.Drawing.Size(916, 455);
            this._phasesTechAppGraphControl.StickDataToVisibleRange = false;
            this._phasesTechAppGraphControl.TabIndex = 2;
            this._phasesTechAppGraphControl.VisibleRange = new NationalInstruments.UI.Range(25D, 3025D);
            // 
            // BandsControl
            // 
            this.BandsControl.ActiveBandIndex = 0;
            this.BandsControl.BandCount = 1;
            this.BandsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BandsControl.Location = new System.Drawing.Point(3, 653);
            this.BandsControl.Name = "BandsControl";
            this.BandsControl.Size = new System.Drawing.Size(910, 18);
            this.BandsControl.TabIndex = 4;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // buttonOpen
            // 
            this.buttonOpen.Location = new System.Drawing.Point(37, 528);
            this.buttonOpen.Margin = new System.Windows.Forms.Padding(0);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(59, 23);
            this.buttonOpen.TabIndex = 14;
            this.buttonOpen.Text = "Открыть";
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // CalibrationTabControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TableLayoutPanel);
            this.Name = "CalibrationTabControl";
            this.Size = new System.Drawing.Size(1016, 674);
            this.Load += new System.EventHandler(this.CalibrationTabControl_Load);
            this.TableLayoutPanel.ResumeLayout(false);
            this.LeftPanel.ResumeLayout(false);
            this.LeftPanel.PerformLayout();
            this.LeftTableLayoutPanel.ResumeLayout(false);
            this.LeftTableLayoutPanel.PerformLayout();
            this.RightTableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel;
        private System.Windows.Forms.Panel LeftPanel;
        private LedGroup LedGroup;
        private System.Windows.Forms.TableLayoutPanel LeftTableLayoutPanel;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label FrequencyLabel;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TableLayoutPanel RightTableLayoutPanel;
        private TechAppGraphControl _phasesTechAppGraphControl;
        private TechAppGraphControl _amplitudesTechAppGraphControl;
        private System.Windows.Forms.Button LoadTheoreticalTable;
        private System.Windows.Forms.TextBox AngleTextBox;
        private System.Windows.Forms.ComboBox ModeComboBox;
        private BandArrayControl BandsControl;
        private System.Windows.Forms.Button SaveTemperatureMapButton;
        private System.Windows.Forms.Button buttonOpen;
    }
}
