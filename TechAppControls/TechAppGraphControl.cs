﻿using System;
using NationalInstruments.UI;
using Settings;

namespace TechAppControls
{
    public class TechAppGraphControl : GraphControl
    {
        public TechAppGraphControl()
        {
            try
            {
                VisibleRange = new Range(Constants.FirstBandMinKhz / 1000d, Config.Instance.BandSettings.LastBandMaxKhz / 1000d);
                DataRange = new Range(Constants.FirstBandMinKhz / 1000d, Config.Instance.BandSettings.LastBandMaxKhz / 1000d);
            }
            catch (Exception e)
            {
                VisibleRange = new Range(Constants.FirstBandMinKhz / 1000d, (Constants.FirstBandMinKhz + Constants.BandwidthKhz) / 1000d);
                DataRange = new Range(Constants.FirstBandMinKhz / 1000d, (Constants.FirstBandMinKhz + Constants.BandwidthKhz) / 1000d);
            }
        }

        public double MouseClickFrequency
        {
            get { return MouseClickPosition == -1  ? MouseClickPosition * 1000 : -1; }
        }
    }
}
