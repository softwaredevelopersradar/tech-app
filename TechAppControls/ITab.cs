﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechAppControls
{
    public interface ITab
    {
        bool IsWorking { get; }
        void StartTabWork();
        void StopTabWork();
    }
}
