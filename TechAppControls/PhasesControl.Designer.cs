﻿namespace TechAppControls
{
    partial class PhasesControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.phaseControl10 = new TechAppControls.PhaseControl();
            this.phaseControl9 = new TechAppControls.PhaseControl();
            this.phaseControl8 = new TechAppControls.PhaseControl();
            this.phaseControl7 = new TechAppControls.PhaseControl();
            this.phaseControl6 = new TechAppControls.PhaseControl();
            this.phaseControl5 = new TechAppControls.PhaseControl();
            this.phaseControl4 = new TechAppControls.PhaseControl();
            this.phaseControl3 = new TechAppControls.PhaseControl();
            this.phaseControl2 = new TechAppControls.PhaseControl();
            this.phaseControl1 = new TechAppControls.PhaseControl();
            this.TableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TableLayoutPanel
            // 
            this.TableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TableLayoutPanel.ColumnCount = 1;
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel.Controls.Add(this.phaseControl10, 0, 9);
            this.TableLayoutPanel.Controls.Add(this.phaseControl9, 0, 8);
            this.TableLayoutPanel.Controls.Add(this.phaseControl8, 0, 7);
            this.TableLayoutPanel.Controls.Add(this.phaseControl7, 0, 6);
            this.TableLayoutPanel.Controls.Add(this.phaseControl6, 0, 5);
            this.TableLayoutPanel.Controls.Add(this.phaseControl5, 0, 4);
            this.TableLayoutPanel.Controls.Add(this.phaseControl4, 0, 3);
            this.TableLayoutPanel.Controls.Add(this.phaseControl3, 0, 2);
            this.TableLayoutPanel.Controls.Add(this.phaseControl2, 0, 1);
            this.TableLayoutPanel.Controls.Add(this.phaseControl1, 0, 0);
            this.TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel.Margin = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.TableLayoutPanel.Name = "TableLayoutPanel";
            this.TableLayoutPanel.RowCount = 10;
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.TableLayoutPanel.Size = new System.Drawing.Size(70, 737);
            this.TableLayoutPanel.TabIndex = 0;
            // 
            // phaseControl10
            // 
            this.phaseControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseControl10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseControl10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseControl10.ControlName = "4-5";
            this.phaseControl10.Location = new System.Drawing.Point(1, 658);
            this.phaseControl10.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.phaseControl10.Name = "phaseControl10";
            this.phaseControl10.Size = new System.Drawing.Size(68, 78);
            this.phaseControl10.TabIndex = 9;
            this.phaseControl10.Value = 0F;
            // 
            // phaseControl9
            // 
            this.phaseControl9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseControl9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseControl9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseControl9.ControlName = "3-5";
            this.phaseControl9.Location = new System.Drawing.Point(1, 585);
            this.phaseControl9.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.phaseControl9.Name = "phaseControl9";
            this.phaseControl9.Size = new System.Drawing.Size(68, 71);
            this.phaseControl9.TabIndex = 8;
            this.phaseControl9.Value = 0F;
            // 
            // phaseControl8
            // 
            this.phaseControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseControl8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseControl8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseControl8.ControlName = "3-4";
            this.phaseControl8.Location = new System.Drawing.Point(1, 512);
            this.phaseControl8.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.phaseControl8.Name = "phaseControl8";
            this.phaseControl8.Size = new System.Drawing.Size(68, 71);
            this.phaseControl8.TabIndex = 7;
            this.phaseControl8.Value = 0F;
            // 
            // phaseControl7
            // 
            this.phaseControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseControl7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseControl7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseControl7.ControlName = "2-5";
            this.phaseControl7.Location = new System.Drawing.Point(1, 439);
            this.phaseControl7.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.phaseControl7.Name = "phaseControl7";
            this.phaseControl7.Size = new System.Drawing.Size(68, 71);
            this.phaseControl7.TabIndex = 6;
            this.phaseControl7.Value = 0F;
            // 
            // phaseControl6
            // 
            this.phaseControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseControl6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseControl6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseControl6.ControlName = "2-4";
            this.phaseControl6.Location = new System.Drawing.Point(1, 366);
            this.phaseControl6.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.phaseControl6.Name = "phaseControl6";
            this.phaseControl6.Size = new System.Drawing.Size(68, 71);
            this.phaseControl6.TabIndex = 5;
            this.phaseControl6.Value = 0F;
            // 
            // phaseControl5
            // 
            this.phaseControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseControl5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseControl5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseControl5.ControlName = "2-3";
            this.phaseControl5.Location = new System.Drawing.Point(1, 293);
            this.phaseControl5.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.phaseControl5.Name = "phaseControl5";
            this.phaseControl5.Size = new System.Drawing.Size(68, 71);
            this.phaseControl5.TabIndex = 4;
            this.phaseControl5.Value = 0F;
            // 
            // phaseControl4
            // 
            this.phaseControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseControl4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseControl4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseControl4.ControlName = "1-5";
            this.phaseControl4.Location = new System.Drawing.Point(1, 220);
            this.phaseControl4.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.phaseControl4.Name = "phaseControl4";
            this.phaseControl4.Size = new System.Drawing.Size(68, 71);
            this.phaseControl4.TabIndex = 3;
            this.phaseControl4.Value = 0F;
            // 
            // phaseControl3
            // 
            this.phaseControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseControl3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseControl3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseControl3.ControlName = "1-4";
            this.phaseControl3.Location = new System.Drawing.Point(1, 147);
            this.phaseControl3.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.phaseControl3.Name = "phaseControl3";
            this.phaseControl3.Size = new System.Drawing.Size(68, 71);
            this.phaseControl3.TabIndex = 2;
            this.phaseControl3.Value = 0F;
            // 
            // phaseControl2
            // 
            this.phaseControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseControl2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseControl2.ControlName = "1-3";
            this.phaseControl2.Location = new System.Drawing.Point(1, 74);
            this.phaseControl2.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.phaseControl2.Name = "phaseControl2";
            this.phaseControl2.Size = new System.Drawing.Size(68, 71);
            this.phaseControl2.TabIndex = 1;
            this.phaseControl2.Value = 0F;
            // 
            // phaseControl1
            // 
            this.phaseControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseControl1.ControlName = "1-2";
            this.phaseControl1.Location = new System.Drawing.Point(1, 1);
            this.phaseControl1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.phaseControl1.Name = "phaseControl1";
            this.phaseControl1.Size = new System.Drawing.Size(68, 71);
            this.phaseControl1.TabIndex = 0;
            this.phaseControl1.Value = 0F;
            // 
            // PhasesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.TableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "PhasesControl";
            this.Size = new System.Drawing.Size(70, 737);
            this.TableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel;
        private PhaseControl phaseControl10;
        private PhaseControl phaseControl9;
        private PhaseControl phaseControl8;
        private PhaseControl phaseControl7;
        private PhaseControl phaseControl6;
        private PhaseControl phaseControl5;
        private PhaseControl phaseControl4;
        private PhaseControl phaseControl3;
        private PhaseControl phaseControl2;
        private PhaseControl phaseControl1;
    }
}
