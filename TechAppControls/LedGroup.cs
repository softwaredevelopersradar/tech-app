﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using NationalInstruments.UI;
using NationalInstruments.UI.WindowsForms;

namespace TechAppControls
{
    public partial class LedGroup : UserControl
    {
        public ColorGenerator ColorGenerator
        {
            get { return _colorGenerator; }
            set
            {
                _colorGenerator = value;
                UpdateColors();
            }
        }

        public SelectionMode SelectionMode
        {
            get { return _selectionMode; }
            set
            {
                _selectionMode = value;
                if (SelectionMode == SelectionMode.One)
                {
                    SetValue(false);
                    _leds[0].Value = true;
                }
            }
        }

        public IReadOnlyList<Led> Leds { get { return _leds; } }

        public event EventHandler<int> LedClickEvent; 

        private Led[] _leds;
        private ColorGenerator _colorGenerator;
        private SelectionMode _selectionMode;

        public LedGroup()
        {
            InitializeComponent();
            SelectionMode = SelectionMode.MultiSimple;
        }

        public void Initialize(ColorGenerator colorGenerator, IEnumerable<string> names)
        {
            Initialize(colorGenerator, names.Count());
            
            var i = 0;
            foreach (var name in names)
            {
                _leds[i].Caption = name;
                ++i;
            }
        }

        public void Initialize(ColorGenerator colorGenerator, int ledCount)
        {
            ColorGenerator = colorGenerator;
            _leds = new Led[ledCount];

            for (var i = 0; i < ledCount; ++i)
            {
                _leds[i] = new Led
                {
                    OnColor = ColorGenerator.GetColor(i),
                    OffColor = Color.Black,
                    LedStyle = LedStyle.Square,
                    Size = new Size(26, 46),
                    CaptionVisible = true,
                    CaptionBackColor = Color.Transparent
                };
                _leds[i].Click += OnLedClick;
                FlowLayoutPanel.Controls.Add(_leds[i]);
            }
        }

        public void SetEnabled(bool isEnabled)
        {
            foreach (var led in _leds)
            {
                led.Enabled = isEnabled;
            }
        }

        public void SetValue(bool value)
        {
            foreach (var led in _leds)
            {
                led.Value = value;
            }
        }

        private void OnLedClick(object sender, EventArgs e)
        {
            if (SelectionMode == SelectionMode.None)
                return;
            var led = (Led)sender;

            if (SelectionMode == SelectionMode.One)
            {
                if (!led.Value)
                {
                    SetValue(false);
                    led.Value = true;
                }
            }
            else
            {
                led.Value = !led.Value;
            }
            OnLedClickEvent(Array.IndexOf(_leds, led));
        }

        private void UpdateColors()
        {
            if (Leds == null)
            {
                return;
            }
            for (int i = 0; i < Leds.Count; ++i)
            {
                _leds[i].OnColor = ColorGenerator.GetColor(i);
            }
        }

        private void OnLedClickEvent(int e)
        {
            var handler = LedClickEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
