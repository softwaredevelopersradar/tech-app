﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Correlator;
using ExternalCalibrationData;
using Phases;
using Settings;

namespace TechAppControls
{
    public class ExternalCalibrationVisualTable : IVisualTable
    {
        private readonly Dictionary<int, DirectionCalibration> _calibrations = new Dictionary<int, DirectionCalibration>();

        public ExternalCalibrationVisualTable(string filename)
        {
            LoadTable(filename);
        }

        private void LoadTable(string inputFilename)
        {
            var directory = Path.GetDirectoryName(inputFilename);
            var filenames = Directory.EnumerateFiles(directory)
                .Where(path =>
                {
                    var filename = Path.GetFileName(path);
                    return filename.StartsWith("calibration_") && filename.EndsWith(".txt");
                })
                .ToArray();
            foreach (var path in filenames)
            {
                try
                {
                    var filename = Path.GetFileName(path);
                    var directionString = filename.Replace("calibration_", "").Replace(".txt", "");
                    int direction;
                    if (int.TryParse(directionString, out direction))
                    {
                        _calibrations.Add(direction, DirectionCalibration.Load(directory, direction));
                    }
                }
                catch (Exception e)
                {
                    // ignored
                }
            }
        }

        public float GetPhaseDeviations(int bandNumber)
        {
            return 0;
        }

        public float[] FrequencyToPhases(float frequencyKhz)
        {
            const float maxDistanceKhz = 5 * 1000;

            var signal = GetNearestSignal(frequencyKhz);
            if (signal == null)
            {
                return  new float[10];
            }
            var distance = Math.Abs(frequencyKhz - signal.FrequencyKhz);
            
            return distance <= maxDistanceKhz
                ? signal.Phases
                : new float[10];
        }

        public float[] FrequencyToAmplitudes(float frequencyKhz)
        {
            return new float[5];
        }

        private DirectionCalibration GetNearestDirectionCalibration(int direction)
        {
            var bestDistance = 360;
            DirectionCalibration result = null;

            if (_calibrations.ContainsKey(direction))
            {
                return _calibrations[direction];
            }

            foreach (var directionCalibration in _calibrations)
            {
                var distance = PhaseMath.Angle(direction, directionCalibration.Key);
                if (distance < bestDistance)
                {
                    bestDistance = distance;
                    result = directionCalibration.Value;
                }
            }
            if (result == null)
            {
                return null;
            }
            const float maxAngleDistance = 5;
            return bestDistance <= maxAngleDistance ? result : null;
        }

        private SignalCalibration GetNearestSignal(float frequencyKhz)
        {
            var calibration = GetNearestDirectionCalibration(CurrentAngle);
            if (calibration == null)
            {
                return null;
            }

            SignalCalibration nearestSignal = null;
            var nearestDistance = float.MaxValue;

            foreach (var bandCalibration in calibration.BandCalibrations.Values)
            {
                foreach (var signal in bandCalibration.Signals)
                {
                    var distance = Math.Abs(signal.FrequencyKhz - frequencyKhz);
                    if (distance < nearestDistance)
                    {
                        nearestDistance = distance;
                        nearestSignal = signal;
                    }
                }
            }
            return nearestSignal;
        }

        public float GetNearestTableFrequency(float frequency)
        {
            var signal = GetNearestSignal(frequency);
            if (signal == null)
                return Constants.FirstBandMinKhz;
            return GetNearestSignal(frequency).FrequencyKhz;
        }

        public int MinFrequencyKhz
        {
            get { return Constants.FirstBandMinKhz; }
        }

        public int MaxFrequencyKhz
        {
            get { return Config.Instance.BandSettings.LastBandMaxKhz; }
        }

        public int ValuesCount
        {
            get { return (Config.Instance.BandSettings.LastBandMaxKhz - Constants.FirstBandMinKhz) / 1000; }
        }

        public int CurrentAngle { get; set; }
    }
}
