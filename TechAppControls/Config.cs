﻿using System;
using System.IO;
using DspDataModel;
using NLog;
using YamlDotNet.Serialization;

namespace TechAppControls
{
    public class Config
    {
        private static Config _instance;

        public const string ConfigPath = "config.yaml";

        public string ServerHost { get; private set; }

        public int ServerPort { get; private set; }

        public TheoreticalTableConfig TheoreticalTableSettings { get; set; }

        public string RadioPathTableFilename { get; private set; }

        public BandConfig BandSettings { get; private set; }

        private static readonly object LockObject;
        private static readonly Logger Logger;

        static Config()
        {
            LockObject = new object();
            Logger = LogManager.GetCurrentClassLogger();
        }

        public static void Save()
        {
            Save(ConfigPath);
        }

        public static void Save(string filename)
        {
            try
            {
                lock (LockObject)
                {
                    var serializer = new SerializerBuilder()
                        .EmitDefaults()
                        .Build();
                    using (var fs = File.Open(filename, FileMode.Create))
                    using (var sw = new StreamWriter(fs))
                    {
                        serializer.Serialize(sw, _instance);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Can't save config");
            }
            
        }

        /// <returns> returns true if load is successful </returns>
        public static bool Load(string filename)
        {
            try
            {
                lock (LockObject)
                {
                    var deserializer = new DeserializerBuilder()
                        .Build();
                    _instance = deserializer.Deserialize<Config>(File.ReadAllText(filename));
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Can't load config");
                return false;
            }

            return true;
        }

        public static Config Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;
                if (Load(ConfigPath))
                    return _instance;
                throw new ArgumentException("Can't load config from " + ConfigPath);
            }
        }
    }
}
