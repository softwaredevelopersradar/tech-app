﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Correlator;
using DspDataModel.Hardware;
using DspDataModel.Tasks;
using NationalInstruments.UI;
using NetLib;
using Phases;
using Protocols;
using Settings;
using SharpExtensions;

namespace TechAppControls
{
    public partial class TechTabControl : UserControl, ITab
    {
        public DspClient.DspClient Client
        {
            get { return _client; }
            set
            {
                _client = value;
                BandSettingsControl.Client = _client;
            }
        }

        public RadioPathTable RadiopathTable;

        private int _bandNumber = -1;
        private readonly double[,] _spectrum = new double[6, Constants.BandSampleCount];
        
        private int _selectedSignalPosition;
        private int _averagingCount = 1;

        private bool _calibrationBandsAreShown = false;
        private HashSet<int> _calibrationBandsHashSet = new HashSet<int>();
        private Dictionary<int, Color> _bandsColors = new Dictionary<int, Color>();
        private const int MaxPhaseCount = 200;

        public bool IsWorking
        {
            get { return IsCalibrationRunning || IsSpectrumTaskRunning; }
        }

        public bool IsCalibrationRunning { get; private set; }

        public bool IsSpectrumTaskRunning { get; private set; }

        private Queue<float>[] _phaseDifferences;
        private DspClient.DspClient _client;

        private int _spectrumTaskUpdateDelay = 50;

        private int _signalDirection = 0;

        public float SelectedSignalFrequency { get { return Utilities.GetFrequencyKhz(BandNumber, _selectedSignalPosition); } }

        public XYCursor AdaptiveCursor;

        public int AveragingCount
        {
            get { return _averagingCount; }
            set
            {
                if (value <= 0 || value > 100)
                {
                    return;
                }
                _averagingCount = value;
            }
        }

        public int BandNumber
        {
            get { return _bandNumber; }
            private set
            {
                if (_bandNumber == value)
                {
                    return;
                }
                try
                {
                    _bandNumber = value;
                    BandNumberTextBox.Text = _bandNumber.ToString();
                    _techAppGraphControl.VisibleRange = new Range(Utilities.GetBandMinFrequencyKhz(BandNumber) / 1000d,
                        Utilities.GetBandMaxFrequencyKhz(BandNumber) / 1000d);
                    _techAppGraphControl.DataRange = new Range(Utilities.GetBandMinFrequencyKhz(BandNumber) / 1000d,
                        Utilities.GetBandMaxFrequencyKhz(BandNumber) / 1000d);
                    UpdateBandSettingPanels();
                    
                    if (BandArrayControl != null)
                    {
                        BandArrayControl.ActiveBandIndex = BandNumber;
                    }
                }
                catch (Exception e)
                {
                }
            }
        }

        public TechTabControl()
        {
            InitializeComponent();

            LedGroup.Initialize(_techAppGraphControl.Graph.PlotLineColorGenerator,
                new[]
                {
                    "1", "2", "3", "4", "5", "6", "Все"
                });
            LedGroup.Leds[0].Value = true;
            LedGroup.Leds.Last().OnColor = Color.White;
            LedGroup.LedClickEvent += OnLedClick;

            BandArrayControl.PictureClickEvent += OnBandArrayPictureClick;

            _techAppGraphControl.Graph.AfterDrawPlotArea += OnSpectrumPlotDrawed;
            _techAppGraphControl.GraphClickEvent += OnTechAppGraphClick;

            _techAppGraphControl.Graph.YAxes[0].Range = new Range(Constants.ReceiverMinAmplitude, 0);
            _techAppGraphControl.Graph.YAxes[0].Mode = AxisMode.Fixed;
            _techAppGraphControl.Graph.YAxes[0].Caption = "дБм";
            _techAppGraphControl.Graph.XAxes[0].Caption = "МГц";
            _techAppGraphControl.MinVisibleRange = 0.1;
            InitializeAdaptiveThresholdCursor();
            BandNumber = 0;
            AveragingCount = 1;

            _phaseDifferences = new Queue<float>[10];
            for (var i = 0; i < 10; i++)
            {
                _phaseDifferences[i] = new Queue<float>();
            }
        }

        private void InitializeAdaptiveThresholdCursor()
        {
            AdaptiveCursor = new XYCursor();

            AdaptiveCursor.BeginInit();

            AdaptiveCursor.Color = Color.DodgerBlue;
            AdaptiveCursor.LabelDisplay = XYCursorLabelDisplay.ShowY;
            AdaptiveCursor.Plot = _techAppGraphControl.Graph.Plots[0];
            AdaptiveCursor.PointSize = new Size(0, 0);
            AdaptiveCursor.PointStyle = PointStyle.Cross;
            AdaptiveCursor.SnapMode = CursorSnapMode.Fixed;
            AdaptiveCursor.VerticalCrosshairMode = CursorCrosshairMode.None;

            AdaptiveCursor.EndInit();

            _techAppGraphControl.Graph.Cursors.Add(AdaptiveCursor);
        }

        void OnTechAppGraphClick(object sender, float e)
        {
            foreach (var phaseDifference in _phaseDifferences)
            {
                phaseDifference.Clear();
            }
        }

        private void OnBandArrayPictureClick(object sender, int index)
        {
            if (Control.ModifierKeys != Keys.Shift)
            {
                BandNumber = index;
            }
            else
            {
                var range = new List<int>();
                if (BandNumber > index)
                    range = Enumerable.Range(index, BandNumber - index + 1).ToList();
                else
                    range = Enumerable.Range(BandNumber, index - BandNumber + 1).ToList();
                foreach (var band in range)
                {
                    _calibrationBandsHashSet.Add(band);
                }

                ShowCalibrationBands(1000);
            }
        }

        void OnLedClick(object sender, int index)
        {
            if (index != LedGroup.Leds.Count - 1)
            {
                return;
            }
            var led = LedGroup.Leds.Last();
            LedGroup.SetValue(led.Value);
        }

        public async Task UpdateBandSettingPanels()
        {
            if (Client == null)
            {
                return;
            }
            var attenuatorSettings = await Client.GetAttenuatorsValues(new[] {BandNumber});
            var ampliferSettings = await Client.GetAmplifiersValues(new[] {BandNumber});

            BandSettingsControl.Update(BandNumber, attenuatorSettings[0], ampliferSettings[0]);
        }

        void OnNextBandButtonClick(object sender, EventArgs e)
        {
            if (BandNumber < Config.Instance.BandSettings.BandCount - 1)
            {
                BandNumber++;
            }
        }

        void OnPrevBandButtonClick(object sender, EventArgs e)
        {
            if (BandNumber > 0)
            {
                BandNumber--;
            }
        }

        public void StartTabWork()
        {
            if (IsCalibrationRunning)
            {
                IsCalibrationRunning = false;
            }
            if (Client.Client.IsWorking)
                UpdateBandSettingPanels();
            SpectrumTask();
        }

        public async void StopTabWork()
        {
            IsCalibrationRunning = false;
            IsSpectrumTaskRunning = false;

            if (Client == null)
            {
                return;
            }
            await Client.SetMode(DspServerMode.Stop);
        }

        private void OnBandNumberChanged(object sender, EventArgs e)
        {
            int number;
            if (!int.TryParse(BandNumberTextBox.Text, out number))
            {
                return;
            }
            if (number >= 0 && number < Config.Instance.BandSettings.BandCount)
            {
                BandNumber = number;
            }
        }

        private async void OnCalibrationButtonClick(object sender, EventArgs e)
        {
            if (IsCalibrationRunning)
            {
                return;
            }
            StopTabWork();
            if (CalibrationFullRadioButton.Checked)
                await Client.SetCalibrationBands(new int[0]);
            if (CalibrationCurrentBandRadioButton.Checked)
            {
                await Client.SetCalibrationBands(new int[1] { BandNumber });
            }
            if (CalibrationSelectedBandsRadioButton.Checked)
                await Client.SetCalibrationBands(_calibrationBandsHashSet.ToArray());
            await Client.SetMode(DspServerMode.Calibration);
            CalibrationTask();
        }

        private async Task CalibrationTask()
        {
            if (IsCalibrationRunning)
            {
                return;
            }
            IsCalibrationRunning = true;
            CalibrationButton.Enabled = false;

            var progress = 0;
            BandArrayControl.ClearAllRectangles();
            while (IsCalibrationRunning && progress != 255)
            {
                var requestResult = await Client.GetCalibrationProgress();
                if (requestResult == null)
                {
                    return;
                }

                progress = requestResult.Progress;
                var numberOfBands = Config.Instance.BandSettings.BandCount;
                if (CalibrationCurrentBandRadioButton.Checked)
                    numberOfBands = 1;
                if (CalibrationSelectedBandsRadioButton.Checked)
                    numberOfBands = _calibrationBandsHashSet.Count;
                var readyBandCount = (numberOfBands * progress) / 255;
                for (var i = 0; i < readyBandCount; ++i)
                {
                    var band = i;
                    if (CalibrationCurrentBandRadioButton.Checked)
                        band = BandNumber;
                    if (_calibrationBandsHashSet.Count != 0 && CalibrationSelectedBandsRadioButton.Checked)
                        band = _calibrationBandsHashSet.ElementAt(i);
                    var bandDeviation = requestResult.BandPhaseDeviations[i];
                    BandArrayControl.SetColor(band, PhaseDeviationToColor(bandDeviation));
                }
                BandArrayControl.Invalidate();
                await Task.Delay(1000);
            }
            IsCalibrationRunning = false;
            CalibrationButton.Enabled = true;
            if (CalibrationCurrentBandRadioButton.Checked)
                Client.SetCalibrationBands(new int[0]);
        }

        public static Color PhaseDeviationToColor(float deviation)
        {
            var color = Color.Green;
            if (deviation > 10)
            {
                color = deviation < 20 ? Color.Yellow : Color.Red;
            }
            return color;
        }

        private async Task SpectrumTask()
        {
            if (IsSpectrumTaskRunning || Client == null)
            {
                return;
            }
            try
            {
                IsSpectrumTaskRunning = true;
                while (IsSpectrumTaskRunning)
                {
                    if (Client.Status != ClientStatus.IsWorking)
                        return;
                    var scans = await Client.GetTechAppSpectrum(BandNumber, AveragingCount);
                    var adaptiveThreshold = await Client.GetAdaptiveThreshold(BandNumber);
                    if (scans == null)
                    {
                        IsSpectrumTaskRunning = false;
                        return;
                    }
                    AdaptiveCursor.YPosition = adaptiveThreshold;
                    UpdateSpectrum(scans);
                    await Task.Delay(_spectrumTaskUpdateDelay);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                MessageBox.Show(e.StackTrace);
                IsSpectrumTaskRunning = false;
                throw;
            }
            IsSpectrumTaskRunning = false;
        }

        private void UpdateSpectrum(Scan[] scans)
        {
            UpdateAmplitudeSpectrum(scans);
            UpdatePhases(scans);
            _techAppGraphControl.Plot(_spectrum);
        }


        private void UpdateAmplitudeSpectrum(Scan[] scans)
        {
            for (var i = 0; i < scans.Length; i++)
            {
                var scan = scans[i];
                for (var j = 0; j < scan.Amplitudes.Length; ++j)
                {
                    if (LedGroup.Leds[i].Value)
                    {
                        _spectrum[i, j] = Constants.ReceiverMinAmplitude + scan.Amplitudes[j];
                    }
                    else
                    {
                        _spectrum[i, j] = 0;
                    }
                }
            }
        }

        private float[] GetPhases(Scan[] scans, int index)
        {
            var result = new float[10];
            var calibrationPhases = GetCalibrationPhases(index);

            for (int i = 0, k = 0; i < 5; ++i)
            {
                for (var j = i + 1; j < 5; ++j)
                {
                    result[k] = PhaseMath.NormalizePhase((scans[i].Phases[index] - scans[j].Phases[index]) * 0.1f - calibrationPhases[k]);
                    ++k;
                }
            }
            return result;
        }

        private float[] GetCalibrationPhases(int index)
        {
            try
            {
                return AbsolutePhasesCheckBox.Checked
                    ? new float[10]
                    : RadiopathTable.GetPhases(BandNumber, index).Phases;
            }
            catch (Exception)
            {
                // it's possible that radiopath is not loaded
                return new float[10];
            }
        }

        void OnSpectrumPlotDrawed(object sender, AfterDrawEventArgs e)
        {
            if (_techAppGraphControl.GetClickedDataIndex() == null)
            {
                return;
            }
            var amplitude = MaxAmplitude(_selectedSignalPosition);

            var yPosition = AmplitudeToMouseYPosition(amplitude);
            var xPosition = _techAppGraphControl.DataIndexToComponentXPosition(_selectedSignalPosition);
            var frequency = Utilities.GetFrequencyKhz(BandNumber, _selectedSignalPosition);

            var message = frequency.ToString("F0") + " КГц, " +
                          amplitude.ToString("F0") + " дБм";

            e.Graphics.DrawString(message, SystemFonts.CaptionFont, Brushes.White, xPosition - 10, yPosition - 10);
        }

        private float MaxAmplitude(int index)
        {
            var maxAmplitude = double.MinValue;
            for (var i = 0; i < 6; ++i)
            {
                if (LedGroup.Leds[i].Value && maxAmplitude < _spectrum[i, index])
                {
                    maxAmplitude = _spectrum[i, index];
                }
            }
            return (float)maxAmplitude;
        }

        private int AmplitudeToMouseYPosition(float amplitude)
        {
            var y = _techAppGraphControl.Graph.PlotAreaBounds.Height - (int) (_techAppGraphControl.Graph.PlotAreaBounds.Height *
                                                            (1 - amplitude / Constants.ReceiverMinAmplitude));
            return y < 40 ? 40 : y;
        }

        private void UpdatePhases(Scan[] scans)
        {
            if (_techAppGraphControl.GetClickedDataIndex() == null)
            {
                return;
            }
            var mouseClickPos = (int) _techAppGraphControl.GetClickedDataIndex();
            if (mouseClickPos < 0 || mouseClickPos > Constants.BandSampleCount)
            {
                return;
            }

            var width = _techAppGraphControl.GetSelectionWidthDataCount();
            if (width < 2)
            {
                width = 2;
            }
            var to = mouseClickPos + width / 2;
            var from = mouseClickPos - width / 2;

            to = to.ToBounds(0, Constants.BandSampleCount - 1);
            from = from.ToBounds(0, Constants.BandSampleCount - 1);

            var max = byte.MinValue;
            var maxIndex = mouseClickPos;

            for (var i = 0; i < scans.Length; i++)
            {
                for (var j = from; j < to; ++j)
                {
                    if (LedGroup.Leds[i].Value && scans[i].Amplitudes[j] > max)
                    {
                        max = scans[i].Amplitudes[j];
                        maxIndex = j;
                    }
                }
            }
            if (maxIndex == scans[0].Amplitudes.Length)
            {
                maxIndex--;
            }
            _selectedSignalPosition = maxIndex;

            var phases = GetPhases(scans, maxIndex);
            var resultPhases = new float[10];
            for (var i = 0; i < 10; ++i)
            {
                if (UsePhaseAveragingCheckbox.Checked)
                {
                    _phaseDifferences[i].Enqueue(phases[i]);
                    while (_phaseDifferences[i].Count > MaxPhaseCount)
                    {
                        _phaseDifferences[i].Dequeue();
                    }
                    resultPhases[i] = PhaseMath.AveragePhase(_phaseDifferences[i]);
                }
                else
                {
                    _phaseDifferences[i].Clear();
                    resultPhases[i] = phases[i];
                }
            }
            PhasesControl.SetPhases(resultPhases);
        }

        private void AveragingTextBox_TextChanged(object sender, EventArgs e)
        {
            int number;
            if (!int.TryParse(AveragingTextBox.Text, out number))
            {
                return;
            }
            if (number > 0 && number <= 100)
            {
                AveragingCount = number;
            }
        }

        private void SavePhasesButton_Click(object sender, EventArgs e)
        {
            if (PhasesControl.Phases == null)
            {
                return;
            }
            var frequency = (SelectedSignalFrequency / 1000).ToString("F1") + " МГц ";
            var phases = PhasesControl.Phases.Select(p => p.ToString("000")).ToArray();

            var str = frequency + phases.Aggregate("", (s, p) => s + "\t " + p) + "\n";

            File.AppendAllText("phases.txt", str);
        }

        private void SelectionWidthTextBox_TextChanged(object sender, EventArgs e)
        {
            int number;
            if (!int.TryParse(SelectionWidthTextBox.Text, out number))
            {
                return;
            }
            if (number >= 0 && number <= 30000)
            {
                _techAppGraphControl.SelectionWidth = number;
            }
        }

        private void _techAppGraphControl_Load(object sender, EventArgs e)
        {
            //if(Config.IsConfigLoaded())
                BandArrayControl.BandCount = Config.Instance.BandSettings.BandCount;
        }

        private async void AirRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (Client == null)
            {
                return;
            }
            await Client.SetReceiversChannel(ReceiverChannel.Air);
        }

        private async void NoiseGeneratorRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (Client == null)
            {
                return;
            }
            await Client.SetReceiversChannel(ReceiverChannel.NoiseGenerator);
        }

        private async void ExternalCalibrationRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (Client == null)
            {
                return;
            }
            await Client.SetReceiversChannel(ReceiverChannel.ExternalCalibration);
        }

        private void UpdateTimeTextBox_TextChanged(object sender, EventArgs e)
        {
            int time;
            if (!int.TryParse(UpdateTimeTextBox.Text, out time))
            {
                return;
            }
            if (time >= 10 && time <= 10000)
            {
                _spectrumTaskUpdateDelay = time;
            }
        }

        private void DirectionTextbox_TextChanged(object sender, EventArgs e)
        {
            int direction;
            if (!int.TryParse(DirectionTextbox.Text, out direction))
            {
                return;
            }
            if (direction >= 0 && direction < 360)
            {
                _signalDirection = direction;
            }
        }

        private async void AddSignalToCalibrationButton_Click(object sender, EventArgs e)
        {
            await Client.SetPreciseCalibrationPhases(SelectedSignalFrequency, _signalDirection, PhasesControl.Phases.ToArray());
        }


        #region Katya's save

        private FileStream _fileStream;

        private readonly object _fileStreamLock = new object();

        private List<Scan> lastTwentyScans = new List<Scan>();
        private void button1_Click(object sender, EventArgs e)
        {
            RecordAmplitudes(lastTwentyScans);
        }

        private void RecordAmplitudes(List<Scan> scans)
        {
            if (scans == null || lastTwentyScans.Count < 20)
                return;
            lock (_fileStreamLock)
            {
                try
                {
                    _fileStream = new FileStream("amplitudes.bin", FileMode.Create, FileAccess.Write);
                }
                catch (Exception)
                {
                    _fileStream.Close();
                }
            }
            lock (_fileStreamLock)
            {
                try
                {
                    foreach (var scan in scans)
                    {
                        _fileStream.Write(scan.Amplitudes, 0, scan.Amplitudes.Length);
                    }
                    _fileStream.Close();
                }
                catch (Exception)
                {
                    _fileStream.Close();
                }
            }
        }
#endregion

        private void buttonResetAttenuators_Click(object sender, EventArgs e)
        {
            var currentBand = BandNumber;
            for (int i = 0; i < Config.Instance.BandSettings.BandCount; i++)
            {
                BandSettingsControl.SetBandNumber(i);
                BandSettingsControl.SetAttenuatorValue(0);
                BandSettingsControl.SetConstAttenuatorEnabled(false);
                System.Threading.Thread.Sleep(50);
            }
            BandSettingsControl.SetBandNumber(currentBand);
            BandNumber = currentBand;
        }

        private void CalibrationSelectedBandsRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            buttonClearSelectedBands.Visible = CalibrationSelectedBandsRadioButton.Checked;
            buttonShowSelectedBands.Visible = CalibrationSelectedBandsRadioButton.Checked;
            labelCalibrationInfo.Visible = !CalibrationSelectedBandsRadioButton.Checked;
        }

        private void buttonShowSelectedBands_Click(object sender, EventArgs e)
        {
            ShowCalibrationBands();
        }

        private void buttonClearSelectedBands_Click(object sender, EventArgs e)
        {
            _calibrationBandsHashSet.Clear();
            Client.SetCalibrationBands(new int[0]);
            ShowCalibrationBands();
        }

        private void ShowCalibrationBands(int durationMs = 0)
        {
            if (IsCalibrationRunning)
                return;
            if (_calibrationBandsAreShown && durationMs == 0)
            {
                //hide
                foreach (var bandWithColor in _bandsColors)
                {
                    BandArrayControl.SetColor(bandWithColor.Key, bandWithColor.Value);
                }
                BandArrayControl.Invalidate();
                _calibrationBandsAreShown = false;
                return;
            }
            //show
            foreach (var band in _calibrationBandsHashSet)
            {
                //natural color saved
                if (BandArrayControl.Colors.ElementAt(band) != Color.Gray)
                    if (_bandsColors.ContainsKey(band) == false)
                        _bandsColors.Add(band, BandArrayControl.Colors.ElementAt(band));
                    else
                        _bandsColors[band] = BandArrayControl.Colors.ElementAt(band);

                BandArrayControl.SetColor(band, Color.Gray);
            }

            BandArrayControl.Invalidate();
            if(durationMs == 0)
                _calibrationBandsAreShown = true;

            if (durationMs != 0 && _calibrationBandsAreShown == false)
            {
                Task.Run(async () =>
                {
                    await Task.Delay(durationMs);
                    foreach (var bandWithColor in _bandsColors)
                    {
                        BandArrayControl.SetColor(bandWithColor.Key, bandWithColor.Value);
                    }
                });
                BandArrayControl.Invalidate();
                _calibrationBandsAreShown = false;
            }
        }
    }
}
