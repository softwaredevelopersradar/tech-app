﻿namespace TechAppControls
{
    partial class BandSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.TotalGainLevel = new System.Windows.Forms.Label();
            this.AmplifierLabel = new System.Windows.Forms.Label();
            this.AttenuatorTank = new NationalInstruments.UI.WindowsForms.Tank();
            this.TopPanel = new System.Windows.Forms.Panel();
            this.BandNumberLabel = new System.Windows.Forms.Label();
            this.MiddlePanel = new System.Windows.Forms.Panel();
            this.ConstAttenuator = new System.Windows.Forms.CheckBox();
            this.TableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AttenuatorTank)).BeginInit();
            this.TopPanel.SuspendLayout();
            this.MiddlePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TableLayoutPanel
            // 
            this.TableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TableLayoutPanel.ColumnCount = 1;
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel.Controls.Add(this.TopPanel, 0, 0);
            this.TableLayoutPanel.Controls.Add(this.AttenuatorTank, 0, 2);
            this.TableLayoutPanel.Controls.Add(this.MiddlePanel, 0, 1);
            this.TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.TableLayoutPanel.Name = "TableLayoutPanel";
            this.TableLayoutPanel.RowCount = 3;
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel.Size = new System.Drawing.Size(365, 175);
            this.TableLayoutPanel.TabIndex = 27;
            // 
            // TotalGainLevel
            // 
            this.TotalGainLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TotalGainLevel.AutoSize = true;
            this.TotalGainLevel.Location = new System.Drawing.Point(275, 3);
            this.TotalGainLevel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.TotalGainLevel.Name = "TotalGainLevel";
            this.TotalGainLevel.Size = new System.Drawing.Size(40, 13);
            this.TotalGainLevel.TabIndex = 23;
            this.TotalGainLevel.Text = "Всего:";
            // 
            // AmplifierLabel
            // 
            this.AmplifierLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AmplifierLabel.AutoSize = true;
            this.AmplifierLabel.Location = new System.Drawing.Point(250, 3);
            this.AmplifierLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.AmplifierLabel.Name = "AmplifierLabel";
            this.AmplifierLabel.Size = new System.Drawing.Size(65, 13);
            this.AmplifierLabel.TabIndex = 22;
            this.AmplifierLabel.Text = "Усилитель:";
            // 
            // AttenuatorTank
            // 
            this.AttenuatorTank.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AttenuatorTank.FillColor = System.Drawing.SystemColors.Highlight;
            this.AttenuatorTank.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AttenuatorTank.InteractionMode = ((NationalInstruments.UI.LinearNumericPointerInteractionModes)((NationalInstruments.UI.LinearNumericPointerInteractionModes.DragPointer | NationalInstruments.UI.LinearNumericPointerInteractionModes.SnapPointer)));
            this.AttenuatorTank.InteractionMouseCursors.Drag = System.Windows.Forms.Cursors.SizeWE;
            this.AttenuatorTank.InteractionMouseCursors.Dragging = System.Windows.Forms.Cursors.SizeWE;
            this.AttenuatorTank.InteractionMouseCursors.Snap = System.Windows.Forms.Cursors.SizeWE;
            this.AttenuatorTank.Location = new System.Drawing.Point(0, 36);
            this.AttenuatorTank.Margin = new System.Windows.Forms.Padding(0);
            this.AttenuatorTank.MinimumSize = new System.Drawing.Size(30, 40);
            this.AttenuatorTank.Name = "AttenuatorTank";
            this.AttenuatorTank.Range = new NationalInstruments.UI.Range(0D, 30D);
            this.AttenuatorTank.ScalePosition = NationalInstruments.UI.NumericScalePosition.Bottom;
            this.AttenuatorTank.Size = new System.Drawing.Size(365, 139);
            this.AttenuatorTank.TabIndex = 21;
            this.AttenuatorTank.TankStyle = NationalInstruments.UI.TankStyle.Flat;
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.AmplifierLabel);
            this.TopPanel.Controls.Add(this.BandNumberLabel);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Margin = new System.Windows.Forms.Padding(0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(365, 18);
            this.TopPanel.TabIndex = 24;
            // 
            // BandNumberLabel
            // 
            this.BandNumberLabel.AutoSize = true;
            this.BandNumberLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.BandNumberLabel.Location = new System.Drawing.Point(6, 3);
            this.BandNumberLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.BandNumberLabel.Name = "BandNumberLabel";
            this.BandNumberLabel.Size = new System.Drawing.Size(45, 13);
            this.BandNumberLabel.TabIndex = 25;
            this.BandNumberLabel.Text = "Полоса";
            this.BandNumberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MiddlePanel
            // 
            this.MiddlePanel.Controls.Add(this.TotalGainLevel);
            this.MiddlePanel.Controls.Add(this.ConstAttenuator);
            this.MiddlePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MiddlePanel.Location = new System.Drawing.Point(0, 18);
            this.MiddlePanel.Margin = new System.Windows.Forms.Padding(0);
            this.MiddlePanel.Name = "MiddlePanel";
            this.MiddlePanel.Size = new System.Drawing.Size(365, 18);
            this.MiddlePanel.TabIndex = 25;
            // 
            // ConstAttenuator
            // 
            this.ConstAttenuator.AutoSize = true;
            this.ConstAttenuator.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ConstAttenuator.Location = new System.Drawing.Point(6, 1);
            this.ConstAttenuator.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.ConstAttenuator.Name = "ConstAttenuator";
            this.ConstAttenuator.Size = new System.Drawing.Size(117, 17);
            this.ConstAttenuator.TabIndex = 20;
            this.ConstAttenuator.Text = "Аттенюатор 10 дБ";
            this.ConstAttenuator.UseVisualStyleBackColor = true;
            // 
            // BandSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.TableLayoutPanel);
            this.Name = "BandSettingsControl";
            this.Size = new System.Drawing.Size(365, 175);
            this.TableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AttenuatorTank)).EndInit();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.MiddlePanel.ResumeLayout(false);
            this.MiddlePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel;
        internal System.Windows.Forms.Label TotalGainLevel;
        internal System.Windows.Forms.Label AmplifierLabel;
        internal NationalInstruments.UI.WindowsForms.Tank AttenuatorTank;
        private System.Windows.Forms.Panel TopPanel;
        internal System.Windows.Forms.Label BandNumberLabel;
        private System.Windows.Forms.Panel MiddlePanel;
        internal System.Windows.Forms.CheckBox ConstAttenuator;

    }
}
