﻿namespace TechAppControls
{
    partial class TechTabControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.TopTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.PhasesControl = new TechAppControls.PhasesControl();
            this.LedGroup = new TechAppControls.LedGroup();
            this._techAppGraphControl = new TechAppControls.TechAppGraphControl();
            this.BottomPanel = new System.Windows.Forms.Panel();
            this.panelCalibrationSettings = new System.Windows.Forms.Panel();
            this.labelCalibrationInfo = new System.Windows.Forms.Label();
            this.buttonClearSelectedBands = new System.Windows.Forms.Button();
            this.buttonShowSelectedBands = new System.Windows.Forms.Button();
            this.CalibrationSelectedBandsRadioButton = new System.Windows.Forms.RadioButton();
            this.CalibrationCurrentBandRadioButton = new System.Windows.Forms.RadioButton();
            this.CalibrationFullRadioButton = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.AddSignalToCalibrationButton = new System.Windows.Forms.Button();
            this.DirectionLabel = new System.Windows.Forms.Label();
            this.DirectionTextbox = new System.Windows.Forms.TextBox();
            this.UsePhaseAveragingCheckbox = new System.Windows.Forms.CheckBox();
            this.RightBottomPanel2 = new System.Windows.Forms.Panel();
            this.buttonResetAttenuators = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.UpdateTimeTextBox = new System.Windows.Forms.TextBox();
            this.RightBottomPanel = new System.Windows.Forms.Panel();
            this.ExternalCalibrationRadioButton = new System.Windows.Forms.RadioButton();
            this.NoiseGeneratorRadioButton = new System.Windows.Forms.RadioButton();
            this.AirRadioButton = new System.Windows.Forms.RadioButton();
            this.SelectionWidthLabel = new System.Windows.Forms.Label();
            this.SelectionWidthTextBox = new System.Windows.Forms.TextBox();
            this.ControlsPanel = new System.Windows.Forms.Panel();
            this.SavePhasesButton = new System.Windows.Forms.Button();
            this.AveragingTextLabel = new System.Windows.Forms.Label();
            this.AveragingTextBox = new System.Windows.Forms.TextBox();
            this.AbsolutePhasesCheckBox = new System.Windows.Forms.CheckBox();
            this.BandSettingsControl = new TechAppControls.BandSettingsControl();
            this.CalibrationButton = new System.Windows.Forms.Button();
            this.NextBandButton = new System.Windows.Forms.Button();
            this.PrevBandButton = new System.Windows.Forms.Button();
            this.BandNumberTextBox = new System.Windows.Forms.TextBox();
            this.BandArrayControl = new TechAppControls.BandArrayControl();
            this.TabTableLayoutPanel.SuspendLayout();
            this.TopTableLayoutPanel.SuspendLayout();
            this.BottomPanel.SuspendLayout();
            this.panelCalibrationSettings.SuspendLayout();
            this.panel1.SuspendLayout();
            this.RightBottomPanel2.SuspendLayout();
            this.RightBottomPanel.SuspendLayout();
            this.ControlsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabTableLayoutPanel
            // 
            this.TabTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TabTableLayoutPanel.ColumnCount = 1;
            this.TabTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TabTableLayoutPanel.Controls.Add(this.TopTableLayoutPanel, 0, 0);
            this.TabTableLayoutPanel.Controls.Add(this.BottomPanel, 0, 1);
            this.TabTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.TabTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.TabTableLayoutPanel.Name = "TabTableLayoutPanel";
            this.TabTableLayoutPanel.RowCount = 2;
            this.TabTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TabTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.TabTableLayoutPanel.Size = new System.Drawing.Size(1191, 782);
            this.TabTableLayoutPanel.TabIndex = 0;
            // 
            // TopTableLayoutPanel
            // 
            this.TopTableLayoutPanel.ColumnCount = 3;
            this.TopTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.TopTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TopTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.TopTableLayoutPanel.Controls.Add(this.PhasesControl, 2, 0);
            this.TopTableLayoutPanel.Controls.Add(this.LedGroup, 0, 0);
            this.TopTableLayoutPanel.Controls.Add(this._techAppGraphControl, 1, 0);
            this.TopTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TopTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.TopTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.TopTableLayoutPanel.Name = "TopTableLayoutPanel";
            this.TopTableLayoutPanel.RowCount = 1;
            this.TopTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TopTableLayoutPanel.Size = new System.Drawing.Size(1191, 672);
            this.TopTableLayoutPanel.TabIndex = 0;
            // 
            // PhasesControl
            // 
            this.PhasesControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PhasesControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PhasesControl.Location = new System.Drawing.Point(1111, 0);
            this.PhasesControl.Margin = new System.Windows.Forms.Padding(0);
            this.PhasesControl.Name = "PhasesControl";
            this.PhasesControl.Size = new System.Drawing.Size(80, 672);
            this.PhasesControl.TabIndex = 0;
            // 
            // LedGroup
            // 
            this.LedGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.LedGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LedGroup.ColorGenerator = null;
            this.LedGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LedGroup.Location = new System.Drawing.Point(0, 0);
            this.LedGroup.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.LedGroup.MinimumSize = new System.Drawing.Size(32, 2);
            this.LedGroup.Name = "LedGroup";
            this.LedGroup.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.LedGroup.Size = new System.Drawing.Size(33, 672);
            this.LedGroup.TabIndex = 1;
            // 
            // _techAppGraphControl
            // 
            this._techAppGraphControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._techAppGraphControl.DataRange = new NationalInstruments.UI.Range(25D, 3025D);
            this._techAppGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._techAppGraphControl.Location = new System.Drawing.Point(34, 0);
            this._techAppGraphControl.Margin = new System.Windows.Forms.Padding(0);
            this._techAppGraphControl.Name = "_techAppGraphControl";
            this._techAppGraphControl.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this._techAppGraphControl.SelectionEnabled = true;
            this._techAppGraphControl.SelectionWidth = 1000F;
            this._techAppGraphControl.Size = new System.Drawing.Size(1077, 672);
            this._techAppGraphControl.StickDataToVisibleRange = false;
            this._techAppGraphControl.TabIndex = 2;
            this._techAppGraphControl.VisibleRange = new NationalInstruments.UI.Range(0D, 10D);
            this._techAppGraphControl.Load += new System.EventHandler(this._techAppGraphControl_Load);
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.panelCalibrationSettings);
            this.BottomPanel.Controls.Add(this.panel1);
            this.BottomPanel.Controls.Add(this.RightBottomPanel2);
            this.BottomPanel.Controls.Add(this.RightBottomPanel);
            this.BottomPanel.Controls.Add(this.ControlsPanel);
            this.BottomPanel.Controls.Add(this.BandSettingsControl);
            this.BottomPanel.Controls.Add(this.CalibrationButton);
            this.BottomPanel.Controls.Add(this.NextBandButton);
            this.BottomPanel.Controls.Add(this.PrevBandButton);
            this.BottomPanel.Controls.Add(this.BandNumberTextBox);
            this.BottomPanel.Controls.Add(this.BandArrayControl);
            this.BottomPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BottomPanel.Location = new System.Drawing.Point(0, 672);
            this.BottomPanel.Margin = new System.Windows.Forms.Padding(0);
            this.BottomPanel.Name = "BottomPanel";
            this.BottomPanel.Size = new System.Drawing.Size(1191, 110);
            this.BottomPanel.TabIndex = 1;
            // 
            // panelCalibrationSettings
            // 
            this.panelCalibrationSettings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelCalibrationSettings.Controls.Add(this.labelCalibrationInfo);
            this.panelCalibrationSettings.Controls.Add(this.buttonClearSelectedBands);
            this.panelCalibrationSettings.Controls.Add(this.buttonShowSelectedBands);
            this.panelCalibrationSettings.Controls.Add(this.CalibrationSelectedBandsRadioButton);
            this.panelCalibrationSettings.Controls.Add(this.CalibrationCurrentBandRadioButton);
            this.panelCalibrationSettings.Controls.Add(this.CalibrationFullRadioButton);
            this.panelCalibrationSettings.Location = new System.Drawing.Point(343, 4);
            this.panelCalibrationSettings.Name = "panelCalibrationSettings";
            this.panelCalibrationSettings.Size = new System.Drawing.Size(243, 79);
            this.panelCalibrationSettings.TabIndex = 45;
            // 
            // labelCalibrationInfo
            // 
            this.labelCalibrationInfo.AutoSize = true;
            this.labelCalibrationInfo.Location = new System.Drawing.Point(146, 15);
            this.labelCalibrationInfo.Name = "labelCalibrationInfo";
            this.labelCalibrationInfo.Size = new System.Drawing.Size(71, 39);
            this.labelCalibrationInfo.TabIndex = 4;
            this.labelCalibrationInfo.Text = "Настройки\r\nкалибровки\r\nрадиотракта";
            // 
            // buttonClearSelectedBands
            // 
            this.buttonClearSelectedBands.Location = new System.Drawing.Point(134, 48);
            this.buttonClearSelectedBands.Name = "buttonClearSelectedBands";
            this.buttonClearSelectedBands.Size = new System.Drawing.Size(94, 22);
            this.buttonClearSelectedBands.TabIndex = 3;
            this.buttonClearSelectedBands.Text = "Очистить";
            this.buttonClearSelectedBands.UseVisualStyleBackColor = true;
            this.buttonClearSelectedBands.Visible = false;
            this.buttonClearSelectedBands.Click += new System.EventHandler(this.buttonClearSelectedBands_Click);
            // 
            // buttonShowSelectedBands
            // 
            this.buttonShowSelectedBands.Location = new System.Drawing.Point(118, 3);
            this.buttonShowSelectedBands.Name = "buttonShowSelectedBands";
            this.buttonShowSelectedBands.Size = new System.Drawing.Size(120, 37);
            this.buttonShowSelectedBands.TabIndex = 2;
            this.buttonShowSelectedBands.Text = "Показать полосы для калибровки";
            this.buttonShowSelectedBands.UseVisualStyleBackColor = true;
            this.buttonShowSelectedBands.Visible = false;
            this.buttonShowSelectedBands.Click += new System.EventHandler(this.buttonShowSelectedBands_Click);
            // 
            // CalibrationSelectedBandsRadioButton
            // 
            this.CalibrationSelectedBandsRadioButton.AutoSize = true;
            this.CalibrationSelectedBandsRadioButton.Location = new System.Drawing.Point(3, 51);
            this.CalibrationSelectedBandsRadioButton.Name = "CalibrationSelectedBandsRadioButton";
            this.CalibrationSelectedBandsRadioButton.Size = new System.Drawing.Size(125, 17);
            this.CalibrationSelectedBandsRadioButton.TabIndex = 1;
            this.CalibrationSelectedBandsRadioButton.Text = "Выбранные полосы";
            this.CalibrationSelectedBandsRadioButton.UseVisualStyleBackColor = true;
            this.CalibrationSelectedBandsRadioButton.CheckedChanged += new System.EventHandler(this.CalibrationSelectedBandsRadioButton_CheckedChanged);
            // 
            // CalibrationCurrentBandRadioButton
            // 
            this.CalibrationCurrentBandRadioButton.AutoSize = true;
            this.CalibrationCurrentBandRadioButton.Location = new System.Drawing.Point(3, 28);
            this.CalibrationCurrentBandRadioButton.Name = "CalibrationCurrentBandRadioButton";
            this.CalibrationCurrentBandRadioButton.Size = new System.Drawing.Size(109, 17);
            this.CalibrationCurrentBandRadioButton.TabIndex = 0;
            this.CalibrationCurrentBandRadioButton.Text = "Текущая полоса";
            this.CalibrationCurrentBandRadioButton.UseVisualStyleBackColor = true;
            // 
            // CalibrationFullRadioButton
            // 
            this.CalibrationFullRadioButton.AutoSize = true;
            this.CalibrationFullRadioButton.Checked = true;
            this.CalibrationFullRadioButton.Location = new System.Drawing.Point(3, 6);
            this.CalibrationFullRadioButton.Name = "CalibrationFullRadioButton";
            this.CalibrationFullRadioButton.Size = new System.Drawing.Size(85, 17);
            this.CalibrationFullRadioButton.TabIndex = 0;
            this.CalibrationFullRadioButton.TabStop = true;
            this.CalibrationFullRadioButton.Text = "Все полосы";
            this.CalibrationFullRadioButton.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.AddSignalToCalibrationButton);
            this.panel1.Controls.Add(this.DirectionLabel);
            this.panel1.Controls.Add(this.DirectionTextbox);
            this.panel1.Controls.Add(this.UsePhaseAveragingCheckbox);
            this.panel1.Location = new System.Drawing.Point(767, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(121, 79);
            this.panel1.TabIndex = 44;
            // 
            // AddSignalToCalibrationButton
            // 
            this.AddSignalToCalibrationButton.Location = new System.Drawing.Point(3, 51);
            this.AddSignalToCalibrationButton.Name = "AddSignalToCalibrationButton";
            this.AddSignalToCalibrationButton.Size = new System.Drawing.Size(112, 23);
            this.AddSignalToCalibrationButton.TabIndex = 44;
            this.AddSignalToCalibrationButton.Text = "Калибровать";
            this.AddSignalToCalibrationButton.UseVisualStyleBackColor = true;
            this.AddSignalToCalibrationButton.Click += new System.EventHandler(this.AddSignalToCalibrationButton_Click);
            // 
            // DirectionLabel
            // 
            this.DirectionLabel.AutoSize = true;
            this.DirectionLabel.Location = new System.Drawing.Point(44, 6);
            this.DirectionLabel.Name = "DirectionLabel";
            this.DirectionLabel.Size = new System.Drawing.Size(75, 13);
            this.DirectionLabel.TabIndex = 43;
            this.DirectionLabel.Text = "Направление";
            // 
            // DirectionTextbox
            // 
            this.DirectionTextbox.Location = new System.Drawing.Point(3, 3);
            this.DirectionTextbox.Name = "DirectionTextbox";
            this.DirectionTextbox.Size = new System.Drawing.Size(35, 20);
            this.DirectionTextbox.TabIndex = 42;
            this.DirectionTextbox.Text = "0";
            this.DirectionTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.DirectionTextbox.TextChanged += new System.EventHandler(this.DirectionTextbox_TextChanged);
            // 
            // UsePhaseAveragingCheckbox
            // 
            this.UsePhaseAveragingCheckbox.AutoSize = true;
            this.UsePhaseAveragingCheckbox.Location = new System.Drawing.Point(3, 29);
            this.UsePhaseAveragingCheckbox.Name = "UsePhaseAveragingCheckbox";
            this.UsePhaseAveragingCheckbox.Size = new System.Drawing.Size(112, 17);
            this.UsePhaseAveragingCheckbox.TabIndex = 41;
            this.UsePhaseAveragingCheckbox.Text = "Усреднять фазы";
            this.UsePhaseAveragingCheckbox.UseVisualStyleBackColor = true;
            // 
            // RightBottomPanel2
            // 
            this.RightBottomPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RightBottomPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RightBottomPanel2.Controls.Add(this.buttonResetAttenuators);
            this.RightBottomPanel2.Controls.Add(this.label1);
            this.RightBottomPanel2.Controls.Add(this.UpdateTimeTextBox);
            this.RightBottomPanel2.Location = new System.Drawing.Point(592, 4);
            this.RightBottomPanel2.Name = "RightBottomPanel2";
            this.RightBottomPanel2.Size = new System.Drawing.Size(169, 79);
            this.RightBottomPanel2.TabIndex = 40;
            // 
            // buttonResetAttenuators
            // 
            this.buttonResetAttenuators.Location = new System.Drawing.Point(3, 29);
            this.buttonResetAttenuators.Name = "buttonResetAttenuators";
            this.buttonResetAttenuators.Size = new System.Drawing.Size(161, 22);
            this.buttonResetAttenuators.TabIndex = 41;
            this.buttonResetAttenuators.Text = "Обнулить аттенюаторы";
            this.buttonResetAttenuators.UseVisualStyleBackColor = true;
            this.buttonResetAttenuators.Click += new System.EventHandler(this.buttonResetAttenuators_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 13);
            this.label1.TabIndex = 40;
            this.label1.Text = "Время обновления, мс";
            // 
            // UpdateTimeTextBox
            // 
            this.UpdateTimeTextBox.Location = new System.Drawing.Point(3, 3);
            this.UpdateTimeTextBox.Name = "UpdateTimeTextBox";
            this.UpdateTimeTextBox.Size = new System.Drawing.Size(35, 20);
            this.UpdateTimeTextBox.TabIndex = 39;
            this.UpdateTimeTextBox.Text = "50";
            this.UpdateTimeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.UpdateTimeTextBox.TextChanged += new System.EventHandler(this.UpdateTimeTextBox_TextChanged);
            // 
            // RightBottomPanel
            // 
            this.RightBottomPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RightBottomPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RightBottomPanel.Controls.Add(this.ExternalCalibrationRadioButton);
            this.RightBottomPanel.Controls.Add(this.NoiseGeneratorRadioButton);
            this.RightBottomPanel.Controls.Add(this.AirRadioButton);
            this.RightBottomPanel.Controls.Add(this.SelectionWidthLabel);
            this.RightBottomPanel.Controls.Add(this.SelectionWidthTextBox);
            this.RightBottomPanel.Location = new System.Drawing.Point(891, 4);
            this.RightBottomPanel.Name = "RightBottomPanel";
            this.RightBottomPanel.Size = new System.Drawing.Size(150, 79);
            this.RightBottomPanel.TabIndex = 39;
            // 
            // ExternalCalibrationRadioButton
            // 
            this.ExternalCalibrationRadioButton.AutoSize = true;
            this.ExternalCalibrationRadioButton.Location = new System.Drawing.Point(3, 60);
            this.ExternalCalibrationRadioButton.Name = "ExternalCalibrationRadioButton";
            this.ExternalCalibrationRadioButton.Size = new System.Drawing.Size(133, 17);
            this.ExternalCalibrationRadioButton.TabIndex = 43;
            this.ExternalCalibrationRadioButton.Text = "Внешняя калибровка";
            this.ExternalCalibrationRadioButton.UseVisualStyleBackColor = true;
            this.ExternalCalibrationRadioButton.CheckedChanged += new System.EventHandler(this.ExternalCalibrationRadioButton_CheckedChanged);
            // 
            // NoiseGeneratorRadioButton
            // 
            this.NoiseGeneratorRadioButton.AutoSize = true;
            this.NoiseGeneratorRadioButton.Location = new System.Drawing.Point(3, 42);
            this.NoiseGeneratorRadioButton.Name = "NoiseGeneratorRadioButton";
            this.NoiseGeneratorRadioButton.Size = new System.Drawing.Size(108, 17);
            this.NoiseGeneratorRadioButton.TabIndex = 42;
            this.NoiseGeneratorRadioButton.Text = "Генератор шума";
            this.NoiseGeneratorRadioButton.UseVisualStyleBackColor = true;
            this.NoiseGeneratorRadioButton.CheckedChanged += new System.EventHandler(this.NoiseGeneratorRadioButton_CheckedChanged);
            // 
            // AirRadioButton
            // 
            this.AirRadioButton.AutoSize = true;
            this.AirRadioButton.Checked = true;
            this.AirRadioButton.Location = new System.Drawing.Point(3, 24);
            this.AirRadioButton.Name = "AirRadioButton";
            this.AirRadioButton.Size = new System.Drawing.Size(52, 17);
            this.AirRadioButton.TabIndex = 41;
            this.AirRadioButton.TabStop = true;
            this.AirRadioButton.Text = "Эфир";
            this.AirRadioButton.UseVisualStyleBackColor = true;
            this.AirRadioButton.CheckedChanged += new System.EventHandler(this.AirRadioButton_CheckedChanged);
            // 
            // SelectionWidthLabel
            // 
            this.SelectionWidthLabel.AutoSize = true;
            this.SelectionWidthLabel.Location = new System.Drawing.Point(41, 6);
            this.SelectionWidthLabel.Name = "SelectionWidthLabel";
            this.SelectionWidthLabel.Size = new System.Drawing.Size(105, 13);
            this.SelectionWidthLabel.TabIndex = 40;
            this.SelectionWidthLabel.Text = "Ширина выделения";
            // 
            // SelectionWidthTextBox
            // 
            this.SelectionWidthTextBox.Location = new System.Drawing.Point(3, 3);
            this.SelectionWidthTextBox.Name = "SelectionWidthTextBox";
            this.SelectionWidthTextBox.Size = new System.Drawing.Size(35, 20);
            this.SelectionWidthTextBox.TabIndex = 39;
            this.SelectionWidthTextBox.Text = "1000";
            this.SelectionWidthTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.SelectionWidthTextBox.TextChanged += new System.EventHandler(this.SelectionWidthTextBox_TextChanged);
            // 
            // ControlsPanel
            // 
            this.ControlsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ControlsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ControlsPanel.Controls.Add(this.SavePhasesButton);
            this.ControlsPanel.Controls.Add(this.AveragingTextLabel);
            this.ControlsPanel.Controls.Add(this.AveragingTextBox);
            this.ControlsPanel.Controls.Add(this.AbsolutePhasesCheckBox);
            this.ControlsPanel.Location = new System.Drawing.Point(1044, 4);
            this.ControlsPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ControlsPanel.Name = "ControlsPanel";
            this.ControlsPanel.Size = new System.Drawing.Size(147, 79);
            this.ControlsPanel.TabIndex = 37;
            // 
            // SavePhasesButton
            // 
            this.SavePhasesButton.Location = new System.Drawing.Point(3, 51);
            this.SavePhasesButton.Name = "SavePhasesButton";
            this.SavePhasesButton.Size = new System.Drawing.Size(139, 23);
            this.SavePhasesButton.TabIndex = 38;
            this.SavePhasesButton.Text = "Сохранить фазы";
            this.SavePhasesButton.UseVisualStyleBackColor = true;
            this.SavePhasesButton.Click += new System.EventHandler(this.SavePhasesButton_Click);
            // 
            // AveragingTextLabel
            // 
            this.AveragingTextLabel.AutoSize = true;
            this.AveragingTextLabel.Location = new System.Drawing.Point(44, 6);
            this.AveragingTextLabel.Name = "AveragingTextLabel";
            this.AveragingTextLabel.Size = new System.Drawing.Size(101, 13);
            this.AveragingTextLabel.TabIndex = 38;
            this.AveragingTextLabel.Text = "Число усреднений";
            // 
            // AveragingTextBox
            // 
            this.AveragingTextBox.Location = new System.Drawing.Point(3, 3);
            this.AveragingTextBox.Name = "AveragingTextBox";
            this.AveragingTextBox.Size = new System.Drawing.Size(35, 20);
            this.AveragingTextBox.TabIndex = 37;
            this.AveragingTextBox.Text = "1";
            this.AveragingTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.AveragingTextBox.TextChanged += new System.EventHandler(this.AveragingTextBox_TextChanged);
            // 
            // AbsolutePhasesCheckBox
            // 
            this.AbsolutePhasesCheckBox.AutoSize = true;
            this.AbsolutePhasesCheckBox.Location = new System.Drawing.Point(3, 29);
            this.AbsolutePhasesCheckBox.Name = "AbsolutePhasesCheckBox";
            this.AbsolutePhasesCheckBox.Size = new System.Drawing.Size(121, 17);
            this.AbsolutePhasesCheckBox.TabIndex = 36;
            this.AbsolutePhasesCheckBox.Text = "Абсолютные фазы";
            this.AbsolutePhasesCheckBox.UseVisualStyleBackColor = true;
            // 
            // BandSettingsControl
            // 
            this.BandSettingsControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BandSettingsControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BandSettingsControl.Client = null;
            this.BandSettingsControl.Location = new System.Drawing.Point(112, 4);
            this.BandSettingsControl.Name = "BandSettingsControl";
            this.BandSettingsControl.Size = new System.Drawing.Size(225, 78);
            this.BandSettingsControl.TabIndex = 34;
            // 
            // CalibrationButton
            // 
            this.CalibrationButton.Location = new System.Drawing.Point(3, 30);
            this.CalibrationButton.Name = "CalibrationButton";
            this.CalibrationButton.Size = new System.Drawing.Size(103, 52);
            this.CalibrationButton.TabIndex = 28;
            this.CalibrationButton.Text = "Калибровать";
            this.CalibrationButton.UseVisualStyleBackColor = true;
            this.CalibrationButton.Click += new System.EventHandler(this.OnCalibrationButtonClick);
            // 
            // NextBandButton
            // 
            this.NextBandButton.Location = new System.Drawing.Point(78, 4);
            this.NextBandButton.Name = "NextBandButton";
            this.NextBandButton.Size = new System.Drawing.Size(28, 20);
            this.NextBandButton.TabIndex = 31;
            this.NextBandButton.Text = ">";
            this.NextBandButton.UseVisualStyleBackColor = true;
            this.NextBandButton.Click += new System.EventHandler(this.OnNextBandButtonClick);
            // 
            // PrevBandButton
            // 
            this.PrevBandButton.Location = new System.Drawing.Point(3, 4);
            this.PrevBandButton.Name = "PrevBandButton";
            this.PrevBandButton.Size = new System.Drawing.Size(28, 20);
            this.PrevBandButton.TabIndex = 30;
            this.PrevBandButton.Text = "<";
            this.PrevBandButton.UseVisualStyleBackColor = true;
            this.PrevBandButton.Click += new System.EventHandler(this.OnPrevBandButtonClick);
            // 
            // BandNumberTextBox
            // 
            this.BandNumberTextBox.Location = new System.Drawing.Point(37, 4);
            this.BandNumberTextBox.Name = "BandNumberTextBox";
            this.BandNumberTextBox.Size = new System.Drawing.Size(35, 20);
            this.BandNumberTextBox.TabIndex = 29;
            this.BandNumberTextBox.Text = "1";
            this.BandNumberTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.BandNumberTextBox.TextChanged += new System.EventHandler(this.OnBandNumberChanged);
            // 
            // BandArrayControl
            // 
            this.BandArrayControl.ActiveBandIndex = 0;
            this.BandArrayControl.BandCount = 1;
            this.BandArrayControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BandArrayControl.Location = new System.Drawing.Point(0, 88);
            this.BandArrayControl.Margin = new System.Windows.Forms.Padding(0);
            this.BandArrayControl.Name = "BandArrayControl";
            this.BandArrayControl.Size = new System.Drawing.Size(1191, 22);
            this.BandArrayControl.TabIndex = 0;
            // 
            // TechTabControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TabTableLayoutPanel);
            this.Name = "TechTabControl";
            this.Size = new System.Drawing.Size(1191, 782);
            this.TabTableLayoutPanel.ResumeLayout(false);
            this.TopTableLayoutPanel.ResumeLayout(false);
            this.BottomPanel.ResumeLayout(false);
            this.BottomPanel.PerformLayout();
            this.panelCalibrationSettings.ResumeLayout(false);
            this.panelCalibrationSettings.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.RightBottomPanel2.ResumeLayout(false);
            this.RightBottomPanel2.PerformLayout();
            this.RightBottomPanel.ResumeLayout(false);
            this.RightBottomPanel.PerformLayout();
            this.ControlsPanel.ResumeLayout(false);
            this.ControlsPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TabTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel TopTableLayoutPanel;
        private PhasesControl PhasesControl;
        private LedGroup LedGroup;
        private TechAppGraphControl _techAppGraphControl;
        private System.Windows.Forms.Panel BottomPanel;
        private BandArrayControl BandArrayControl;
        internal System.Windows.Forms.Button CalibrationButton;
        internal System.Windows.Forms.Button NextBandButton;
        internal System.Windows.Forms.Button PrevBandButton;
        internal System.Windows.Forms.TextBox BandNumberTextBox;
        private BandSettingsControl BandSettingsControl;
        internal System.Windows.Forms.CheckBox AbsolutePhasesCheckBox;
        private System.Windows.Forms.Panel ControlsPanel;
        internal System.Windows.Forms.TextBox AveragingTextBox;
        private System.Windows.Forms.Label AveragingTextLabel;
        private System.Windows.Forms.Button SavePhasesButton;
        private System.Windows.Forms.Panel RightBottomPanel;
        private System.Windows.Forms.Label SelectionWidthLabel;
        internal System.Windows.Forms.TextBox SelectionWidthTextBox;
        private System.Windows.Forms.RadioButton ExternalCalibrationRadioButton;
        private System.Windows.Forms.RadioButton NoiseGeneratorRadioButton;
        private System.Windows.Forms.RadioButton AirRadioButton;
        private System.Windows.Forms.Panel RightBottomPanel2;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TextBox UpdateTimeTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button AddSignalToCalibrationButton;
        private System.Windows.Forms.Label DirectionLabel;
        internal System.Windows.Forms.TextBox DirectionTextbox;
        internal System.Windows.Forms.CheckBox UsePhaseAveragingCheckbox;
        private System.Windows.Forms.Button buttonResetAttenuators;
        private System.Windows.Forms.Panel panelCalibrationSettings;
        private System.Windows.Forms.RadioButton CalibrationSelectedBandsRadioButton;
        private System.Windows.Forms.RadioButton CalibrationCurrentBandRadioButton;
        private System.Windows.Forms.RadioButton CalibrationFullRadioButton;
        private System.Windows.Forms.Button buttonClearSelectedBands;
        private System.Windows.Forms.Button buttonShowSelectedBands;
        private System.Windows.Forms.Label labelCalibrationInfo;
    }
}
