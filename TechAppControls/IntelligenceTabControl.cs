﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using DspDataModel;
using DspDataModel.Tasks;
using NationalInstruments.UI;
using Protocols;
using Settings;
using SharpExtensions;

namespace TechAppControls
{
    public partial class IntelligenceTabControl : UserControl, ITab
    {
        public DspClient.DspClient Client;
        
        private int _threshold;

        public bool IsWorking { get { return _isRdfTaskRunning || _isRdfTableTaskRunning; } }

        private bool _isRdfTaskRunning = false;
        private bool _isRdfTableTaskRunning = false;

        public XYCursor ThresholdCursor;

        private List<RadioSource> _radioSources = new List<RadioSource>();

        public int StartRdfFrequencyKhz
        {
            get { return _startRdfFrequencyKhz; }
            set
            {
                _startRdfFrequencyKhz = value;
                UpdateTextBoxValue(StartRdfFrequency, StartRdfFrequencyKhz.ToString());
            }
        }

        public int EndRdfFrequencyKhz
        {
            get { return _endRdfFrequencyKhz; }
            set
            {
                _endRdfFrequencyKhz = value;
                UpdateTextBoxValue(EndRdfFrequency, EndRdfFrequencyKhz.ToString());
            }
        }

        public int Threshold
        {
            get { return _threshold; }
            set
            {
                _threshold = value.ToBounds((int) Constants.ReceiverMinAmplitude, 0);
                ThresholdCombobox.Text = Threshold.ToString();
                ThresholdCursor.YPosition = Threshold;
                ThresholdCursor.Visible = true;
                if (Client != null)
                {
                    Client.SetFilters(_threshold, 0, 0, TimeSpan.Zero);
                }
            }
        }

        public IntelligenceTabControl()
        {
            InitializeComponent();
            InitializeThrehsoldCursor();

            _techAppGraphControl.Graph.YAxes[0].Mode = AxisMode.Fixed;
            _techAppGraphControl.Graph.YAxes[0].Range = new Range(Constants.ReceiverMinAmplitude, 0);
            _techAppGraphControl.GraphClickEvent += OnTechAppGraphClicked;
            _techAppGraphControl.VisibleRangeChangedEvent += OnVisibleRangeChanged;
            _techAppGraphControl.VisibleRange = new Range(25, 55);
            _techAppGraphControl.DataRange = new Range(Constants.FirstBandMinKhz / 1000, Config.Instance.BandSettings.LastBandMaxKhz / 1000);

            Threshold = -80;
            InitializeDataGridView();
        }

        void OnVisibleRangeChanged(object sender, EventArgs e)
        {
            StartRdfFrequencyKhz = (int) (_techAppGraphControl.VisibleRange.Minimum * 1000);
            EndRdfFrequencyKhz = (int) (_techAppGraphControl.VisibleRange.Maximum * 1000);
            UpdateSector();
        }

        private void InitializeThrehsoldCursor()
        {
            ThresholdCursor = new XYCursor();

            ThresholdCursor.BeginInit();

            ThresholdCursor.Color = Color.Fuchsia;
            ThresholdCursor.LabelDisplay = XYCursorLabelDisplay.ShowY;
            ThresholdCursor.Plot = _techAppGraphControl.Graph.Plots[0];
            ThresholdCursor.PointSize = new Size(0, 0);
            ThresholdCursor.PointStyle = PointStyle.Cross;
            ThresholdCursor.SnapMode = CursorSnapMode.Floating;
            ThresholdCursor.VerticalCrosshairMode = CursorCrosshairMode.None;
            ThresholdCursor.AfterMove += OnThresholdCursorMoved;

            ThresholdCursor.EndInit();

            _techAppGraphControl.Graph.Cursors.Add(ThresholdCursor);
        }

        private void OnThresholdCursorMoved(object sender, AfterMoveXYCursorEventArgs e)
        {
            Threshold = (int)e.YPosition;
        }

        private void InitializeDataGridView()
        {
            typeof(DataGridView).InvokeMember(
                "DoubleBuffered",
                BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
                null,
                RadioSourceTable,
                new object[] { true });
            RadioSourceTable.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing; 
            RadioSourceTable.RowHeadersVisible = false;
        }

        int GetPointCount()
        {
            var pointCount = 2048;
            if (EndRdfFrequencyKhz - StartRdfFrequencyKhz < pointCount * Constants.SamplesGapKhz)
            {
                pointCount = (int)((EndRdfFrequencyKhz - StartRdfFrequencyKhz) / Constants.SamplesGapKhz);
                if (pointCount == 0)
                    pointCount = 1;
            }
            return pointCount;
        }

        public void StopTabWork()
        {
            _isRdfTableTaskRunning = false;
            _isRdfTaskRunning = false;
            if (Client != null)
            {
                Client.SetMode(DspServerMode.Stop);
            }
        }

        public async void StartTabWork()
        {
            if (Client == null)
            {
                return;
            }
            var range = new[] { new RangeSector(StartRdfFrequencyKhz * 10, EndRdfFrequencyKhz * 10, 0, 3600) };
            
            await Client.SetSectorsAndRanges(range);
            await Client.SetMode(DfCheckbox.Checked ? DspServerMode.RadioIntelligenceWithDf : DspServerMode.RadioIntelligence);

            if (!_isRdfTableTaskRunning)
            {
                RdfTableTask();
            }
            if (!_isRdfTaskRunning)
            {
                SpectrumTask();
            }
        }

        private async Task UpdateSector()
        {
            if (StartRdfFrequencyKhz < EndRdfFrequencyKhz)
            {
                await Client.SetSectorsAndRanges(new[] { new RangeSector(StartRdfFrequencyKhz * 10, EndRdfFrequencyKhz * 10, 0, 3600) });
            }
        }

        private async void DfCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            await Client.SetMode(DfCheckbox.Checked ? DspServerMode.RadioIntelligenceWithDf : DspServerMode.RadioIntelligence);
        }

        private void PrevButtonIntelligence_Click(object sender, EventArgs e)
        {
            var bandNumber = Utilities.GetBandNumber((StartRdfFrequencyKhz + EndRdfFrequencyKhz) / 2);
            if (bandNumber > 0)
            {
                UpdateBandNumber(bandNumber - 1);
            }
        }

        private void UpdateBandNumber(int bandNumber)
        {
            StartRdfFrequencyKhz = Utilities.GetBandMinFrequencyKhz(bandNumber);
            EndRdfFrequencyKhz = Utilities.GetBandMaxFrequencyKhz(bandNumber);
            UpdateSector();
            UpdateSpectrumAxis();
        }

        private void NextButtonIntelligence_Click(object sender, EventArgs e)
        {
            var bandNumber = Utilities.GetBandNumber((StartRdfFrequencyKhz + EndRdfFrequencyKhz) / 2);
            if (bandNumber < Config.Instance.BandSettings.BandCount - 1)
            {
                UpdateBandNumber(bandNumber + 1);
            }
        }

        private void UpdateSpectrumAxis()
        {
            if (StartRdfFrequencyKhz < EndRdfFrequencyKhz)
            {
                _techAppGraphControl.VisibleRange = new Range(StartRdfFrequencyKhz / 1000d, EndRdfFrequencyKhz / 1000d);
            }
        }

        private void StartRdfFrequency_TextChanged(object sender, EventArgs e)
        {
            int frequency;
            if (UpdateTextBoxValue(StartRdfFrequency, out frequency))
            {
                frequency = frequency.ToBounds(Constants.FirstBandMinKhz, Utilities.GetBandMaxFrequencyKhz(Config.Instance.BandSettings.BandCount - 1));
                if (frequency < EndRdfFrequencyKhz)
                {
                    StartRdfFrequencyKhz = frequency;
                    if (_supressSectorUpdate)
                    {
                        return;
                    }
                    UpdateSector();
                    UpdateSpectrumAxis();
                }
            }
        }

        private bool _supressSectorUpdate = false;
        private int _startRdfFrequencyKhz;
        private int _endRdfFrequencyKhz;

        private void UpdateTextBoxValue(TextBox textBox, string value)
        {
            _supressSectorUpdate = true;
            textBox.Text = value;
            _supressSectorUpdate = true;
        }

        private void EndRdfFrequency_TextChanged(object sender, EventArgs e)
        {
            int frequency;
            if (UpdateTextBoxValue(EndRdfFrequency, out frequency))
            {
                if (StartRdfFrequencyKhz < frequency)
                {
                    EndRdfFrequencyKhz = frequency;
                    if (_supressSectorUpdate)
                    {
                        return;
                    }
                    UpdateSector();
                    UpdateSpectrumAxis();
                }
            }
        }

        private bool UpdateTextBoxValue(TextBox textBox, out int frequency)
        {
            if (int.TryParse(textBox.Text, out frequency))
            {
                frequency = frequency.ToBounds(Constants.FirstBandMinKhz, Utilities.GetBandMaxFrequencyKhz(Config.Instance.BandSettings.BandCount - 1));
                return true;
            }
            return false;
        }

        private void Threshold_TextChanged(object sender, EventArgs e)
        {
            int threshold;
            if (int.TryParse(ThresholdCombobox.Text, out threshold))
            {
                threshold = threshold.ToBounds((int) Constants.ReceiverMinAmplitude, 0);
                _threshold = threshold;
                ThresholdCursor.YPosition = Threshold;
            }
        }


        private async Task SpectrumTask()
        {
            if (_isRdfTaskRunning)
            {
                return;
            }
            try
            {
                _isRdfTaskRunning = true;
                while (_isRdfTaskRunning)
                {
                    var pointCount = GetPointCount();
                    var spectrum = await Client.GetSpectrum(StartRdfFrequencyKhz, EndRdfFrequencyKhz, pointCount);
                    if (spectrum == null)
                    {
                        _isRdfTaskRunning = false;
                        return;
                    }
                  UpdateSpectrum(spectrum);

                  await Task.Delay(100);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                MessageBox.Show(e.StackTrace);
                _isRdfTaskRunning = false;
                throw;
            }
            _isRdfTaskRunning = false;
        }

        private async Task RdfTableTask()
        {
            if (_isRdfTableTaskRunning)
            {
                return;
            }
            try
            {
                _isRdfTableTaskRunning = true;
                while (_isRdfTableTaskRunning)
                {
                    var newSources = await Client.GetRadioSources();

                    if (newSources == null)
                    {
                        _isRdfTableTaskRunning = false;
                        return;
                    }
                    UpdateRadioSources(newSources);
                    UpdateRadioSourceTable(_radioSources);

                    await Task.Delay(500);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                MessageBox.Show(e.StackTrace);
                _isRdfTableTaskRunning = false;
                throw;
            }
            _isRdfTableTaskRunning = false;
        }

        private void UpdateRadioSources(RadioSource[] newSources)
        {
            foreach (var source in newSources)
            {
                var index = _radioSources.FindIndex(rs => rs.Id == source.Id);
                if (index != -1)
                {
                    _radioSources[index] = source;
                }
                else
                {
                    _radioSources.Add(source);
                }
            }
            _radioSources.Sort((r1, r2) => r1.Frequency.CompareTo(r2.Frequency));
        }

        private void UpdateSpectrum(byte[] spectrum)
        {
            var doubleSpectrum = new double[spectrum.Length];

            for (var i = 0; i < spectrum.Length; ++i)
            {
                doubleSpectrum[i] = Constants.ReceiverMinAmplitude + spectrum[i];
            }
            _techAppGraphControl.Plot(doubleSpectrum);
        }

        private void UpdateRadioSourceTable(IReadOnlyList<RadioSource> radioSources)
        {
            RadioSourceTable.SuspendLayout();

            while (radioSources.Count < RadioSourceTable.RowCount)
            {
                RadioSourceTable.Rows.RemoveAt(RadioSourceTable.RowCount - 1);
            }
            if (radioSources.Count > RadioSourceTable.RowCount)
            {
                RadioSourceTable.Rows.Add(radioSources.Count - RadioSourceTable.RowCount);
            }
            for (var i = 0; i < radioSources.Count; i++)
            {
                var radioSource = radioSources[i];
                var row = RadioSourceTable.Rows[i];
                row.DefaultCellStyle.BackColor = radioSource.IsNew ? Color.Aqua : Color.White;
                row.DefaultCellStyle.ForeColor = radioSource.IsActive ? Color.Black : Color.Gray;
                row.Cells[0].Value = radioSource.Frequency / 10;
                row.Cells[1].Value = radioSource.Direction / 10;
                row.Cells[2].Value = radioSource.StandardDeviation / 10;
                row.Cells[3].Value = radioSource.Bandwidth / 10;
                row.Cells[4].Value = radioSource.Time.Hour + ":" + radioSource.Time.Minute.ToString("D2") + ":" + radioSource.Time.Second.ToString("D2");
                row.Cells[5].Value = -radioSource.Amplitude;
            }
            RadioSourceTable.ResumeLayout();
        }

        async void OnTechAppGraphClicked(object sender, float frequencyKhz)
        {
            var width = (float) _techAppGraphControl.VisibleRange.Interval * 15;
            _techAppGraphControl.SelectionWidth = width;

            var startFrequency = (int) (frequencyKhz - width);
            var endFrequency = (int) (frequencyKhz + width);

            if (startFrequency < Constants.FirstBandMinKhz)
            {
                startFrequency = Constants.FirstBandMinKhz;
            }
            else if (endFrequency >= Config.Instance.BandSettings.LastBandMaxKhz)
            {
                endFrequency = Config.Instance.BandSettings.LastBandMaxKhz;
            }
            var dfResult = await Client.ExecutiveDfRequest(startFrequency, endFrequency, 10, 1);
            
            ApplyExecutiveDfResult(dfResult);
        }

        private void ApplyExecutiveDfResult(ExecutiveDFResponse dfResult)
        {
            if (dfResult == null)
            {
                return;
            }
            var correlationHostogram = dfResult.CorrelationHistogram.Select(d => d / 256d).ToArray();
            var direction = dfResult.Direction / 10f;

            CorrelationCurveChart.PlotY(correlationHostogram);
            DirectionControl.Value = direction;
        }

        private async void RadioSourceTable_DoubleClick(object sender, EventArgs e)
        {
            await Client.ClearStorage(StorageType.Frs);
            RadioSourceTable.Rows.Clear();
            _radioSources.Clear();
        }

        private async void CorrelationCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Client == null)
            {
                return;
            }
            await Client.SetCalibrationType(CorrelationCombobox.SelectedIndex);
        }
    }
}
