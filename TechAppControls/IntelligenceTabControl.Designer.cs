﻿namespace TechAppControls
{
    partial class IntelligenceTabControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.RadioSourceTable = new System.Windows.Forms.DataGridView();
            this.Frequency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Direction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StandardDeviation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Bandwidth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetectionTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amplitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LeftTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.LeftBottomPanel = new System.Windows.Forms.Panel();
            this.CorrelationCurveChart = new NationalInstruments.UI.WindowsForms.WaveformGraph();
            this.waveformPlot2 = new NationalInstruments.UI.WaveformPlot();
            this.xAxis = new NationalInstruments.UI.XAxis();
            this.yAxis2 = new NationalInstruments.UI.YAxis();
            this.NextButtonIntelligence = new System.Windows.Forms.Button();
            this.PrevButtonIntelligence = new System.Windows.Forms.Button();
            this.DfCheckbox = new System.Windows.Forms.CheckBox();
            this.ThresholdLabel = new System.Windows.Forms.Label();
            this.ThresholdCombobox = new System.Windows.Forms.TextBox();
            this.EndRdfFrequency = new System.Windows.Forms.TextBox();
            this.StartRdfFrequency = new System.Windows.Forms.TextBox();
            this.ToFrequencyLabel = new System.Windows.Forms.Label();
            this.FromFrequencyLabel = new System.Windows.Forms.Label();
            this.CorrelationCombobox = new System.Windows.Forms.ComboBox();
            this.DirectionControl = new TechAppControls.PhaseControl();
            this._techAppGraphControl = new TechAppControls.TechAppGraphControl();
            this.TabTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadioSourceTable)).BeginInit();
            this.LeftTableLayoutPanel.SuspendLayout();
            this.LeftBottomPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CorrelationCurveChart)).BeginInit();
            this.SuspendLayout();
            // 
            // TabTableLayoutPanel
            // 
            this.TabTableLayoutPanel.ColumnCount = 2;
            this.TabTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.TabTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.TabTableLayoutPanel.Controls.Add(this.RadioSourceTable, 1, 0);
            this.TabTableLayoutPanel.Controls.Add(this.LeftTableLayoutPanel, 0, 0);
            this.TabTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.TabTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.TabTableLayoutPanel.Name = "TabTableLayoutPanel";
            this.TabTableLayoutPanel.RowCount = 1;
            this.TabTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TabTableLayoutPanel.Size = new System.Drawing.Size(966, 726);
            this.TabTableLayoutPanel.TabIndex = 0;
            // 
            // RadioSourceTable
            // 
            this.RadioSourceTable.AllowUserToAddRows = false;
            this.RadioSourceTable.AllowUserToDeleteRows = false;
            this.RadioSourceTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.RadioSourceTable.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.RadioSourceTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RadioSourceTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Frequency,
            this.Direction,
            this.StandardDeviation,
            this.Bandwidth,
            this.DetectionTime,
            this.Amplitude});
            this.RadioSourceTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadioSourceTable.Location = new System.Drawing.Point(676, 0);
            this.RadioSourceTable.Margin = new System.Windows.Forms.Padding(0);
            this.RadioSourceTable.Name = "RadioSourceTable";
            this.RadioSourceTable.ReadOnly = true;
            this.RadioSourceTable.Size = new System.Drawing.Size(290, 726);
            this.RadioSourceTable.TabIndex = 1;
            this.RadioSourceTable.DoubleClick += new System.EventHandler(this.RadioSourceTable_DoubleClick);
            // 
            // Frequency
            // 
            this.Frequency.HeaderText = "Frequency";
            this.Frequency.Name = "Frequency";
            this.Frequency.ReadOnly = true;
            // 
            // Direction
            // 
            this.Direction.HeaderText = "Direction";
            this.Direction.Name = "Direction";
            this.Direction.ReadOnly = true;
            // 
            // StandardDeviation
            // 
            this.StandardDeviation.HeaderText = "SD";
            this.StandardDeviation.Name = "StandardDeviation";
            this.StandardDeviation.ReadOnly = true;
            // 
            // Bandwidth
            // 
            this.Bandwidth.HeaderText = "Bandwidth";
            this.Bandwidth.Name = "Bandwidth";
            this.Bandwidth.ReadOnly = true;
            // 
            // DetectionTime
            // 
            this.DetectionTime.HeaderText = "Time";
            this.DetectionTime.Name = "DetectionTime";
            this.DetectionTime.ReadOnly = true;
            // 
            // Amplitude
            // 
            this.Amplitude.HeaderText = "Amplitude";
            this.Amplitude.Name = "Amplitude";
            this.Amplitude.ReadOnly = true;
            // 
            // LeftTableLayoutPanel
            // 
            this.LeftTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.LeftTableLayoutPanel.ColumnCount = 1;
            this.LeftTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LeftTableLayoutPanel.Controls.Add(this.LeftBottomPanel, 0, 1);
            this.LeftTableLayoutPanel.Controls.Add(this._techAppGraphControl, 0, 0);
            this.LeftTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.LeftTableLayoutPanel.Name = "LeftTableLayoutPanel";
            this.LeftTableLayoutPanel.RowCount = 2;
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LeftTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.LeftTableLayoutPanel.Size = new System.Drawing.Size(676, 726);
            this.LeftTableLayoutPanel.TabIndex = 2;
            // 
            // LeftBottomPanel
            // 
            this.LeftBottomPanel.Controls.Add(this.CorrelationCombobox);
            this.LeftBottomPanel.Controls.Add(this.DirectionControl);
            this.LeftBottomPanel.Controls.Add(this.CorrelationCurveChart);
            this.LeftBottomPanel.Controls.Add(this.NextButtonIntelligence);
            this.LeftBottomPanel.Controls.Add(this.PrevButtonIntelligence);
            this.LeftBottomPanel.Controls.Add(this.DfCheckbox);
            this.LeftBottomPanel.Controls.Add(this.ThresholdLabel);
            this.LeftBottomPanel.Controls.Add(this.ThresholdCombobox);
            this.LeftBottomPanel.Controls.Add(this.EndRdfFrequency);
            this.LeftBottomPanel.Controls.Add(this.StartRdfFrequency);
            this.LeftBottomPanel.Controls.Add(this.ToFrequencyLabel);
            this.LeftBottomPanel.Controls.Add(this.FromFrequencyLabel);
            this.LeftBottomPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftBottomPanel.Location = new System.Drawing.Point(0, 636);
            this.LeftBottomPanel.Margin = new System.Windows.Forms.Padding(0);
            this.LeftBottomPanel.Name = "LeftBottomPanel";
            this.LeftBottomPanel.Size = new System.Drawing.Size(676, 90);
            this.LeftBottomPanel.TabIndex = 0;
            // 
            // CorrelationCurveChart
            // 
            this.CorrelationCurveChart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CorrelationCurveChart.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CorrelationCurveChart.InteractionMode = ((NationalInstruments.UI.GraphInteractionModes)(((NationalInstruments.UI.GraphInteractionModes.ZoomX | NationalInstruments.UI.GraphInteractionModes.ZoomY) 
            | NationalInstruments.UI.GraphInteractionModes.ZoomAroundPoint)));
            this.CorrelationCurveChart.Location = new System.Drawing.Point(366, 3);
            this.CorrelationCurveChart.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.CorrelationCurveChart.Name = "CorrelationCurveChart";
            this.CorrelationCurveChart.PlotAreaBorder = NationalInstruments.UI.Border.Dashed;
            this.CorrelationCurveChart.PlotAreaColor = System.Drawing.SystemColors.Control;
            this.CorrelationCurveChart.PlotLineColorGenerator = NationalInstruments.UI.FixedSetColorGenerator.Dark;
            this.CorrelationCurveChart.Plots.AddRange(new NationalInstruments.UI.WaveformPlot[] {
            this.waveformPlot2});
            this.CorrelationCurveChart.Size = new System.Drawing.Size(227, 84);
            this.CorrelationCurveChart.TabIndex = 37;
            this.CorrelationCurveChart.UseColorGenerator = true;
            this.CorrelationCurveChart.XAxes.AddRange(new NationalInstruments.UI.XAxis[] {
            this.xAxis});
            this.CorrelationCurveChart.YAxes.AddRange(new NationalInstruments.UI.YAxis[] {
            this.yAxis2});
            // 
            // waveformPlot2
            // 
            this.waveformPlot2.XAxis = this.xAxis;
            this.waveformPlot2.YAxis = this.yAxis2;
            // 
            // xAxis
            // 
            this.xAxis.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xAxis.CaptionVisible = false;
            this.xAxis.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.xAxis.OriginLineColor = System.Drawing.Color.White;
            this.xAxis.Position = NationalInstruments.UI.XAxisPosition.Top;
            this.xAxis.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.xAxis.Visible = false;
            // 
            // yAxis2
            // 
            this.yAxis2.AutoSpacing = false;
            this.yAxis2.InteractionMode = NationalInstruments.UI.ScaleInteractionMode.None;
            this.yAxis2.MajorDivisions.GridColor = System.Drawing.Color.DarkGray;
            this.yAxis2.MajorDivisions.GridLineStyle = NationalInstruments.UI.LineStyle.Dot;
            this.yAxis2.MajorDivisions.GridVisible = true;
            this.yAxis2.MajorDivisions.Interval = 0.2D;
            this.yAxis2.MajorDivisions.TickLength = 1F;
            this.yAxis2.MinorDivisions.Interval = 0.1D;
            this.yAxis2.Mode = NationalInstruments.UI.AxisMode.Fixed;
            this.yAxis2.Range = new NationalInstruments.UI.Range(0D, 1D);
            // 
            // NextButtonIntelligence
            // 
            this.NextButtonIntelligence.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.NextButtonIntelligence.Location = new System.Drawing.Point(162, 67);
            this.NextButtonIntelligence.Name = "NextButtonIntelligence";
            this.NextButtonIntelligence.Size = new System.Drawing.Size(28, 20);
            this.NextButtonIntelligence.TabIndex = 36;
            this.NextButtonIntelligence.Text = ">";
            this.NextButtonIntelligence.UseVisualStyleBackColor = true;
            this.NextButtonIntelligence.Click += new System.EventHandler(this.NextButtonIntelligence_Click);
            // 
            // PrevButtonIntelligence
            // 
            this.PrevButtonIntelligence.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PrevButtonIntelligence.Location = new System.Drawing.Point(128, 67);
            this.PrevButtonIntelligence.Name = "PrevButtonIntelligence";
            this.PrevButtonIntelligence.Size = new System.Drawing.Size(28, 20);
            this.PrevButtonIntelligence.TabIndex = 35;
            this.PrevButtonIntelligence.Text = "<";
            this.PrevButtonIntelligence.UseVisualStyleBackColor = true;
            this.PrevButtonIntelligence.Click += new System.EventHandler(this.PrevButtonIntelligence_Click);
            // 
            // DfCheckbox
            // 
            this.DfCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DfCheckbox.AutoSize = true;
            this.DfCheckbox.Checked = true;
            this.DfCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DfCheckbox.Location = new System.Drawing.Point(3, 3);
            this.DfCheckbox.Name = "DfCheckbox";
            this.DfCheckbox.Size = new System.Drawing.Size(102, 17);
            this.DfCheckbox.TabIndex = 34;
            this.DfCheckbox.Text = "Direction finding";
            this.DfCheckbox.UseVisualStyleBackColor = true;
            this.DfCheckbox.CheckedChanged += new System.EventHandler(this.DfCheckbox_CheckedChanged);
            // 
            // ThresholdLabel
            // 
            this.ThresholdLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ThresholdLabel.AutoSize = true;
            this.ThresholdLabel.Location = new System.Drawing.Point(0, 70);
            this.ThresholdLabel.Name = "ThresholdLabel";
            this.ThresholdLabel.Size = new System.Drawing.Size(54, 13);
            this.ThresholdLabel.TabIndex = 33;
            this.ThresholdLabel.Text = "Threshold";
            // 
            // ThresholdCombobox
            // 
            this.ThresholdCombobox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ThresholdCombobox.Location = new System.Drawing.Point(59, 67);
            this.ThresholdCombobox.Name = "ThresholdCombobox";
            this.ThresholdCombobox.Size = new System.Drawing.Size(63, 20);
            this.ThresholdCombobox.TabIndex = 32;
            this.ThresholdCombobox.TextChanged += new System.EventHandler(this.Threshold_TextChanged);
            // 
            // EndRdfFrequency
            // 
            this.EndRdfFrequency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EndRdfFrequency.Location = new System.Drawing.Point(59, 45);
            this.EndRdfFrequency.Name = "EndRdfFrequency";
            this.EndRdfFrequency.Size = new System.Drawing.Size(63, 20);
            this.EndRdfFrequency.TabIndex = 29;
            this.EndRdfFrequency.TextChanged += new System.EventHandler(this.EndRdfFrequency_TextChanged);
            // 
            // StartRdfFrequency
            // 
            this.StartRdfFrequency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.StartRdfFrequency.Location = new System.Drawing.Point(59, 23);
            this.StartRdfFrequency.Name = "StartRdfFrequency";
            this.StartRdfFrequency.Size = new System.Drawing.Size(63, 20);
            this.StartRdfFrequency.TabIndex = 28;
            this.StartRdfFrequency.TextChanged += new System.EventHandler(this.StartRdfFrequency_TextChanged);
            // 
            // ToFrequencyLabel
            // 
            this.ToFrequencyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ToFrequencyLabel.AutoSize = true;
            this.ToFrequencyLabel.Location = new System.Drawing.Point(0, 48);
            this.ToFrequencyLabel.Name = "ToFrequencyLabel";
            this.ToFrequencyLabel.Size = new System.Drawing.Size(46, 13);
            this.ToFrequencyLabel.TabIndex = 31;
            this.ToFrequencyLabel.Text = "To, KHz";
            // 
            // FromFrequencyLabel
            // 
            this.FromFrequencyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FromFrequencyLabel.AutoSize = true;
            this.FromFrequencyLabel.Location = new System.Drawing.Point(0, 26);
            this.FromFrequencyLabel.Name = "FromFrequencyLabel";
            this.FromFrequencyLabel.Size = new System.Drawing.Size(56, 13);
            this.FromFrequencyLabel.TabIndex = 30;
            this.FromFrequencyLabel.Text = "From, KHz";
            // 
            // CorrelationCombobox
            // 
            this.CorrelationCombobox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CorrelationCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CorrelationCombobox.FormattingEnabled = true;
            this.CorrelationCombobox.Items.AddRange(new object[] {
            "Correlation",
            "LinearDeviation",
            "StandardDeviation",
            "QuadroDeviation",
            "QuadroFactorDeviation",
            "FactorQuadroCorrelation"});
            this.CorrelationCombobox.Location = new System.Drawing.Point(242, 66);
            this.CorrelationCombobox.Name = "CorrelationCombobox";
            this.CorrelationCombobox.Size = new System.Drawing.Size(121, 21);
            this.CorrelationCombobox.TabIndex = 39;
            this.CorrelationCombobox.SelectedIndexChanged += new System.EventHandler(this.CorrelationCombobox_SelectedIndexChanged);
            // 
            // DirectionControl
            // 
            this.DirectionControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DirectionControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.DirectionControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DirectionControl.ControlName = "Direction";
            this.DirectionControl.Location = new System.Drawing.Point(595, 3);
            this.DirectionControl.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.DirectionControl.Name = "DirectionControl";
            this.DirectionControl.Size = new System.Drawing.Size(79, 84);
            this.DirectionControl.TabIndex = 38;
            this.DirectionControl.Value = 0F;
            // 
            // _techAppGraphControl
            // 
            this._techAppGraphControl.DataRange = new NationalInstruments.UI.Range(25D, 3025D);
            this._techAppGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._techAppGraphControl.Location = new System.Drawing.Point(0, 0);
            this._techAppGraphControl.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this._techAppGraphControl.Name = "_techAppGraphControl";
            this._techAppGraphControl.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this._techAppGraphControl.SelectionEnabled = true;
            this._techAppGraphControl.SelectionWidth = 1000F;
            this._techAppGraphControl.Size = new System.Drawing.Size(674, 636);
            this._techAppGraphControl.StickDataToVisibleRange = true;
            this._techAppGraphControl.TabIndex = 1;
            this._techAppGraphControl.VisibleRange = null;
            // 
            // IntelligenceTabControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TabTableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "IntelligenceTabControl";
            this.Size = new System.Drawing.Size(966, 726);
            this.TabTableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadioSourceTable)).EndInit();
            this.LeftTableLayoutPanel.ResumeLayout(false);
            this.LeftBottomPanel.ResumeLayout(false);
            this.LeftBottomPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CorrelationCurveChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TabTableLayoutPanel;
        private System.Windows.Forms.DataGridView RadioSourceTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Frequency;
        private System.Windows.Forms.DataGridViewTextBoxColumn Direction;
        private System.Windows.Forms.DataGridViewTextBoxColumn StandardDeviation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bandwidth;
        private System.Windows.Forms.DataGridViewTextBoxColumn DetectionTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amplitude;
        private System.Windows.Forms.TableLayoutPanel LeftTableLayoutPanel;
        private System.Windows.Forms.Panel LeftBottomPanel;
        internal System.Windows.Forms.Button NextButtonIntelligence;
        internal System.Windows.Forms.Button PrevButtonIntelligence;
        private System.Windows.Forms.CheckBox DfCheckbox;
        private System.Windows.Forms.Label ThresholdLabel;
        private System.Windows.Forms.TextBox ThresholdCombobox;
        private System.Windows.Forms.TextBox EndRdfFrequency;
        private System.Windows.Forms.TextBox StartRdfFrequency;
        private System.Windows.Forms.Label ToFrequencyLabel;
        private System.Windows.Forms.Label FromFrequencyLabel;
        private PhaseControl DirectionControl;
        private NationalInstruments.UI.WindowsForms.WaveformGraph CorrelationCurveChart;
        private NationalInstruments.UI.WaveformPlot waveformPlot2;
        private NationalInstruments.UI.XAxis xAxis;
        private NationalInstruments.UI.YAxis yAxis2;
        private TechAppGraphControl _techAppGraphControl;
        private System.Windows.Forms.ComboBox CorrelationCombobox;
    }
}
