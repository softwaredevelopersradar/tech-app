﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace TechAppControls
{
    public partial class PhasesTextControl : UserControl
    {
        private IReadOnlyList<string> _names;

        public IReadOnlyList<string> Names
        {
            get { return _names; }
            set
            {
                if (value == null)
                {
                    return;
                }
                _names = value;
                for (var i = 0; i < _nameTextBoxs.Length; ++i)
                {
                    _nameTextBoxs[i].Text = _names[i];
                }
            }
        }

        public IReadOnlyList<float> Values { get { return _values; } }

        private readonly TextBox[] _nameTextBoxs;
        private readonly TextBox[] _valueTextBoxs;

        private readonly float[] _values;

        public PhasesTextControl()
        {
            InitializeComponent();
            _nameTextBoxs = new[]
            {
                textBox11, textBox12, textBox13, textBox14, textBox15,
                textBox16, textBox17, textBox18, textBox19, textBox20,
            };

            _valueTextBoxs = new[]
            {
                textBox1, textBox2, textBox3, textBox4, textBox5,
                textBox6, textBox7, textBox8, textBox9, textBox10,
            };
            _values = new float[10];
        }

        public void SetValue(int index, float value)
        {
            _values[index] = value;
            _valueTextBoxs[index].Text = ((int) value).ToString();
        }

        public void SetValues(IReadOnlyList<float> values)
        {
            for (var i = 0; i < 10; ++i)
            {
                SetValue(i, values[i]);
            }
        }
    }
}
