﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using Protocols;

namespace TechAppControls
{
    public partial class BandSettingsControl : UserControl
    {
        public DspClient.DspClient Client { get; set; }

        public float AmplifierLevel { get; private set; }

        public float AttenuatorLevel { get; private set; }

        public bool IsConstAttenuatorEnabled { get; private set; }

        public int BandNumber { get; private set; }

        private bool _isUserUpdate;

        public BandSettingsControl()
        {
            InitializeComponent();

            _isUserUpdate = true;

            AttenuatorTank.MouseUp += OnAttenuatorTankValueChanged;
            ConstAttenuator.CheckedChanged += OnConstAttenuatorCheckBoxCheckedChanged;
        }

        void OnAttenuatorTankValueChanged(object sender, MouseEventArgs e)
        {
            SetAttenuatorValue((float) AttenuatorTank.Value);
        }

        void OnConstAttenuatorCheckBoxCheckedChanged(object sender, System.EventArgs e)
        {
            SetConstAttenuatorEnabled(ConstAttenuator.Checked);
        }

        public void SetEnabled(bool isEnabled)
        {
            Enabled = isEnabled;
            if (!isEnabled)
            {
                BandNumberLabel.Text = "Полоса";
                AmplifierLabel.Text = "Усилитель:";
                TotalGainLevel.Text = "Всего:";
            }
        }

        public void SetBandNumber(int bandNumber)
        {
            BandNumber = bandNumber;
            BandNumberLabel.Text = "Полоса " + bandNumber;
        }

        public void SetAmplifierLevel(float level)
        {
            AmplifierLevel = level;
            AmplifierLabel.Text = "Усилитель: " + AmplifierLevel.ToString("F1") + " дБ";
            UpdateTotalGain();
        }

        public void SetConstAttenuatorEnabled(bool isEnabled)
        {
            IsConstAttenuatorEnabled = isEnabled;
            ConstAttenuator.Checked = IsConstAttenuatorEnabled;
            UpdateTotalGain();
            SetAttenuatorRequestIfNeeded();
        }

        public void SetAttenuatorValue(float level)
        {
            AttenuatorLevel = level;
            AttenuatorTank.Value = AttenuatorLevel;
            UpdateTotalGain();
            SetAttenuatorRequestIfNeeded();
        }

        private async Task SetAttenuatorRequestIfNeeded()
        {
            if (_isUserUpdate)
            {
                try
                {
                    await Client.SetAttenuatorsValue(BandNumber, AttenuatorLevel, IsConstAttenuatorEnabled);
                }
                catch (Exception)
                {
                    // ignored
                }
                
            }
        }

        private void UpdateTotalGain()
        {
            var totalGain = AmplifierLevel - AttenuatorLevel;
            if (IsConstAttenuatorEnabled)
            {
                totalGain -= 10;
            }
            TotalGainLevel.Text = "Всего: " + totalGain.ToString("F1") + " дБ";
        }

        public void Update(int bandNumber, AttenuatorSetting attenuatorSetting, byte amplifier)
        {
            _isUserUpdate = false;
            SetEnabled(true);
            SetBandNumber(bandNumber);
            SetAmplifierLevel(amplifier * 0.5f);
            SetAttenuatorValue(attenuatorSetting.AttenuatorValue * 0.5f);
            SetConstAttenuatorEnabled(attenuatorSetting.IsConstAttenuatorEnabled == 1);
            _isUserUpdate = true;
        }
    }
}
