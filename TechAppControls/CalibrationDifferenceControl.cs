﻿using System;
using System.IO;
using System.Windows.Forms;
using Correlator;
using DspDataModel;
using NationalInstruments.UI;
using Phases;

namespace TechAppControls
{
    public partial class CalibrationDifferenceControl: UserControl
    {
        private IVisualTable _table1;

        private IVisualTable _table2;

        private int _minFrequency;
        private int _maxFrequency;

        private bool _showTwoGraphs;

        private AntennaTableConfig[] _antennaSettings;

        private int _angle;

        public CalibrationDifferenceControl()
        {
            InitializeComponent();

            InitControlAxis(_differencesTechAppGraphControl, -180, 180);
            _differencesTechAppGraphControl.Graph.YAxes[0].Mode = AxisMode.AutoScaleExact;
            InitControlAxis(_techAppGraphControl1, 0, 360);
            InitControlAxis(_techAppGraphControl2, 0, 360);

            _techAppGraphControl1.VisibleRangeChangedEvent += OnGraph1VisibleRangeChanged;
            _techAppGraphControl2.VisibleRangeChangedEvent += OnGraph2VisibleRangeChanged;

            LedGroup.Initialize(_differencesTechAppGraphControl.Graph.PlotLineColorGenerator, new[]
            {
                "1-2", "1-3", "1-4", "1-5", "2-3", "2-4", "2-5", "3-4", "3-5", "4-5"
            });

            

            LedGroup.SetValue(false);
            LedGroup.SetEnabled(false);
            LedGroup.LedClickEvent += OnLedClick;
        }

        private void CalibrationDifferenceControl_Load(object sender, EventArgs e)
        {
            _antennaSettings = Config.Instance.TheoreticalTableSettings.AntennaTableSettings;

            RadiusTextBox1.Text = _antennaSettings[0].Radius.ToString("F5");
            RadiusTextBox2.Text = _antennaSettings[1].Radius.ToString("F5");
            RadiusTextBox3.Text = _antennaSettings[2].Radius.ToString("F5");
        }

        void OnGraph1VisibleRangeChanged(object sender, EventArgs e)
        {
            _techAppGraphControl2.VisibleRange = _techAppGraphControl1.VisibleRange;
        }

        void OnGraph2VisibleRangeChanged(object sender, EventArgs e)
        {
            _techAppGraphControl1.VisibleRange = _techAppGraphControl2.VisibleRange;
        }

        private void InitControlAxis(TechAppGraphControl techAppGraphControl, int startRange, int endRange)
        {
            techAppGraphControl.SelectionEnabled = false;
            techAppGraphControl.Graph.YAxes[0].Range = new Range(startRange, endRange);
            techAppGraphControl.Graph.YAxes[0].Mode = AxisMode.Fixed;
        }

        void OnLedClick(object sender, int index)
        {
            _differencesTechAppGraphControl.Graph.Plots[index].Visible = LedGroup.Leds[index].Value;

            _techAppGraphControl1.Graph.Plots[index].Visible = LedGroup.Leds[index].Value;
            _techAppGraphControl2.Graph.Plots[index].Visible = LedGroup.Leds[index].Value;
        }

        void ShowDifferences()
        {
            if (_table1 == null || _table2 == null)
            {
                return;
            }
            LedGroup.SetEnabled(true);
            LedGroup.SetValue(true);

            var valuesCount = Math.Max(_table1.ValuesCount, _table2.ValuesCount);
            var step = Math.Min(
                1f * (_table2.MaxFrequencyKhz - _table2.MinFrequencyKhz) / valuesCount,
                1f * (_table1.MaxFrequencyKhz - _table1.MinFrequencyKhz) / valuesCount
            );

            var data = new double[10, valuesCount];
            var data1 = new double[10, valuesCount];
            var data2 = new double[10, valuesCount];

            for (var i = 0; i < valuesCount; ++i)
            {
                var frequency = _minFrequency + i * step;
                var phases1 = _table1.FrequencyToPhases(frequency);
                var phases2 = _table2.FrequencyToPhases(frequency);

                for (var j = 0; j < 10; ++j)
                {
                    data[j, i] = PhaseMath.SignAngle(phases1[j], phases2[j]);
                    data1[j, i] = phases1[j];
                    data2[j, i] = phases2[j];
                }
            }
            _differencesTechAppGraphControl.Plot(data);
            _techAppGraphControl1.Plot(data1);
            _techAppGraphControl2.Plot(data2);
        }

        private void OpenCalibrationButton1_Click(object sender, EventArgs e)
        {
            _table1 = OpenCalibration();
            if (_table1 != null)
            {
                OpenCalibrationButton1.Text = Path.GetFileName(openFileDialog.FileName);
            }
            if (_table1 != null && _table2 != null)
            {
                ShowDifferences();
            }
        }

        private IVisualTable OpenCalibration()
        {
            var table = openFileDialog.ShowDialog() == DialogResult.OK 
                ? VisualTableExtensions.LoadTable(openFileDialog.FileName)
                : null;
            if (table != null)
            {
                var range = new Range(table.MinFrequencyKhz / 1000d, table.MaxFrequencyKhz / 1000d);
                _techAppGraphControl1.DataRange = range;
                _techAppGraphControl2.DataRange = range;
                _differencesTechAppGraphControl.DataRange = range;
            }
            return table;
        }

        private void OpenCalibrationButton2_Click(object sender, System.EventArgs e)
        {
            _table2 = OpenCalibration();
            if (_table2 != null)
            {
                OpenCalibrationButton2.Text = Path.GetFileName(openFileDialog.FileName);
            }
            if (_table1 != null && _table2 != null)
            {
                _minFrequency = Math.Min(_table1.MinFrequencyKhz, _table2.MinFrequencyKhz);
                _maxFrequency = Math.Min(_table1.MaxFrequencyKhz, _table2.MaxFrequencyKhz);
                ShowDifferences();
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void ButtonsPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void AngleTextBox_TextChanged(object sender, EventArgs e)
        {
            if (_table1 == null || _table2 == null)
            {
                return;
            }

            int angle;
            if (!int.TryParse(AngleTextBox.Text, out angle))
            {
                return;
            }
            angle = PhaseMath.NormalizePhase(angle);
            _angle = angle;
            _table1.CurrentAngle = angle;
            _table2.CurrentAngle = angle;
            ShowDifferences();
        }

        private void AngleLabel_Click(object sender, EventArgs e)
        {

        }

        private void LoadTheoreticalTable_Click(object sender, EventArgs e)
        {
            _table1 = new TheoreticalTable(Config.Instance.TheoreticalTableSettings).ToVisualTable();
            if (_table1 != null)
            {
                OpenCalibrationButton1.Text = "Theory table";
            }
            if (_table1 != null && _table2 != null)
            {
                ShowDifferences();
            }
        }

        private void GraphModeButton_Click(object sender, EventArgs e)
        {
            _showTwoGraphs = !_showTwoGraphs;

            OneGraphModePanel.Visible = !_showTwoGraphs;
            TwoGraphPanelMode.Visible = _showTwoGraphs;
        }

        private void RadiusTextBox1_TextChanged(object sender, EventArgs e)
        {
            RadiusTextBoxChanged(sender as TextBox, 0);
        }

        private void RadiusTextBox2_TextChanged(object sender, EventArgs e)
        {
            RadiusTextBoxChanged(sender as TextBox, 1);
        }

        private void RadiusTextBox3_TextChanged(object sender, EventArgs e)
        {
            RadiusTextBoxChanged(sender as TextBox, 2);
        }

        private void RadiusTextBoxChanged(TextBox textBox, int index)
        {
            if (_table1 == null || _table2 == null)
            {
                return;
            }

            var value = 0f;
            if (!float.TryParse(textBox.Text, out value))
            {
                return;
            }
            if (Math.Abs(value - _antennaSettings[index].Radius) < 1e-5)
            {
                return;
            }
            if (value <= 0 || value >= 100)
            {
                return;
            }
            _antennaSettings[index].Radius = value;
            _table1 = new TheoreticalTable(Config.Instance.TheoreticalTableSettings).ToVisualTable();
            _table1.CurrentAngle = _angle;
            if (_table1 != null && _table2 != null)
            {
                ShowDifferences();
            }
        }
    }
}
