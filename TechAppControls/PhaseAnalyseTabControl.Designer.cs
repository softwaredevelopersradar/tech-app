﻿namespace TechAppControls
{
    partial class PhaseAnalyseTabControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.TopPanel = new System.Windows.Forms.Panel();
            this.AveragingLabel = new System.Windows.Forms.Label();
            this.AveragingTextBox = new System.Windows.Forms.TextBox();
            this.BandNumberLabel = new System.Windows.Forms.Label();
            this.DirectionLabel = new System.Windows.Forms.Label();
            this.DirectionTextBox = new System.Windows.Forms.TextBox();
            this.AutoDirectionCheckBox = new System.Windows.Forms.CheckBox();
            this.PhaseControl = new TechAppControls.PhaseControl();
            this.phaseBox5 = new TechAppControls.PhaseBox();
            this.phaseBox3 = new TechAppControls.PhaseBox();
            this.phaseBox2 = new TechAppControls.PhaseBox();
            this.phaseBox4 = new TechAppControls.PhaseBox();
            this.phaseBox1 = new TechAppControls.PhaseBox();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.BandArrayControl = new TechAppControls.BandArrayControl();
            this._techAppGraphControl = new TechAppControls.TechAppGraphControl();
            this.RealPhasesTextControl = new TechAppControls.PhasesTextControl();
            this.RealPhasesLabel = new System.Windows.Forms.Label();
            this.TheoreticalPhasesLabel = new System.Windows.Forms.Label();
            this.TheoryPhasesTextControl = new TechAppControls.PhasesTextControl();
            this.DeltaPhasesLabel = new System.Windows.Forms.Label();
            this.DeltaPhasesTextControl = new TechAppControls.PhasesTextControl();
            this.RightPanel = new System.Windows.Forms.Panel();
            this.TopPanel.SuspendLayout();
            this.RightPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 267;
            this.lineShape1.X2 = 266;
            this.lineShape1.Y1 = 237;
            this.lineShape1.Y2 = 61;
            // 
            // TopPanel
            // 
            this.TopPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TopPanel.Controls.Add(this.AveragingLabel);
            this.TopPanel.Controls.Add(this.AveragingTextBox);
            this.TopPanel.Controls.Add(this.BandNumberLabel);
            this.TopPanel.Controls.Add(this.DirectionLabel);
            this.TopPanel.Controls.Add(this.DirectionTextBox);
            this.TopPanel.Controls.Add(this.AutoDirectionCheckBox);
            this.TopPanel.Controls.Add(this.PhaseControl);
            this.TopPanel.Controls.Add(this.phaseBox3);
            this.TopPanel.Controls.Add(this.phaseBox4);
            this.TopPanel.Controls.Add(this.phaseBox5);
            this.TopPanel.Controls.Add(this.phaseBox1);
            this.TopPanel.Controls.Add(this.phaseBox2);
            this.TopPanel.Controls.Add(this.shapeContainer2);
            this.TopPanel.Location = new System.Drawing.Point(3, 3);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(548, 448);
            this.TopPanel.TabIndex = 2;
            // 
            // AveragingLabel
            // 
            this.AveragingLabel.AutoSize = true;
            this.AveragingLabel.Location = new System.Drawing.Point(6, 426);
            this.AveragingLabel.Name = "AveragingLabel";
            this.AveragingLabel.Size = new System.Drawing.Size(85, 13);
            this.AveragingLabel.TabIndex = 11;
            this.AveragingLabel.Text = "Averaging count";
            // 
            // AveragingTextBox
            // 
            this.AveragingTextBox.Location = new System.Drawing.Point(97, 423);
            this.AveragingTextBox.Name = "AveragingTextBox";
            this.AveragingTextBox.Size = new System.Drawing.Size(31, 20);
            this.AveragingTextBox.TabIndex = 10;
            this.AveragingTextBox.Text = "1";
            this.AveragingTextBox.TextChanged += new System.EventHandler(this.AveragingTextBox_TextChanged);
            // 
            // BandNumberLabel
            // 
            this.BandNumberLabel.AutoSize = true;
            this.BandNumberLabel.Location = new System.Drawing.Point(442, 433);
            this.BandNumberLabel.Name = "BandNumberLabel";
            this.BandNumberLabel.Size = new System.Drawing.Size(94, 13);
            this.BandNumberLabel.TabIndex = 9;
            this.BandNumberLabel.Text = "Band number: 100";
            // 
            // DirectionLabel
            // 
            this.DirectionLabel.AutoSize = true;
            this.DirectionLabel.Location = new System.Drawing.Point(428, 29);
            this.DirectionLabel.Name = "DirectionLabel";
            this.DirectionLabel.Size = new System.Drawing.Size(49, 13);
            this.DirectionLabel.TabIndex = 8;
            this.DirectionLabel.Text = "Direction";
            // 
            // DirectionTextBox
            // 
            this.DirectionTextBox.Enabled = false;
            this.DirectionTextBox.Location = new System.Drawing.Point(483, 26);
            this.DirectionTextBox.Name = "DirectionTextBox";
            this.DirectionTextBox.Size = new System.Drawing.Size(53, 20);
            this.DirectionTextBox.TabIndex = 7;
            this.DirectionTextBox.Text = "0";
            this.DirectionTextBox.TextChanged += new System.EventHandler(this.DirectionTextBox_TextChanged);
            // 
            // AutoDirectionCheckBox
            // 
            this.AutoDirectionCheckBox.AutoSize = true;
            this.AutoDirectionCheckBox.Checked = true;
            this.AutoDirectionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AutoDirectionCheckBox.Location = new System.Drawing.Point(431, 3);
            this.AutoDirectionCheckBox.Name = "AutoDirectionCheckBox";
            this.AutoDirectionCheckBox.Size = new System.Drawing.Size(112, 17);
            this.AutoDirectionCheckBox.TabIndex = 6;
            this.AutoDirectionCheckBox.Text = "Use auto direction";
            this.AutoDirectionCheckBox.UseVisualStyleBackColor = true;
            this.AutoDirectionCheckBox.CheckedChanged += new System.EventHandler(this.AutoDirectionCheckBox_CheckedChanged);
            // 
            // PhaseControl
            // 
            this.PhaseControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PhaseControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PhaseControl.ControlName = "Direction";
            this.PhaseControl.Location = new System.Drawing.Point(222, 184);
            this.PhaseControl.Margin = new System.Windows.Forms.Padding(0);
            this.PhaseControl.Name = "PhaseControl";
            this.PhaseControl.Size = new System.Drawing.Size(93, 116);
            this.PhaseControl.TabIndex = 5;
            this.PhaseControl.Value = 288F;
            // 
            // phaseBox5
            // 
            this.phaseBox5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseBox5.BackColor = System.Drawing.Color.Transparent;
            this.phaseBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseBox5.IsSelected = false;
            this.phaseBox5.Location = new System.Drawing.Point(61, 139);
            this.phaseBox5.Margin = new System.Windows.Forms.Padding(0);
            this.phaseBox5.Name = "phaseBox5";
            this.phaseBox5.Number = "5";
            this.phaseBox5.Phase = 0F;
            this.phaseBox5.Size = new System.Drawing.Size(90, 79);
            this.phaseBox5.TabIndex = 1;
            this.phaseBox5.TheoryPhase = 0F;
            // 
            // phaseBox3
            // 
            this.phaseBox3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseBox3.BackColor = System.Drawing.Color.Transparent;
            this.phaseBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseBox3.IsSelected = false;
            this.phaseBox3.Location = new System.Drawing.Point(323, 338);
            this.phaseBox3.Margin = new System.Windows.Forms.Padding(0);
            this.phaseBox3.Name = "phaseBox3";
            this.phaseBox3.Number = "3";
            this.phaseBox3.Phase = 0F;
            this.phaseBox3.Size = new System.Drawing.Size(90, 79);
            this.phaseBox3.TabIndex = 4;
            this.phaseBox3.TheoryPhase = 0F;
            // 
            // phaseBox2
            // 
            this.phaseBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseBox2.BackColor = System.Drawing.Color.Transparent;
            this.phaseBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseBox2.IsSelected = false;
            this.phaseBox2.Location = new System.Drawing.Point(393, 139);
            this.phaseBox2.Margin = new System.Windows.Forms.Padding(0);
            this.phaseBox2.Name = "phaseBox2";
            this.phaseBox2.Number = "2";
            this.phaseBox2.Phase = 0F;
            this.phaseBox2.Size = new System.Drawing.Size(90, 79);
            this.phaseBox2.TabIndex = 2;
            this.phaseBox2.TheoryPhase = 0F;
            // 
            // phaseBox4
            // 
            this.phaseBox4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseBox4.BackColor = System.Drawing.Color.Transparent;
            this.phaseBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseBox4.IsSelected = false;
            this.phaseBox4.Location = new System.Drawing.Point(127, 338);
            this.phaseBox4.Margin = new System.Windows.Forms.Padding(0);
            this.phaseBox4.Name = "phaseBox4";
            this.phaseBox4.Number = "4";
            this.phaseBox4.Phase = 0F;
            this.phaseBox4.Size = new System.Drawing.Size(90, 79);
            this.phaseBox4.TabIndex = 3;
            this.phaseBox4.TheoryPhase = 0F;
            // 
            // phaseBox1
            // 
            this.phaseBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.phaseBox1.BackColor = System.Drawing.Color.Aqua;
            this.phaseBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.phaseBox1.IsSelected = true;
            this.phaseBox1.Location = new System.Drawing.Point(225, 39);
            this.phaseBox1.Margin = new System.Windows.Forms.Padding(0);
            this.phaseBox1.Name = "phaseBox1";
            this.phaseBox1.Number = "1";
            this.phaseBox1.Phase = 0F;
            this.phaseBox1.Size = new System.Drawing.Size(90, 79);
            this.phaseBox1.TabIndex = 0;
            this.phaseBox1.TheoryPhase = 0F;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape5,
            this.lineShape4,
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(546, 446);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape5
            // 
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.X1 = 267;
            this.lineShape5.X2 = 191;
            this.lineShape5.Y1 = 235;
            this.lineShape5.Y2 = 345;
            // 
            // lineShape4
            // 
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 267;
            this.lineShape4.X2 = 351;
            this.lineShape4.Y1 = 236;
            this.lineShape4.Y2 = 348;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 127;
            this.lineShape3.X2 = 266;
            this.lineShape3.Y1 = 193;
            this.lineShape3.Y2 = 236;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 267;
            this.lineShape2.X2 = 412;
            this.lineShape2.Y1 = 236;
            this.lineShape2.Y2 = 191;
            // 
            // BandArrayControl
            // 
            this.BandArrayControl.ActiveBandIndex = 0;
            this.BandArrayControl.BandCount = 1;
            this.BandArrayControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BandArrayControl.Location = new System.Drawing.Point(0, 689);
            this.BandArrayControl.Margin = new System.Windows.Forms.Padding(0);
            this.BandArrayControl.Name = "BandArrayControl";
            this.BandArrayControl.Size = new System.Drawing.Size(822, 16);
            this.BandArrayControl.TabIndex = 4;
            // 
            // _techAppGraphControl
            // 
            this._techAppGraphControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._techAppGraphControl.DataRange = new NationalInstruments.UI.Range(25D, 3025D);
            this._techAppGraphControl.Location = new System.Drawing.Point(0, 453);
            this._techAppGraphControl.Margin = new System.Windows.Forms.Padding(0);
            this._techAppGraphControl.Name = "_techAppGraphControl";
            this._techAppGraphControl.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this._techAppGraphControl.SelectionEnabled = true;
            this._techAppGraphControl.SelectionWidth = 1000F;
            this._techAppGraphControl.Size = new System.Drawing.Size(822, 236);
            this._techAppGraphControl.StickDataToVisibleRange = false;
            this._techAppGraphControl.TabIndex = 3;
            this._techAppGraphControl.VisibleRange = new NationalInstruments.UI.Range(25D, 3025D);
            // 
            // RealPhasesTextControl
            // 
            this.RealPhasesTextControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RealPhasesTextControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RealPhasesTextControl.Location = new System.Drawing.Point(7, 19);
            this.RealPhasesTextControl.Margin = new System.Windows.Forms.Padding(0);
            this.RealPhasesTextControl.Name = "RealPhasesTextControl";
            this.RealPhasesTextControl.Names = null;
            this.RealPhasesTextControl.Size = new System.Drawing.Size(74, 416);
            this.RealPhasesTextControl.TabIndex = 5;
            // 
            // RealPhasesLabel
            // 
            this.RealPhasesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RealPhasesLabel.AutoSize = true;
            this.RealPhasesLabel.Location = new System.Drawing.Point(10, -1);
            this.RealPhasesLabel.Name = "RealPhasesLabel";
            this.RealPhasesLabel.Size = new System.Drawing.Size(66, 13);
            this.RealPhasesLabel.TabIndex = 6;
            this.RealPhasesLabel.Text = "Real phases";
            // 
            // TheoreticalPhasesLabel
            // 
            this.TheoreticalPhasesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TheoreticalPhasesLabel.AutoSize = true;
            this.TheoreticalPhasesLabel.Location = new System.Drawing.Point(92, -1);
            this.TheoreticalPhasesLabel.Name = "TheoreticalPhasesLabel";
            this.TheoreticalPhasesLabel.Size = new System.Drawing.Size(77, 13);
            this.TheoreticalPhasesLabel.TabIndex = 8;
            this.TheoreticalPhasesLabel.Text = "Theory phases";
            // 
            // TheoryPhasesTextControl
            // 
            this.TheoryPhasesTextControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TheoryPhasesTextControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TheoryPhasesTextControl.Location = new System.Drawing.Point(93, 19);
            this.TheoryPhasesTextControl.Margin = new System.Windows.Forms.Padding(0);
            this.TheoryPhasesTextControl.Name = "TheoryPhasesTextControl";
            this.TheoryPhasesTextControl.Names = null;
            this.TheoryPhasesTextControl.Size = new System.Drawing.Size(74, 416);
            this.TheoryPhasesTextControl.TabIndex = 7;
            // 
            // DeltaPhasesLabel
            // 
            this.DeltaPhasesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DeltaPhasesLabel.AutoSize = true;
            this.DeltaPhasesLabel.Location = new System.Drawing.Point(183, -1);
            this.DeltaPhasesLabel.Name = "DeltaPhasesLabel";
            this.DeltaPhasesLabel.Size = new System.Drawing.Size(69, 13);
            this.DeltaPhasesLabel.TabIndex = 10;
            this.DeltaPhasesLabel.Text = "Delta phases";
            // 
            // DeltaPhasesTextControl
            // 
            this.DeltaPhasesTextControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DeltaPhasesTextControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DeltaPhasesTextControl.Location = new System.Drawing.Point(178, 19);
            this.DeltaPhasesTextControl.Margin = new System.Windows.Forms.Padding(0);
            this.DeltaPhasesTextControl.Name = "DeltaPhasesTextControl";
            this.DeltaPhasesTextControl.Names = null;
            this.DeltaPhasesTextControl.Size = new System.Drawing.Size(74, 416);
            this.DeltaPhasesTextControl.TabIndex = 9;
            // 
            // RightPanel
            // 
            this.RightPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RightPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RightPanel.Controls.Add(this.DeltaPhasesTextControl);
            this.RightPanel.Controls.Add(this.DeltaPhasesLabel);
            this.RightPanel.Controls.Add(this.RealPhasesTextControl);
            this.RightPanel.Controls.Add(this.RealPhasesLabel);
            this.RightPanel.Controls.Add(this.TheoreticalPhasesLabel);
            this.RightPanel.Controls.Add(this.TheoryPhasesTextControl);
            this.RightPanel.Location = new System.Drawing.Point(554, 3);
            this.RightPanel.Name = "RightPanel";
            this.RightPanel.Size = new System.Drawing.Size(265, 448);
            this.RightPanel.TabIndex = 11;
            // 
            // PhaseAnalyseTabControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.RightPanel);
            this.Controls.Add(this.BandArrayControl);
            this.Controls.Add(this._techAppGraphControl);
            this.Controls.Add(this.TopPanel);
            this.Name = "PhaseAnalyseTabControl";
            this.Size = new System.Drawing.Size(822, 705);
            this.Load += new System.EventHandler(this.PhaseAnalyseTabControl_Load);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.RightPanel.ResumeLayout(false);
            this.RightPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private PhaseBox phaseBox1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Panel TopPanel;
        private PhaseBox phaseBox4;
        private PhaseBox phaseBox5;
        private PhaseBox phaseBox2;
        private PhaseBox phaseBox3;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private PhaseControl PhaseControl;
        private System.Windows.Forms.CheckBox AutoDirectionCheckBox;
        private System.Windows.Forms.Label DirectionLabel;
        private System.Windows.Forms.TextBox DirectionTextBox;
        private TechAppGraphControl _techAppGraphControl;
        private System.Windows.Forms.Label BandNumberLabel;
        private BandArrayControl BandArrayControl;
        private PhasesTextControl RealPhasesTextControl;
        private System.Windows.Forms.Label RealPhasesLabel;
        private System.Windows.Forms.Label TheoreticalPhasesLabel;
        private PhasesTextControl TheoryPhasesTextControl;
        private System.Windows.Forms.Label DeltaPhasesLabel;
        private PhasesTextControl DeltaPhasesTextControl;
        private System.Windows.Forms.Panel RightPanel;
        private System.Windows.Forms.Label AveragingLabel;
        private System.Windows.Forms.TextBox AveragingTextBox;
    }
}
