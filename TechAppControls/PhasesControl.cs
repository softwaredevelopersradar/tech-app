﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace TechAppControls
{
    public partial class PhasesControl : UserControl
    {
        public readonly PhaseControl[] PhaseControls;

        public IReadOnlyList<float> Phases { get; private set; }

        public PhasesControl()
        {
            InitializeComponent();
            PhaseControls = new[]
            {
                phaseControl1, phaseControl2, phaseControl3, phaseControl4, phaseControl5,
                phaseControl6, phaseControl7, phaseControl8, phaseControl9, phaseControl10,
            };
        }

        public void SetPhases(float[] phases)
        {
            Phases = phases;

            for (var i = 0; i < PhaseControls.Length; ++i)
            {
                PhaseControls[i].Value = phases[i];
            }
        }
    }
}
