﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Correlator;
using NationalInstruments.UI;
using Phases;
using Protocols;
using Settings;
using SharpExtensions;

namespace TechAppControls
{
    public partial class PhaseAnalyseTabControl : UserControl, ITab
    {
        private PhaseBox[] _phaseBoxes;

        private int _selectedBoxIndex;

        public DspClient.DspClient Client;

        public RadioPathTable RadiopathTable;

        public bool IsWorking { get; private set; }

        private int _bandNumber = -1;

        private TheoreticalTable _theoreticalTable;

        private readonly double[,] _spectrum = new double[6, Constants.BandSampleCount];

        private readonly double[] _spectrumPlot = new double[Constants.BandSampleCount];

        private int _direction;

        public int AveragingCount { get; private set; }

        public int BandNumber
        {
            get { return _bandNumber; }
            private set
            {
                if (_bandNumber == value)
                {
                    return;
                }
                try
                {
                    _bandNumber = value;
                    _techAppGraphControl.Deselect();
                    BandNumberLabel.Text = "Band number: " + _bandNumber;
                    _techAppGraphControl.VisibleRange = new Range(Utilities.GetBandMinFrequencyKhz(BandNumber) / 1000d,
                        Utilities.GetBandMaxFrequencyKhz(BandNumber) / 1000d);
                    _techAppGraphControl.DataRange = new Range(Utilities.GetBandMinFrequencyKhz(BandNumber) / 1000d,
                        Utilities.GetBandMaxFrequencyKhz(BandNumber) / 1000d);

                    if (BandArrayControl != null)
                    {
                        BandArrayControl.ActiveBandIndex = BandNumber;
                    }
                }
                catch (Exception e)
                {
                }
            }
        }

        public PhaseAnalyseTabControl()
        {
            InitializeComponent();
            InitPhaseBoxes();

            AveragingCount = 1;
            BandArrayControl.PictureClickEvent += OnBandArrayPictureClick;

            _techAppGraphControl.Graph.YAxes[0].Range = new Range(Constants.ReceiverMinAmplitude, 0);
            _techAppGraphControl.Graph.YAxes[0].Mode = AxisMode.Fixed;
            _techAppGraphControl.Graph.YAxes[0].Caption = "dB";
            _techAppGraphControl.Graph.XAxes[0].Caption = "MHz";
            _techAppGraphControl.MinVisibleRange = 0.1;

            var phasesNames = new[]
            {
                "1-2", "1-3", "1-4", "1-5", "2-3", "2-4", "2-5", "3-4", "3-5", "4-5"
            };

            RealPhasesTextControl.Names = phasesNames;
            DeltaPhasesTextControl.Names = phasesNames;
            TheoryPhasesTextControl.Names = phasesNames;

            try
            {
                _theoreticalTable = new TheoreticalTable(Config.Instance.TheoreticalTableSettings);
            }
            catch
            {
                // ignored
            }
            BandNumber = 0;
        }

        private void PhaseAnalyseTabControl_Load(object sender, EventArgs e)
        {
            BandArrayControl.BandCount = Config.Instance.BandSettings.BandCount;
        }

        private void InitPhaseBoxes()
        {
            _phaseBoxes = new[] { phaseBox1, phaseBox2, phaseBox3, phaseBox4, phaseBox5 };
            foreach (var phaseBox in _phaseBoxes)
            {
                phaseBox.SelectionEvent += OnPhaseBoxSelection;
            }   
        }

        private void OnBandArrayPictureClick(object sender, int index)
        {
            BandNumber = index;
        }

        void OnPhaseBoxSelection(object sender, EventArgs e)
        {
            var box = sender as PhaseBox;
            _selectedBoxIndex = Array.IndexOf(_phaseBoxes, box);

            foreach (var phaseBox in _phaseBoxes)
            {
                if (phaseBox != box)
                {
                    phaseBox.Deselect();
                }
            }
        }

        private async Task SpectrumTask()
        {
            if (IsWorking || Client == null)
            {
                return;
            }
            try
            {
                IsWorking = true;
                while (IsWorking)
                {
                    var scans = await Client.GetTechAppSpectrum(BandNumber, AveragingCount);
                    if (scans == null)
                    {
                        IsWorking = false;
                        return;
                    }
                    UpdateSpectrum(scans);
                    if (_techAppGraphControl.MouseClickFrequency > Constants.FirstBandMinKhz)
                    {
                        var from = (int) (_techAppGraphControl.MouseClickFrequency - _techAppGraphControl.SelectionWidth / 2);
                        var to = (int) (_techAppGraphControl.MouseClickFrequency + _techAppGraphControl.SelectionWidth / 2);
                        if (AutoDirectionCheckBox.Checked)
                        {
                            var source = await Client.ExecutiveDfRequest(from, to, 10, 1);
                            PhaseControl.Value = source.Direction * 0.1f;
                            _direction = (int) (source.Direction * 0.1f);
                        }
                        UpdatePhases(scans);
                    }
                    await Task.Delay(100);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                MessageBox.Show(e.StackTrace);
                IsWorking = false;
                throw;
            }
            IsWorking = false;
        }

        private void UpdateSpectrum(Scan[] scans)
        {
            UpdateAmplitudeSpectrum(scans);
            _techAppGraphControl.Plot(_spectrumPlot);
        }


        private void UpdateAmplitudeSpectrum(Scan[] scans)
        {
            for (var i = 0; i < scans.Length; i++)
            {
                var scan = scans[i];
                for (var j = 0; j < scan.Amplitudes.Length; ++j)
                {
                    _spectrum[i, j] = -scan.Amplitudes[j];
                    _spectrumPlot[j] = _spectrum[0, j];
                }
            }
        }

        private void UpdatePhases(Scan[] scans)
        {
            if (_techAppGraphControl.MouseClickFrequency < Constants.FirstBandMinKhz)
            {
                return;
            }
            var index = GetSignalIndex(scans);
            var phases = GetPhases(scans, index, false);
            var theoryPhases = GetPhases(scans, index, true);

            for (var i = 0; i < 5; ++i)
            {
                _phaseBoxes[i].Phase = phases[i];
                _phaseBoxes[i].TheoryPhase = theoryPhases[i];
            }
        }

        private int GetSignalIndex(Scan[] scans)
        {
            var clickIndex = (int)_techAppGraphControl.GetClickedDataIndex();
            var width = _techAppGraphControl.GetSelectionWidthDataCount() / 2 + 1;

            var to = clickIndex + width;
            var from = clickIndex - width;

            to = to.ToBounds(0, Constants.BandSampleCount - 1);
            from = from.ToBounds(0, Constants.BandSampleCount - 1);

            var maxs = new int[6] { clickIndex, clickIndex, clickIndex, clickIndex, clickIndex, clickIndex };
            for (var i = 0; i < scans.Length; i++)
            {
                for (var j = from; j < to; ++j)
                {
                    if (scans[i].Amplitudes[j] < scans[i].Amplitudes[maxs[i]])
                    {
                        maxs[i] = j;
                    }
                }
            }
            var medianMax = maxs.Median();
            if (medianMax == scans[0].Amplitudes.Length)
            {
                medianMax--;
            }

            return medianMax;
        }

        private float[] GetPhases(Scan[] scans, int index, bool theoreticalPhases)
        {
            var result = new float[5];
            var calibrationPhases = RadiopathTable.GetPhases(BandNumber, index).Phases.ToArray();

            var theoryPhases = theoreticalPhases
                ? _theoreticalTable.GetPhases(Utilities.GetFrequencyKhz(BandNumber, index))[_direction].Phases
                : new float[10];

            var phases = new float[10];
            var realPhases = new float[10];

            var phasesIndexes = new[]
            {
                new[] {-1, 0, 1, 2, 3},
                new[] {0, -1, 4, 5, 6},
                new[] {1, 4, -1, 7, 8},
                new[] {2, 5, 7, -1, 9},
                new[] {3, 6, 8, 9, -1}
            };

            for (int i = 0, k = 0; i < 5; ++i)
            {
                for (var j = i + 1; j < 5; ++j)
                {
                    phases[k] = PhaseMath.NormalizePhase((scans[i].Phases[index] - scans[j].Phases[index]) * 0.1f - calibrationPhases[k] - theoryPhases[k]);
                    realPhases[k] = PhaseMath.NormalizePhase((scans[i].Phases[index] - scans[j].Phases[index]) * 0.1f - calibrationPhases[k]);
                    ++k;
                }
            }
            for (var i = 0; i < 5; ++i)
            {
                var phaseIndex = phasesIndexes[_selectedBoxIndex][i];
                if (phaseIndex != -1)
                {
                    result[i] = phases[phaseIndex];
                }
            }

            // TODO: remove this shitcode
            if (theoreticalPhases)
            {
                RealPhasesTextControl.SetValues(realPhases);
                TheoryPhasesTextControl.SetValues(theoryPhases);
                DeltaPhasesTextControl.SetValues(phases.Select(p => PhaseMath.SignAngle(0, p)).ToArray());
            }
            return result;
        }

        public void StartTabWork()
        {
            SpectrumTask();
        }

        public void StopTabWork()
        {
            IsWorking = false;
        }

        private void AveragingTextBox_TextChanged(object sender, EventArgs e)
        {
            int number;
            if (!int.TryParse(AveragingTextBox.Text, out number))
            {
                return;
            }
            if (number > 0 && number <= 100)
            {
                AveragingCount = number;
            }
        }

        private void AutoDirectionCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            DirectionTextBox.Enabled = !AutoDirectionCheckBox.Checked;
            if (!AutoDirectionCheckBox.Checked)
            {
                DirectionTextBox_TextChanged(this, EventArgs.Empty);
            }
        }

        private void DirectionTextBox_TextChanged(object sender, EventArgs e)
        {
            int number;
            if (!int.TryParse(DirectionTextBox.Text, out number))
            {
                return;
            }
            if (number >= 0 && number <= 360)
            {
                _direction = number;
                PhaseControl.Value = number;
            }
        }
    }
}
