﻿namespace TechAppControls
{
    partial class GraphControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Graph = new NationalInstruments.UI.WindowsForms.WaveformGraph();
            this.waveformPlot1 = new NationalInstruments.UI.WaveformPlot();
            this.xAxis1 = new NationalInstruments.UI.XAxis();
            this.yAxis1 = new NationalInstruments.UI.YAxis();
            ((System.ComponentModel.ISupportInitialize)(this.Graph)).BeginInit();
            this.SuspendLayout();
            // 
            // Graph
            // 
            this.Graph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Graph.Location = new System.Drawing.Point(0, 0);
            this.Graph.Name = "Graph";
            this.Graph.Plots.AddRange(new NationalInstruments.UI.WaveformPlot[] {
            this.waveformPlot1});
            this.Graph.Size = new System.Drawing.Size(744, 466);
            this.Graph.TabIndex = 1;
            this.Graph.UseColorGenerator = true;
            this.Graph.XAxes.AddRange(new NationalInstruments.UI.XAxis[] {
            this.xAxis1});
            this.Graph.YAxes.AddRange(new NationalInstruments.UI.YAxis[] {
            this.yAxis1});
            this.Graph.PlotAreaMouseMove += new System.Windows.Forms.MouseEventHandler(this.PhasesGraph_PlotAreaMouseMove);
            this.Graph.PlotAreaMouseWheel += new System.Windows.Forms.MouseEventHandler(this.PhasesGraph_PlotAreaMouseWheel);
            this.Graph.AfterDrawPlotArea += new NationalInstruments.UI.AfterDrawEventHandler(this.OnSpectrumPlotDrawed);
            this.Graph.Click += new System.EventHandler(this.OnSpectrumClick);
            // 
            // waveformPlot1
            // 
            this.waveformPlot1.XAxis = this.xAxis1;
            this.waveformPlot1.YAxis = this.yAxis1;
            // 
            // xAxis1
            // 
            this.xAxis1.MajorDivisions.GridColor = System.Drawing.Color.Gray;
            this.xAxis1.MajorDivisions.GridLineStyle = NationalInstruments.UI.LineStyle.Dot;
            this.xAxis1.MajorDivisions.GridVisible = true;
            // 
            // yAxis1
            // 
            this.yAxis1.MajorDivisions.GridColor = System.Drawing.Color.Gray;
            this.yAxis1.MajorDivisions.GridLineStyle = NationalInstruments.UI.LineStyle.Dot;
            this.yAxis1.MajorDivisions.GridVisible = true;
            // 
            // GraphControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Graph);
            this.Name = "GraphControl";
            this.Size = new System.Drawing.Size(744, 466);
            ((System.ComponentModel.ISupportInitialize)(this.Graph)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private NationalInstruments.UI.WaveformPlot waveformPlot1;
        private NationalInstruments.UI.XAxis xAxis1;
        private NationalInstruments.UI.YAxis yAxis1;
        public NationalInstruments.UI.WindowsForms.WaveformGraph Graph;
    }
}
