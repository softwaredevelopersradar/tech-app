﻿using System;
using System.Drawing;
using System.Windows.Forms;
using NationalInstruments.UI;
using Cursor = System.Windows.Forms.Cursor;

namespace TechAppControls
{
    public partial class GraphControl : UserControl
    {
        public Color SelectionColor
        {
            get { return Graph.SelectionColor; }
            set { Graph.SelectionColor = value; }
        }

        public XYCursorCollection Cursors
        {
            get { return Graph.Cursors; }
        }

        public float SelectionWidth { get; set; }

        public bool StickDataToVisibleRange { get; set; }

        public Range DataRange
        {
            get { return _dataRange; }
            set
            {
                var isChanged = !EqualRanges(_dataRange, value);
                _dataRange = value;
                if (_mouseClickPosition == null)
                {
                    return;
                }
                if (_mouseClickPosition < _dataRange.Minimum || _mouseClickPosition > _dataRange.Maximum)
                {
                    _mouseClickPosition = null;
                }
                if (isChanged && DataRangeChangedEvent != null)
                {
                    DataRangeChangedEvent(this, EventArgs.Empty);
                }
            }
        }

        public Range VisibleRange
        {
            get { return _visibleRange; }
            set
            {
                var isChanged = !EqualRanges(_visibleRange, value);
                _visibleRange = value;
                UpdateVisibleRange();
                if (isChanged && VisibleRangeChangedEvent != null)
                {
                    VisibleRangeChangedEvent(this, EventArgs.Empty);
                }
            }
        }

        private bool EqualRanges(Range a, Range b)
        {
            if (a == null || b == null)
                return false;
            return a.Equals(b);
        }

        /// <summary>
        /// provides the frequency in khz of click point
        /// </summary>
        public event EventHandler<float> GraphClickEvent;

        public event EventHandler VisibleRangeChangedEvent;

        public event EventHandler DataRangeChangedEvent;

        public double MinVisibleRange = 1;

        public bool SelectionEnabled { get; set; }

        private bool _isDataLoaded = false;
        private bool _isMouseMoving = false;
        private int _dataLength;
        private Point _previousMousePosition;
        private Range _visibleRange;

        /// <summary>
        /// Click position in datarange, or -1 if nothing is clicked
        /// </summary>
        public double MouseClickPosition
        {
            get { return _mouseClickPosition ?? -1; }
        }

        private double? _mouseClickPosition = null;
        private Range _dataRange;

        public GraphControl()
        {
            InitializeComponent();
            
            DataRange = new Range(0, 1);
            VisibleRange = new Range(0, 1);
            SelectionWidth = 1000;
            SelectionEnabled = true;
            SelectionColor = Color.FromArgb(20, 255, 255, 255);
        }

        private void UpdateVisibleRange()
        {
            if (Graph == null || VisibleRange == null)
            {
                return;
            }
            foreach (WaveformPlot plot in Graph.Plots)
            {
                plot.XAxis.Range = VisibleRange;
                plot.XAxis.Mode = AxisMode.Fixed;
            }
        }

        public int? GetClickedDataIndex()
        {
            if (_mouseClickPosition == null || !_isDataLoaded)
            {
                return null;
            }
            var frequency = (double) _mouseClickPosition;
            return FrequencyToIndex(frequency);
        }

        private int FrequencyToIndex(double frequencyMhz)
        {
            var index = (int)(((frequencyMhz - DataRange.Minimum) / DataRange.Interval) * _dataLength);
            return index;
        }

        public int GetSelectionWidthDataCount()
        {
            var index = (int) ((SelectionWidth / 1000) / DataRange.Interval *
                               _dataLength);
            return index;
        }

        public int DataIndexToComponentXPosition(int dataIndex)
        {
            var frequency = DataRange.Minimum + DataRange.Interval * dataIndex / _dataLength;
            var alpha = (frequency - VisibleRange.Minimum) / VisibleRange.Interval;
            return (int) (Graph.PlotAreaBounds.Left + alpha * (Graph.PlotAreaBounds.Width));
        }

        public void Plot(double[,] data)
        {
            _isDataLoaded = true;
            _dataLength = data.GetLength(1);
            var updateVisibleRange = data.GetLength(0) > Graph.Plots.Count;
            
            var range = StickDataToVisibleRange ? VisibleRange : DataRange;
            Graph.PlotYMultiple(data, DataOrientation.DataInRows, range.Minimum, range.Interval / _dataLength);

            if (updateVisibleRange)
            {
                UpdateVisibleRange();
            }
        }

        public void Plot(double[] data)
        {
            _isDataLoaded = true;
            _dataLength = data.Length;
            var range = StickDataToVisibleRange ? VisibleRange : DataRange;
            Graph.PlotY(data, range.Minimum, range.Interval / _dataLength);
        }

        public void Deselect()
        {
            _mouseClickPosition = null;
        }

        private void OnSpectrumClick(object sender, EventArgs e)
        {
            if (_isMouseMoving)
            {
                _isMouseMoving = false;
                return;
            }
            _mouseClickPosition = MousePositionToFrequency();
            if (GraphClickEvent != null)
            {
                GraphClickEvent(this, (float) (_mouseClickPosition * 1000));
            }
        }

        private double MousePositionToFrequency()
        {
            var pos = 1d * (Graph.PointToClient(Cursor.Position).X - Graph.PlotAreaBounds.Left) / Graph.PlotAreaBounds.Width;
            return VisibleRange.Minimum + VisibleRange.Interval * pos;
        }

        private void OnSpectrumPlotDrawed(object sender, AfterDrawEventArgs e)
        {
            if (!SelectionEnabled || _mouseClickPosition == null)
            {
                return;
            }
            var frequency = (double) _mouseClickPosition;

            var width = SelectionWidth / VisibleRange.Interval * Graph.PlotAreaBounds.Width / 1000;
            var center = (frequency - VisibleRange.Minimum) / VisibleRange.Interval * Graph.PlotAreaBounds.Width + Graph.PlotAreaBounds.Left;
            if (SelectionWidth > 0)
            {
                if (width < 1)
                {
                    width = 1;
                }
                var rectangle = new Rectangle((int) (center - width / 2), 0, (int) width, Graph.Height);
                e.Graphics.FillRectangle(new SolidBrush(SelectionColor), rectangle);
            }
            else
            {
                e.Graphics.DrawLine(new Pen(SelectionColor), (float)center, 0, (float)center, Graph.Height);
            }
        }

        private void PhasesGraph_PlotAreaMouseWheel(object sender, MouseEventArgs e)
        {
            if (!_isDataLoaded)
            {
                return;
            }

            var range = Graph.XAxes[0].Range;
            var size = (range.Maximum - range.Minimum) / 2;
            var s = e.Delta > 0 ? 5f / 4 : 4f / 5;
            size *= s;
            if (size < 0.5 * MinVisibleRange)
            {
                size = 0.5 * MinVisibleRange;
            }

            var pos = 1d * (Graph.PointToClient(Cursor.Position).X - Graph.PlotAreaBounds.Left) / Graph.PlotAreaBounds.Width;
            var mouseFrequencyMhz = VisibleRange.Minimum + VisibleRange.Interval * pos;
            var center = size + mouseFrequencyMhz - 2 * pos * size;

            var left = center - size;
            var right = center + size;
            if (left < DataRange.Minimum)
            {
                left = DataRange.Minimum;
            }
            if (right > DataRange.Maximum)
            {
                right = DataRange.Maximum;
            }
            VisibleRange = new Range(left, right);
        }

        private void PhasesGraph_PlotAreaMouseMove(object sender, MouseEventArgs e)
        {
            if (!_isDataLoaded)
            {
                return;
            }

            if ((e.Button & MouseButtons.Left) != 0)
            {
                var range = Graph.XAxes[0].Range;
                var size = range.Maximum - range.Minimum;

                var position = Graph.PointToClient(Cursor.Position);
                double delta = _previousMousePosition.X - position.X;
                var multiplier = size / (Graph.Width - Graph.PlotAreaBounds.Left);
                delta = delta * multiplier;

                var left = range.Minimum + delta;
                var right = range.Maximum + delta;
                if (left < DataRange.Minimum)
                {
                    left = DataRange.Minimum;
                }
                if (right > DataRange.Maximum)
                {
                    right = DataRange.Maximum;
                }
                VisibleRange = new Range(left, right);
                _isMouseMoving = true;
            }
            _previousMousePosition = Graph.PointToClient(Cursor.Position);
        }
    }
}
