﻿namespace FrequencyRangeTester
{
    partial class MainTesterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FormTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._techAppGraphControl = new TechAppControls.TechAppGraphControl();
            this.LeftPanel = new System.Windows.Forms.Panel();
            this.LevelLabel = new System.Windows.Forms.Label();
            this.LevelTextBox = new System.Windows.Forms.TextBox();
            this.SignalGeneratorLabel = new System.Windows.Forms.Label();
            this.SignalGeneratorLed = new NationalInstruments.UI.WindowsForms.Led();
            this.ServerConnectionLabel = new System.Windows.Forms.Label();
            this.ConnectionLed = new NationalInstruments.UI.WindowsForms.Led();
            this.StartButton = new System.Windows.Forms.Button();
            this.Progresslabel = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.StepLabel = new System.Windows.Forms.Label();
            this.StepTextBox = new System.Windows.Forms.TextBox();
            this.MaxFrequencyLabel = new System.Windows.Forms.Label();
            this.MaxFrequencyTextBox = new System.Windows.Forms.TextBox();
            this.MinFrequencyLabel = new System.Windows.Forms.Label();
            this.MinFrequencyTextBox = new System.Windows.Forms.TextBox();
            this.FormTableLayoutPanel.SuspendLayout();
            this.LeftPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SignalGeneratorLed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConnectionLed)).BeginInit();
            this.SuspendLayout();
            // 
            // FormTableLayoutPanel
            // 
            this.FormTableLayoutPanel.ColumnCount = 2;
            this.FormTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.FormTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.FormTableLayoutPanel.Controls.Add(this._techAppGraphControl, 1, 0);
            this.FormTableLayoutPanel.Controls.Add(this.LeftPanel, 0, 0);
            this.FormTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.FormTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.FormTableLayoutPanel.Name = "FormTableLayoutPanel";
            this.FormTableLayoutPanel.RowCount = 1;
            this.FormTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.FormTableLayoutPanel.Size = new System.Drawing.Size(972, 667);
            this.FormTableLayoutPanel.TabIndex = 0;
            // 
            // TechAppGraphControl
            // 
            this._techAppGraphControl.DataRange = new NationalInstruments.UI.Range(25D, 3025D);
            this._techAppGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._techAppGraphControl.Location = new System.Drawing.Point(119, 0);
            this._techAppGraphControl.Margin = new System.Windows.Forms.Padding(0);
            this._techAppGraphControl.Name = "_techAppGraphControl";
            this._techAppGraphControl.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this._techAppGraphControl.SelectionEnabled = true;
            this._techAppGraphControl.SelectionWidth = 1000F;
            this._techAppGraphControl.Size = new System.Drawing.Size(853, 667);
            this._techAppGraphControl.StickDataToVisibleRange = false;
            this._techAppGraphControl.TabIndex = 0;
            this._techAppGraphControl.VisibleRange = new NationalInstruments.UI.Range(25D, 3025D);
            // 
            // LeftPanel
            // 
            this.LeftPanel.AutoSize = true;
            this.LeftPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.LeftPanel.Controls.Add(this.LevelLabel);
            this.LeftPanel.Controls.Add(this.LevelTextBox);
            this.LeftPanel.Controls.Add(this.SignalGeneratorLabel);
            this.LeftPanel.Controls.Add(this.SignalGeneratorLed);
            this.LeftPanel.Controls.Add(this.ServerConnectionLabel);
            this.LeftPanel.Controls.Add(this.ConnectionLed);
            this.LeftPanel.Controls.Add(this.StartButton);
            this.LeftPanel.Controls.Add(this.Progresslabel);
            this.LeftPanel.Controls.Add(this.progressBar);
            this.LeftPanel.Controls.Add(this.StepLabel);
            this.LeftPanel.Controls.Add(this.StepTextBox);
            this.LeftPanel.Controls.Add(this.MaxFrequencyLabel);
            this.LeftPanel.Controls.Add(this.MaxFrequencyTextBox);
            this.LeftPanel.Controls.Add(this.MinFrequencyLabel);
            this.LeftPanel.Controls.Add(this.MinFrequencyTextBox);
            this.LeftPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftPanel.Margin = new System.Windows.Forms.Padding(0);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(119, 667);
            this.LeftPanel.TabIndex = 1;
            // 
            // LevelLabel
            // 
            this.LevelLabel.AutoSize = true;
            this.LevelLabel.Location = new System.Drawing.Point(12, 165);
            this.LevelLabel.Name = "LevelLabel";
            this.LevelLabel.Size = new System.Drawing.Size(63, 13);
            this.LevelLabel.TabIndex = 14;
            this.LevelLabel.Text = "Level, dBm:";
            this.LevelLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // LevelTextBox
            // 
            this.LevelTextBox.Location = new System.Drawing.Point(12, 182);
            this.LevelTextBox.Name = "LevelTextBox";
            this.LevelTextBox.Size = new System.Drawing.Size(100, 20);
            this.LevelTextBox.TabIndex = 13;
            this.LevelTextBox.Text = "-20";
            this.LevelTextBox.TextChanged += new System.EventHandler(this.LevelBoxTextChanged);
            // 
            // SignalGeneratorLabel
            // 
            this.SignalGeneratorLabel.AutoSize = true;
            this.SignalGeneratorLabel.Location = new System.Drawing.Point(11, 602);
            this.SignalGeneratorLabel.Name = "SignalGeneratorLabel";
            this.SignalGeneratorLabel.Size = new System.Drawing.Size(60, 13);
            this.SignalGeneratorLabel.TabIndex = 12;
            this.SignalGeneratorLabel.Text = "Signal gen:";
            // 
            // SignalGeneratorLed
            // 
            this.SignalGeneratorLed.CaptionBackColor = System.Drawing.Color.Transparent;
            this.SignalGeneratorLed.LedStyle = NationalInstruments.UI.LedStyle.Square;
            this.SignalGeneratorLed.Location = new System.Drawing.Point(86, 596);
            this.SignalGeneratorLed.Margin = new System.Windows.Forms.Padding(0);
            this.SignalGeneratorLed.Name = "SignalGeneratorLed";
            this.SignalGeneratorLed.OffColor = System.Drawing.Color.Red;
            this.SignalGeneratorLed.Size = new System.Drawing.Size(26, 26);
            this.SignalGeneratorLed.TabIndex = 11;
            // 
            // ServerConnectionLabel
            // 
            this.ServerConnectionLabel.AutoSize = true;
            this.ServerConnectionLabel.Location = new System.Drawing.Point(11, 628);
            this.ServerConnectionLabel.Name = "ServerConnectionLabel";
            this.ServerConnectionLabel.Size = new System.Drawing.Size(41, 13);
            this.ServerConnectionLabel.TabIndex = 10;
            this.ServerConnectionLabel.Text = "Server:";
            // 
            // ConnectionLed
            // 
            this.ConnectionLed.CaptionBackColor = System.Drawing.Color.Transparent;
            this.ConnectionLed.LedStyle = NationalInstruments.UI.LedStyle.Square;
            this.ConnectionLed.Location = new System.Drawing.Point(86, 622);
            this.ConnectionLed.Margin = new System.Windows.Forms.Padding(0);
            this.ConnectionLed.Name = "ConnectionLed";
            this.ConnectionLed.OffColor = System.Drawing.Color.Red;
            this.ConnectionLed.Size = new System.Drawing.Size(26, 26);
            this.ConnectionLed.TabIndex = 9;
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(12, 229);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(101, 23);
            this.StartButton.TabIndex = 8;
            this.StartButton.Text = "Start";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // Progresslabel
            // 
            this.Progresslabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Progresslabel.AutoSize = true;
            this.Progresslabel.BackColor = System.Drawing.Color.Transparent;
            this.Progresslabel.Location = new System.Drawing.Point(82, 651);
            this.Progresslabel.Name = "Progresslabel";
            this.Progresslabel.Size = new System.Drawing.Size(0, 13);
            this.Progresslabel.TabIndex = 7;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progressBar.Location = new System.Drawing.Point(3, 651);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(73, 13);
            this.progressBar.TabIndex = 6;
            // 
            // StepLabel
            // 
            this.StepLabel.AutoSize = true;
            this.StepLabel.Location = new System.Drawing.Point(12, 112);
            this.StepLabel.Name = "StepLabel";
            this.StepLabel.Size = new System.Drawing.Size(56, 13);
            this.StepLabel.TabIndex = 5;
            this.StepLabel.Text = "Step, Khz:";
            // 
            // StepTextBox
            // 
            this.StepTextBox.Location = new System.Drawing.Point(12, 129);
            this.StepTextBox.Name = "StepTextBox";
            this.StepTextBox.Size = new System.Drawing.Size(100, 20);
            this.StepTextBox.TabIndex = 4;
            this.StepTextBox.Text = "1000";
            this.StepTextBox.TextChanged += new System.EventHandler(this.StepTextBoxTextChanged);
            // 
            // MaxFrequencyLabel
            // 
            this.MaxFrequencyLabel.AutoSize = true;
            this.MaxFrequencyLabel.Location = new System.Drawing.Point(12, 60);
            this.MaxFrequencyLabel.Name = "MaxFrequencyLabel";
            this.MaxFrequencyLabel.Size = new System.Drawing.Size(104, 13);
            this.MaxFrequencyLabel.TabIndex = 3;
            this.MaxFrequencyLabel.Text = "Max frequency, Khz:";
            // 
            // MaxFrequencyTextBox
            // 
            this.MaxFrequencyTextBox.Location = new System.Drawing.Point(12, 77);
            this.MaxFrequencyTextBox.Name = "MaxFrequencyTextBox";
            this.MaxFrequencyTextBox.Size = new System.Drawing.Size(100, 20);
            this.MaxFrequencyTextBox.TabIndex = 2;
            this.MaxFrequencyTextBox.Text = "3025000";
            this.MaxFrequencyTextBox.TextChanged += new System.EventHandler(this.MaxFrequencyTextBoxTextChanged);
            // 
            // MinFrequencyLabel
            // 
            this.MinFrequencyLabel.AutoSize = true;
            this.MinFrequencyLabel.Location = new System.Drawing.Point(12, 9);
            this.MinFrequencyLabel.Name = "MinFrequencyLabel";
            this.MinFrequencyLabel.Size = new System.Drawing.Size(101, 13);
            this.MinFrequencyLabel.TabIndex = 1;
            this.MinFrequencyLabel.Text = "Min frequency, Khz:";
            // 
            // MinFrequencyTextBox
            // 
            this.MinFrequencyTextBox.Location = new System.Drawing.Point(12, 26);
            this.MinFrequencyTextBox.Name = "MinFrequencyTextBox";
            this.MinFrequencyTextBox.Size = new System.Drawing.Size(100, 20);
            this.MinFrequencyTextBox.TabIndex = 0;
            this.MinFrequencyTextBox.Text = "25500";
            this.MinFrequencyTextBox.TextChanged += new System.EventHandler(this.MinFrequencyTextBoxTextChanged);
            // 
            // MainTesterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 667);
            this.Controls.Add(this.FormTableLayoutPanel);
            this.Name = "MainTesterForm";
            this.Text = "Frequency range tester";
            this.Load += new System.EventHandler(this.MainTesterForm_Load);
            this.FormTableLayoutPanel.ResumeLayout(false);
            this.FormTableLayoutPanel.PerformLayout();
            this.LeftPanel.ResumeLayout(false);
            this.LeftPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SignalGeneratorLed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConnectionLed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel FormTableLayoutPanel;
        private TechAppControls.TechAppGraphControl _techAppGraphControl;
        private System.Windows.Forms.Panel LeftPanel;
        private System.Windows.Forms.Label MinFrequencyLabel;
        private System.Windows.Forms.TextBox MinFrequencyTextBox;
        private System.Windows.Forms.Label StepLabel;
        private System.Windows.Forms.TextBox StepTextBox;
        private System.Windows.Forms.Label MaxFrequencyLabel;
        private System.Windows.Forms.TextBox MaxFrequencyTextBox;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label Progresslabel;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Label ServerConnectionLabel;
        private NationalInstruments.UI.WindowsForms.Led ConnectionLed;
        private System.Windows.Forms.Label SignalGeneratorLabel;
        private NationalInstruments.UI.WindowsForms.Led SignalGeneratorLed;
        private System.Windows.Forms.Label LevelLabel;
        private System.Windows.Forms.TextBox LevelTextBox;
    }
}

