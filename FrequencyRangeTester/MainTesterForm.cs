﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using NationalInstruments.UI;
using NetLib;
using Settings;
using SharpExtensions;
using TechAppControls;

namespace FrequencyRangeTester
{
    public partial class MainTesterForm : Form
    {
        private float _minFrequency;
        private float _maxFrequency;
        private float _step;
        private float _level;

        private double[,] _data;

        public DspClient.DspClient Client;

        public KeysightDriver.KeysightDriver SignalGenerator;

        public float MinFrequency
        {
            get { return _minFrequency; }
            set
            {
                _minFrequency = value;
                UpdateTextBoxValue(MinFrequencyTextBox, MinFrequency.ToString());
            }
        }

        public float MaxFrequency
        {
            get { return _maxFrequency; }
            set
            {
                _maxFrequency = value;
                UpdateTextBoxValue(MaxFrequencyTextBox, MaxFrequency.ToString());
            }
        }

        public float Step
        {
            get { return _step; }
            set
            {
                _step = value;
                UpdateTextBoxValue(StepTextBox, Step.ToString());
            }
        }

        public float Level
        {
            get { return _level; }
            set
            {
                _level = value;
                UpdateTextBoxValue(LevelTextBox, Level.ToString());
            }
        }

        public MainTesterForm()
        {
            InitializeComponent();
            SignalGenerator = new KeysightDriver.KeysightDriver();
        }

        private async void MainTesterForm_Load(object sender, EventArgs e)
        {
            MinFrequency = 25500;
            MaxFrequency = 3025000;
            Step = 1000;
            Level = -20;
            await Task.WhenAll(InitializeClient(), InitializeSignalGenerator());
        }

        private async Task InitializeClient()
        {
            Client = new DspClient.DspClient();
            Client.Client.StatusChangedEvent += OnClientStatusChanged;

            await Client.Connect(Config.Instance.ServerHost, Config.Instance.ServerPort);

            SignalGenerator = new KeysightDriver.KeysightDriver();
            if (SignalGenerator.Initialize("192.168.0.99"))
            {
                SignalGeneratorLed.Value = true;
            }
        }

        private Task InitializeSignalGenerator()
        {
            return Task.Run(async () =>
            {
                await Task.Delay(100);
                if (SignalGenerator.Initialize("192.168.0.99"))
                {
                    SignalGeneratorLed.Value = true;
                }
            });
        }

        void OnClientStatusChanged(object sender, EventArgs e)
        {
            ConnectionLed.Value = Client.Client.Status == ClientStatus.IsWorking;
        }

        private bool UpdateFrequencyTextBoxValue(TextBox textBox, out int frequency)
        {
            if (int.TryParse(textBox.Text, out frequency))
            {
                frequency = frequency.ToBounds(Constants.FirstBandMinKhz, Utilities.GetBandMaxFrequencyKhz(Config.Instance.BandSettings.BandCount - 1));
                return true;
            }
            return false;
        }

        private bool UpdateTextBoxValue(TextBox textBox, out int value)
        {
            if (int.TryParse(textBox.Text, out value))
            {
                return true;
            }
            return false;
        }

        private void UpdateTextBoxValue(TextBox textBox, string value)
        {
            textBox.Text = value;
        }


        private void MinFrequencyTextBoxTextChanged(object sender, System.EventArgs e)
        {
            int frequency;
            if (UpdateFrequencyTextBoxValue(MinFrequencyTextBox, out frequency))
            {
                frequency = frequency.ToBounds(Constants.FirstBandMinKhz, Config.Instance.BandSettings.LastBandMaxKhz);
                if (frequency < MaxFrequency)
                {
                    MinFrequency = frequency;
                }
            }
        }

        private void MaxFrequencyTextBoxTextChanged(object sender, System.EventArgs e)
        {
            int frequency;
            if (UpdateFrequencyTextBoxValue(MaxFrequencyTextBox, out frequency))
            {
                frequency = frequency.ToBounds(Constants.FirstBandMinKhz, Config.Instance.BandSettings.LastBandMaxKhz);
                if (frequency > MinFrequency)
                {
                    MaxFrequency = frequency;
                }
            }
        }

        private void StepTextBoxTextChanged(object sender, System.EventArgs e)
        {
            int frequency;
            if (UpdateTextBoxValue(StepTextBox, out frequency))
            {
                if (frequency < 1)
                {
                    frequency = 1;
                }
                Step = frequency;
            }
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            if (!SignalGenerator.IsInitialized || Client.Status != ClientStatus.IsWorking)
            {
                return;
            }
            MinFrequencyTextBox.Enabled = false;
            MaxFrequencyTextBox.Enabled = false;
            LevelTextBox.Enabled = false;
            StepTextBox.Enabled = false;
            StartButton.Enabled = false;
            WorkingCycle();
        }

        private async Task WorkingCycle()
        {
            var stepCount = (int) ((MaxFrequency - MinFrequency) / Step + 1);
            _data = new double[Constants.ReceiversCount, stepCount];

            _techAppGraphControl.DataRange = new Range(MinFrequency / 1000, MaxFrequency / 1000);

            SignalGenerator.SetLevel(Level);
            SignalGenerator.EmitionEnabled = true;

            for (var i = 0; i < stepCount; ++i)
            {
                var frequency = MinFrequency + i * Step;
                if (frequency > MaxFrequency)
                {
                    break;
                }

                var band = Utilities.GetBandNumber(frequency);
                var offset = Utilities.GetSampleNumber(frequency);

                SignalGenerator.SetFrequency(frequency);

                // just skip first scan because of scan delay - we always get previous scan, so when we need the current one we should skip one
                await Client.GetTechAppSpectrum(band, 1);
                var scans = await Client.GetTechAppSpectrum(band, 1);

                var from = Math.Max(0, offset - 3);
                var to = Math.Min(Constants.BandSampleCount, offset + 4);

                var points = Enumerable.Repeat(float.MinValue, scans.Length).ToArray();

                for (var index = from; index < to; ++index)
                {
                    for (var j = 0; j < points.Length; ++j)
                    {
                        var level = -scans[j].Amplitudes[index];
                        if (points[j] < level)
                        {
                            points[j] = level;
                        }
                    }
                }
                for (var j = 0; j < points.Length; ++j)
                {
                    _data[j, i] = points[j];
                }
                _techAppGraphControl.Plot(_data);

                var progress = 100 * i / stepCount;
                Progresslabel.Text = progress + " %";
                progressBar.Value = progress;
            }

            Progresslabel.Text ="100 %";
            progressBar.Value = 100;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void LevelBoxTextChanged(object sender, EventArgs e)
        {
            int level;
            if (UpdateTextBoxValue(LevelTextBox, out level))
            {
                Level = level;
            }
        }
    }
}
