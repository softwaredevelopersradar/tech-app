﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using NationalInstruments.UI;
using NationalInstruments.UI.WindowsForms;
using SharpExtensions;

namespace TechApp
{
    public class LedList
    {
        private readonly Led[] _leds;
        private ColorGenerator _colorGenerator;
        private SelectionMode _selectionMode;

        private ColorGenerator ColorGenerator
        {
            get { return _colorGenerator; }
            set
            {
                _colorGenerator = value;
                UpdateColors();
            }
        }

        public SelectionMode SelectionMode
        {
            get { return _selectionMode; }
            set
            {
                _selectionMode = value;
                if (SelectionMode == SelectionMode.One)
                {
                    SetValue(false);
                    _leds[0].Value = true;
                }
            }
        }

        public event EventHandler<int> LedClickEvent; 

        public IReadOnlyList<Led> Leds { get { return _leds; } }

        public LedList(IEnumerable<Led> leds, ColorGenerator colorGenerator)
        {
            _leds = leds.ToArray();
            ColorGenerator = colorGenerator;
            SelectionMode = SelectionMode.MultiSimple;
            InitializeLeds();
        }

        private void InitializeLeds()
        {
            foreach (var led in _leds)
            {
                led.Value = false;
                led.Click += OnLedClick;
            }
            UpdateColors();
            _leds[0].Value = true;
        }

        public void SetEnabled(bool isEnabled)
        {
            foreach (var led in _leds)
            {
                led.Enabled = isEnabled;
            }
        }

        public void SetValue(bool value)
        {
            foreach (var led in _leds)
            {
                led.Value = value;
            }
        }

        private void OnLedClick(object sender, EventArgs e)
        {
            if (SelectionMode == SelectionMode.None)
                return;
            var led = (Led)sender;

            if (SelectionMode == SelectionMode.One)
            {
                if (!led.Value)
                {
                    SetValue(false);
                    led.Value = true;
                }
            }
            else
            {
                led.Value = !led.Value;
            }
            OnLedClickEvent(_leds.FindIndex(led));
        }

        private void UpdateColors()
        {
            for (int i = 0; i < Leds.Count; ++i)
            {
                _leds[i].OnColor = ColorGenerator.GetColor(i);
            }
        }

        private void OnLedClickEvent(int e)
        {
            var handler = LedClickEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
