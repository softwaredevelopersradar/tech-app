﻿namespace TechApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.led7 = new NationalInstruments.UI.WindowsForms.Led();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.led1 = new NationalInstruments.UI.WindowsForms.Led();
            this.NoiseAllLeds = new NationalInstruments.UI.WindowsForms.Led();
            this.waveformGraph1 = new NationalInstruments.UI.WindowsForms.WaveformGraph();
            this.yAxis3 = new NationalInstruments.UI.YAxis();
            this.xAxis2 = new NationalInstruments.UI.XAxis();
            this.xyCursor1 = new NationalInstruments.UI.XYCursor();
            this.waveformPlot3 = new NationalInstruments.UI.WaveformPlot();
            this.xyCursor2 = new NationalInstruments.UI.XYCursor();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.gauge10 = new NationalInstruments.UI.WindowsForms.Gauge();
            this.gauge9 = new NationalInstruments.UI.WindowsForms.Gauge();
            this.gauge8 = new NationalInstruments.UI.WindowsForms.Gauge();
            this.gauge7 = new NationalInstruments.UI.WindowsForms.Gauge();
            this.gauge6 = new NationalInstruments.UI.WindowsForms.Gauge();
            this.gauge5 = new NationalInstruments.UI.WindowsForms.Gauge();
            this.gauge4 = new NationalInstruments.UI.WindowsForms.Gauge();
            this.gauge3 = new NationalInstruments.UI.WindowsForms.Gauge();
            this.gauge2 = new NationalInstruments.UI.WindowsForms.Gauge();
            this.gauge1 = new NationalInstruments.UI.WindowsForms.Gauge();
            this.FormTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.StatusPanel = new System.Windows.Forms.Panel();
            this.LanguageComboBox = new System.Windows.Forms.ComboBox();
            this.RecordButton = new System.Windows.Forms.Button();
            this.StartStopButton = new System.Windows.Forms.Button();
            this.ConnectionLabel = new System.Windows.Forms.Label();
            this.ConnectionLed = new NationalInstruments.UI.WindowsForms.Led();
            this.FormTabControl = new System.Windows.Forms.TabControl();
            this.NoiseGeneratorTabPage = new System.Windows.Forms.TabPage();
            this.IntelligenceTabPage = new System.Windows.Forms.TabPage();
            this.CalibrationResultTab = new System.Windows.Forms.TabPage();
            this.CalibrationDiffTabPage = new System.Windows.Forms.TabPage();
            //this.tabPage1 = new System.Windows.Forms.TabPage();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.TechTabControl = new TechAppControls.TechTabControl();
            this.IntelligenceTabControl = new TechAppControls.IntelligenceTabControl();
            this.CalibrationTabControl = new TechAppControls.CalibrationTabControl();
            this.CalibrationDiffControl = new TechAppControls.CalibrationDifferenceControl();
            //this.CorrectionTableControl = new TechAppControls.CorrectionTableControl();
            ((System.ComponentModel.ISupportInitialize)(this.led7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.led1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoiseAllLeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveformGraph1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xyCursor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xyCursor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge1)).BeginInit();
            this.FormTableLayoutPanel.SuspendLayout();
            this.StatusPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConnectionLed)).BeginInit();
            this.FormTabControl.SuspendLayout();
            this.NoiseGeneratorTabPage.SuspendLayout();
            this.IntelligenceTabPage.SuspendLayout();
            this.CalibrationResultTab.SuspendLayout();
            this.CalibrationDiffTabPage.SuspendLayout();
            //this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Location = new System.Drawing.Point(1169, 696);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(0, 0);
            this.panel2.TabIndex = 12;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(30, 696);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1139, 1);
            this.panel1.TabIndex = 11;
            // 
            // led7
            // 
            this.led7.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.led7.Location = new System.Drawing.Point(129, 26);
            this.led7.Name = "led7";
            this.led7.Size = new System.Drawing.Size(32, 36);
            this.led7.TabIndex = 17;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(619, 26);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(103, 21);
            this.comboBox1.TabIndex = 13;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(653, 53);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(35, 20);
            this.textBox4.TabIndex = 14;
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(619, 53);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(28, 20);
            this.button2.TabIndex = 15;
            this.button2.Text = "<";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(694, 53);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 20);
            this.button1.TabIndex = 16;
            this.button1.Text = ">";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(30, 350);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // led1
            // 
            this.led1.BackgroundImageAlignment = NationalInstruments.UI.ImageAlignment.Center;
            this.led1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.led1.Caption = "6";
            this.led1.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.led1.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.led1.CaptionVerticalOrientation = NationalInstruments.UI.VerticalCaptionOrientation.BottomToTop;
            this.led1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.led1.LedStyle = NationalInstruments.UI.LedStyle.Square;
            this.led1.Location = new System.Drawing.Point(2, 252);
            this.led1.Margin = new System.Windows.Forms.Padding(2);
            this.led1.Name = "led1";
            this.led1.OffColor = System.Drawing.Color.Gray;
            this.led1.OnColor = System.Drawing.Color.Magenta;
            this.led1.Size = new System.Drawing.Size(26, 46);
            this.led1.TabIndex = 14;
            // 
            // NoiseAllLeds
            // 
            this.NoiseAllLeds.BackgroundImageAlignment = NationalInstruments.UI.ImageAlignment.Center;
            this.NoiseAllLeds.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.NoiseAllLeds.Caption = "All";
            this.NoiseAllLeds.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.NoiseAllLeds.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NoiseAllLeds.CaptionVerticalOrientation = NationalInstruments.UI.VerticalCaptionOrientation.BottomToTop;
            this.NoiseAllLeds.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NoiseAllLeds.LedStyle = NationalInstruments.UI.LedStyle.Square;
            this.NoiseAllLeds.Location = new System.Drawing.Point(2, 302);
            this.NoiseAllLeds.Margin = new System.Windows.Forms.Padding(2);
            this.NoiseAllLeds.Name = "NoiseAllLeds";
            this.NoiseAllLeds.OffColor = System.Drawing.Color.Gray;
            this.NoiseAllLeds.OnColor = System.Drawing.Color.Magenta;
            this.NoiseAllLeds.Size = new System.Drawing.Size(26, 46);
            this.NoiseAllLeds.TabIndex = 18;
            // 
            // waveformGraph1
            // 
            this.waveformGraph1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.waveformGraph1.Location = new System.Drawing.Point(30, 0);
            this.waveformGraph1.Margin = new System.Windows.Forms.Padding(0);
            this.waveformGraph1.Name = "waveformGraph1";
            this.waveformGraph1.Size = new System.Drawing.Size(1139, 696);
            this.waveformGraph1.TabIndex = 2;
            this.waveformGraph1.UseColorGenerator = true;
            // 
            // xyCursor1
            // 
            this.xyCursor1.Plot = this.waveformPlot3;
            // 
            // xyCursor2
            // 
            this.xyCursor2.Plot = this.waveformPlot3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Location = new System.Drawing.Point(1172, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 10;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(49, 690);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // gauge10
            // 
            this.gauge10.Caption = "1-3";
            this.gauge10.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.gauge10.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gauge10.ForeColor = System.Drawing.SystemColors.Window;
            this.gauge10.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThickNeedle;
            this.gauge10.ImmediateUpdates = true;
            this.gauge10.Location = new System.Drawing.Point(2, 71);
            this.gauge10.Margin = new System.Windows.Forms.Padding(2);
            this.gauge10.Name = "gauge10";
            this.gauge10.PointerColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gauge10.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.gauge10.ScaleArc = new NationalInstruments.UI.Arc(90F, 360F);
            this.gauge10.Size = new System.Drawing.Size(45, 65);
            this.gauge10.TabIndex = 1;
            // 
            // gauge9
            // 
            this.gauge9.Caption = "4-5";
            this.gauge9.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.gauge9.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gauge9.ForeColor = System.Drawing.SystemColors.Window;
            this.gauge9.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThickNeedle;
            this.gauge9.ImmediateUpdates = true;
            this.gauge9.Location = new System.Drawing.Point(2, 623);
            this.gauge9.Margin = new System.Windows.Forms.Padding(2);
            this.gauge9.Name = "gauge9";
            this.gauge9.PointerColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gauge9.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.gauge9.ScaleArc = new NationalInstruments.UI.Arc(90F, 360F);
            this.gauge9.Size = new System.Drawing.Size(45, 65);
            this.gauge9.TabIndex = 9;
            // 
            // gauge8
            // 
            this.gauge8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.gauge8.Caption = "1-2";
            this.gauge8.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.gauge8.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gauge8.ForeColor = System.Drawing.SystemColors.Window;
            this.gauge8.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThickNeedle;
            this.gauge8.ImmediateUpdates = true;
            this.gauge8.Location = new System.Drawing.Point(2, 2);
            this.gauge8.Margin = new System.Windows.Forms.Padding(2);
            this.gauge8.Name = "gauge8";
            this.gauge8.PointerColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gauge8.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.gauge8.ScaleArc = new NationalInstruments.UI.Arc(90F, 360F);
            this.gauge8.Size = new System.Drawing.Size(45, 65);
            this.gauge8.TabIndex = 0;
            // 
            // gauge7
            // 
            this.gauge7.Caption = "1-4";
            this.gauge7.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.gauge7.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gauge7.ForeColor = System.Drawing.SystemColors.Window;
            this.gauge7.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThickNeedle;
            this.gauge7.ImmediateUpdates = true;
            this.gauge7.Location = new System.Drawing.Point(2, 140);
            this.gauge7.Margin = new System.Windows.Forms.Padding(2);
            this.gauge7.Name = "gauge7";
            this.gauge7.PointerColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gauge7.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.gauge7.ScaleArc = new NationalInstruments.UI.Arc(90F, 360F);
            this.gauge7.Size = new System.Drawing.Size(45, 65);
            this.gauge7.TabIndex = 2;
            // 
            // gauge6
            // 
            this.gauge6.Caption = "1-5";
            this.gauge6.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.gauge6.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gauge6.ForeColor = System.Drawing.SystemColors.Window;
            this.gauge6.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThickNeedle;
            this.gauge6.ImmediateUpdates = true;
            this.gauge6.Location = new System.Drawing.Point(2, 209);
            this.gauge6.Margin = new System.Windows.Forms.Padding(2);
            this.gauge6.Name = "gauge6";
            this.gauge6.PointerColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gauge6.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.gauge6.ScaleArc = new NationalInstruments.UI.Arc(90F, 360F);
            this.gauge6.Size = new System.Drawing.Size(45, 65);
            this.gauge6.TabIndex = 3;
            // 
            // gauge5
            // 
            this.gauge5.Caption = "2-3";
            this.gauge5.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.gauge5.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gauge5.ForeColor = System.Drawing.SystemColors.Window;
            this.gauge5.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThickNeedle;
            this.gauge5.ImmediateUpdates = true;
            this.gauge5.Location = new System.Drawing.Point(2, 278);
            this.gauge5.Margin = new System.Windows.Forms.Padding(2);
            this.gauge5.Name = "gauge5";
            this.gauge5.PointerColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gauge5.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.gauge5.ScaleArc = new NationalInstruments.UI.Arc(90F, 360F);
            this.gauge5.Size = new System.Drawing.Size(45, 65);
            this.gauge5.TabIndex = 4;
            // 
            // gauge4
            // 
            this.gauge4.Caption = "2-4";
            this.gauge4.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.gauge4.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gauge4.ForeColor = System.Drawing.SystemColors.Window;
            this.gauge4.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThickNeedle;
            this.gauge4.ImmediateUpdates = true;
            this.gauge4.Location = new System.Drawing.Point(2, 347);
            this.gauge4.Margin = new System.Windows.Forms.Padding(2);
            this.gauge4.Name = "gauge4";
            this.gauge4.PointerColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gauge4.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.gauge4.ScaleArc = new NationalInstruments.UI.Arc(90F, 360F);
            this.gauge4.Size = new System.Drawing.Size(45, 65);
            this.gauge4.TabIndex = 5;
            // 
            // gauge3
            // 
            this.gauge3.Caption = "2-5";
            this.gauge3.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.gauge3.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gauge3.ForeColor = System.Drawing.SystemColors.Window;
            this.gauge3.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThickNeedle;
            this.gauge3.ImmediateUpdates = true;
            this.gauge3.Location = new System.Drawing.Point(2, 416);
            this.gauge3.Margin = new System.Windows.Forms.Padding(2);
            this.gauge3.Name = "gauge3";
            this.gauge3.PointerColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gauge3.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.gauge3.ScaleArc = new NationalInstruments.UI.Arc(90F, 360F);
            this.gauge3.Size = new System.Drawing.Size(45, 65);
            this.gauge3.TabIndex = 6;
            // 
            // gauge2
            // 
            this.gauge2.Caption = "3-4";
            this.gauge2.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.gauge2.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gauge2.ForeColor = System.Drawing.SystemColors.Window;
            this.gauge2.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThickNeedle;
            this.gauge2.ImmediateUpdates = true;
            this.gauge2.Location = new System.Drawing.Point(2, 485);
            this.gauge2.Margin = new System.Windows.Forms.Padding(2);
            this.gauge2.Name = "gauge2";
            this.gauge2.PointerColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gauge2.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.gauge2.ScaleArc = new NationalInstruments.UI.Arc(90F, 360F);
            this.gauge2.Size = new System.Drawing.Size(45, 65);
            this.gauge2.TabIndex = 7;
            // 
            // gauge1
            // 
            this.gauge1.Caption = "3-5";
            this.gauge1.CaptionBackColor = System.Drawing.SystemColors.Window;
            this.gauge1.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gauge1.ForeColor = System.Drawing.SystemColors.Window;
            this.gauge1.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThickNeedle;
            this.gauge1.ImmediateUpdates = true;
            this.gauge1.Location = new System.Drawing.Point(2, 554);
            this.gauge1.Margin = new System.Windows.Forms.Padding(2);
            this.gauge1.Name = "gauge1";
            this.gauge1.PointerColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gauge1.Range = new NationalInstruments.UI.Range(0D, 360D);
            this.gauge1.ScaleArc = new NationalInstruments.UI.Arc(90F, 360F);
            this.gauge1.Size = new System.Drawing.Size(45, 65);
            this.gauge1.TabIndex = 8;
            // 
            // FormTableLayoutPanel
            // 
            this.FormTableLayoutPanel.AutoSize = true;
            this.FormTableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.FormTableLayoutPanel.ColumnCount = 1;
            this.FormTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.FormTableLayoutPanel.Controls.Add(this.StatusPanel, 0, 1);
            this.FormTableLayoutPanel.Controls.Add(this.FormTabControl, 0, 0);
            this.FormTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.FormTableLayoutPanel.Name = "FormTableLayoutPanel";
            this.FormTableLayoutPanel.RowCount = 2;
            this.FormTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.FormTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.FormTableLayoutPanel.Size = new System.Drawing.Size(1213, 725);
            this.FormTableLayoutPanel.TabIndex = 3;
            // 
            // StatusPanel
            // 
            this.StatusPanel.AutoSize = true;
            this.StatusPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.StatusPanel.BackColor = System.Drawing.SystemColors.Window;
            this.StatusPanel.Controls.Add(this.LanguageComboBox);
            this.StatusPanel.Controls.Add(this.RecordButton);
            this.StatusPanel.Controls.Add(this.StartStopButton);
            this.StatusPanel.Controls.Add(this.ConnectionLabel);
            this.StatusPanel.Controls.Add(this.ConnectionLed);
            this.StatusPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.StatusPanel.Location = new System.Drawing.Point(0, 699);
            this.StatusPanel.Margin = new System.Windows.Forms.Padding(0);
            this.StatusPanel.Name = "StatusPanel";
            this.StatusPanel.Size = new System.Drawing.Size(1213, 26);
            this.StatusPanel.TabIndex = 3;
            // 
            // LanguageComboBox
            // 
            this.LanguageComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LanguageComboBox.Items.AddRange(new object[] {
            "Азербайджанский",
            "Русский"});
            this.LanguageComboBox.Location = new System.Drawing.Point(906, 1);
            this.LanguageComboBox.Name = "LanguageComboBox";
            this.LanguageComboBox.Size = new System.Drawing.Size(116, 21);
            this.LanguageComboBox.TabIndex = 13;
            this.LanguageComboBox.Visible = false;
            //this.LanguageComboBox.SelectedIndexChanged += new System.EventHandler(this.LanguageComboBox_SelectedIndexChanged);
            // 
            // RecordButton
            // 
            this.RecordButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RecordButton.Location = new System.Drawing.Point(1031, 0);
            this.RecordButton.Name = "RecordButton";
            this.RecordButton.Size = new System.Drawing.Size(92, 26);
            this.RecordButton.TabIndex = 3;
            this.RecordButton.Text = "Запись";
            this.RecordButton.UseVisualStyleBackColor = true;
            this.RecordButton.Click += new System.EventHandler(this.RecordButton_Click);
            // 
            // StartStopButton
            // 
            this.StartStopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StartStopButton.Location = new System.Drawing.Point(1131, 0);
            this.StartStopButton.Name = "StartStopButton";
            this.StartStopButton.Size = new System.Drawing.Size(75, 26);
            this.StartStopButton.TabIndex = 2;
            this.StartStopButton.Text = "Старт/стоп";
            this.StartStopButton.UseVisualStyleBackColor = true;
            this.StartStopButton.Click += new System.EventHandler(this.StartStopButton_Click);
            // 
            // ConnectionLabel
            // 
            this.ConnectionLabel.AutoSize = true;
            this.ConnectionLabel.Location = new System.Drawing.Point(29, 6);
            this.ConnectionLabel.Name = "ConnectionLabel";
            this.ConnectionLabel.Size = new System.Drawing.Size(129, 13);
            this.ConnectionLabel.TabIndex = 1;
            this.ConnectionLabel.Text = "Подключение к серверу";
            // 
            // ConnectionLed
            // 
            this.ConnectionLed.CaptionBackColor = System.Drawing.Color.Transparent;
            this.ConnectionLed.LedStyle = NationalInstruments.UI.LedStyle.Square;
            this.ConnectionLed.Location = new System.Drawing.Point(0, 0);
            this.ConnectionLed.Margin = new System.Windows.Forms.Padding(0);
            this.ConnectionLed.Name = "ConnectionLed";
            this.ConnectionLed.OffColor = System.Drawing.Color.Red;
            this.ConnectionLed.Size = new System.Drawing.Size(26, 26);
            this.ConnectionLed.TabIndex = 0;
            // 
            // FormTabControl
            // 
            this.FormTabControl.Controls.Add(this.NoiseGeneratorTabPage);
            this.FormTabControl.Controls.Add(this.IntelligenceTabPage);
            this.FormTabControl.Controls.Add(this.CalibrationResultTab);
            this.FormTabControl.Controls.Add(this.CalibrationDiffTabPage);
            //this.FormTabControl.Controls.Add(this.tabPage1);
            this.FormTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormTabControl.Location = new System.Drawing.Point(0, 0);
            this.FormTabControl.Margin = new System.Windows.Forms.Padding(0);
            this.FormTabControl.Name = "FormTabControl";
            this.FormTabControl.SelectedIndex = 0;
            this.FormTabControl.Size = new System.Drawing.Size(1213, 699);
            this.FormTabControl.TabIndex = 2;
            this.FormTabControl.SelectedIndexChanged += new System.EventHandler(this.FormTabControl_SelectedIndexChanged);
            this.FormTabControl.TabIndexChanged += new System.EventHandler(this.FormTabControl_TabIndexChanged);
            // 
            // NoiseGeneratorTabPage
            // 
            this.NoiseGeneratorTabPage.Controls.Add(this.TechTabControl);
            this.NoiseGeneratorTabPage.Location = new System.Drawing.Point(4, 22);
            this.NoiseGeneratorTabPage.Name = "NoiseGeneratorTabPage";
            this.NoiseGeneratorTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.NoiseGeneratorTabPage.Size = new System.Drawing.Size(1205, 673);
            this.NoiseGeneratorTabPage.TabIndex = 2;
            this.NoiseGeneratorTabPage.Text = "Анализатор";
            this.NoiseGeneratorTabPage.UseVisualStyleBackColor = true;
            this.NoiseGeneratorTabPage.Enter += new System.EventHandler(this.NoiseGeneratorTabPage_Enter);
            // 
            // IntelligenceTabPage
            // 
            this.IntelligenceTabPage.Controls.Add(this.IntelligenceTabControl);
            this.IntelligenceTabPage.Location = new System.Drawing.Point(4, 22);
            this.IntelligenceTabPage.Name = "IntelligenceTabPage";
            this.IntelligenceTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.IntelligenceTabPage.Size = new System.Drawing.Size(1205, 673);
            this.IntelligenceTabPage.TabIndex = 0;
            this.IntelligenceTabPage.Text = "Разведка";
            this.IntelligenceTabPage.UseVisualStyleBackColor = true;
            // 
            // CalibrationResultTab
            // 
            this.CalibrationResultTab.Controls.Add(this.CalibrationTabControl);
            this.CalibrationResultTab.Location = new System.Drawing.Point(4, 22);
            this.CalibrationResultTab.Name = "CalibrationResultTab";
            this.CalibrationResultTab.Padding = new System.Windows.Forms.Padding(3);
            this.CalibrationResultTab.Size = new System.Drawing.Size(1205, 673);
            this.CalibrationResultTab.TabIndex = 3;
            this.CalibrationResultTab.Text = "Калибровка";
            this.CalibrationResultTab.UseVisualStyleBackColor = true;
            // 
            // CalibrationDiffTabPage
            // 
            this.CalibrationDiffTabPage.Controls.Add(this.CalibrationDiffControl);
            this.CalibrationDiffTabPage.Location = new System.Drawing.Point(4, 22);
            this.CalibrationDiffTabPage.Name = "CalibrationDiffTabPage";
            this.CalibrationDiffTabPage.Size = new System.Drawing.Size(1205, 673);
            this.CalibrationDiffTabPage.TabIndex = 1;
            this.CalibrationDiffTabPage.Text = "Разность калибровок";
            this.CalibrationDiffTabPage.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            //this.tabPage1.Controls.Add(this.CorrectionTableControl);
            //this.tabPage1.Location = new System.Drawing.Point(4, 22);
            //this.tabPage1.Name = "tabPage1";
            //this.tabPage1.Size = new System.Drawing.Size(1205, 673);
            //this.tabPage1.TabIndex = 4;
            //this.tabPage1.Text = "Таблица корректировки";
            //this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // openFileDialog
            // 
            this.openFileDialog.Multiselect = true;
            // 
            // TechTabControl
            // 
            this.TechTabControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TechTabControl.AveragingCount = 1;
            this.TechTabControl.Client = null;
            this.TechTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TechTabControl.Location = new System.Drawing.Point(3, 3);
            this.TechTabControl.Margin = new System.Windows.Forms.Padding(4);
            this.TechTabControl.Name = "TechTabControl";
            this.TechTabControl.Size = new System.Drawing.Size(1199, 667);
            this.TechTabControl.TabIndex = 0;
            // 
            // IntelligenceTabControl
            // 
            this.IntelligenceTabControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.IntelligenceTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IntelligenceTabControl.EndRdfFrequencyKhz = 55000;
            this.IntelligenceTabControl.Location = new System.Drawing.Point(3, 3);
            this.IntelligenceTabControl.Margin = new System.Windows.Forms.Padding(0);
            this.IntelligenceTabControl.Name = "IntelligenceTabControl";
            this.IntelligenceTabControl.Size = new System.Drawing.Size(1199, 667);
            this.IntelligenceTabControl.StartRdfFrequencyKhz = 25000;
            this.IntelligenceTabControl.TabIndex = 0;
            this.IntelligenceTabControl.Threshold = -80;
            // 
            // CalibrationTabControl
            // 
            this.CalibrationTabControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CalibrationTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CalibrationTabControl.Location = new System.Drawing.Point(3, 3);
            this.CalibrationTabControl.Margin = new System.Windows.Forms.Padding(0);
            this.CalibrationTabControl.Name = "CalibrationTabControl";
            this.CalibrationTabControl.Size = new System.Drawing.Size(1199, 667);
            this.CalibrationTabControl.TabIndex = 0;
            // 
            // CalibrationDiffControl
            // 
            this.CalibrationDiffControl.AutoScroll = true;
            this.CalibrationDiffControl.AutoScrollMinSize = new System.Drawing.Size(500, 500);
            this.CalibrationDiffControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CalibrationDiffControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CalibrationDiffControl.Location = new System.Drawing.Point(0, 0);
            this.CalibrationDiffControl.Margin = new System.Windows.Forms.Padding(0);
            this.CalibrationDiffControl.Name = "CalibrationDiffControl";
            this.CalibrationDiffControl.Size = new System.Drawing.Size(1205, 673);
            this.CalibrationDiffControl.TabIndex = 0;
            // 
            // CorrectionTableControl
            // 
            //this.CorrectionTableControl.Dock = System.Windows.Forms.DockStyle.Fill;
            //this.CorrectionTableControl.Location = new System.Drawing.Point(0, 0);
            //this.CorrectionTableControl.Name = "CorrectionTableControl";
            //this.CorrectionTableControl.Size = new System.Drawing.Size(1205, 673);
            //this.CorrectionTableControl.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1213, 725);
            this.Controls.Add(this.FormTableLayoutPanel);
            this.Name = "MainForm";
            this.Text = "Анализатор";
            this.Load += new System.EventHandler(this.MainForm_Load);
            //this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.led7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.led1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoiseAllLeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveformGraph1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xyCursor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xyCursor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gauge1)).EndInit();
            this.FormTableLayoutPanel.ResumeLayout(false);
            this.FormTableLayoutPanel.PerformLayout();
            this.StatusPanel.ResumeLayout(false);
            this.StatusPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConnectionLed)).EndInit();
            this.FormTabControl.ResumeLayout(false);
            this.NoiseGeneratorTabPage.ResumeLayout(false);
            this.IntelligenceTabPage.ResumeLayout(false);
            this.CalibrationResultTab.ResumeLayout(false);
            this.CalibrationDiffTabPage.ResumeLayout(false);
            //this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private NationalInstruments.UI.WindowsForms.Led led7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private NationalInstruments.UI.WindowsForms.Led led1;
        private NationalInstruments.UI.WindowsForms.Led NoiseAllLeds;
        private NationalInstruments.UI.WindowsForms.WaveformGraph waveformGraph1;
        private NationalInstruments.UI.YAxis yAxis3;
        private NationalInstruments.UI.XAxis xAxis2;
        private NationalInstruments.UI.XYCursor xyCursor1;
        private NationalInstruments.UI.WaveformPlot waveformPlot3;
        private NationalInstruments.UI.XYCursor xyCursor2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private NationalInstruments.UI.WindowsForms.Gauge gauge10;
        private NationalInstruments.UI.WindowsForms.Gauge gauge9;
        private NationalInstruments.UI.WindowsForms.Gauge gauge8;
        private NationalInstruments.UI.WindowsForms.Gauge gauge7;
        private NationalInstruments.UI.WindowsForms.Gauge gauge6;
        private NationalInstruments.UI.WindowsForms.Gauge gauge5;
        private NationalInstruments.UI.WindowsForms.Gauge gauge4;
        private NationalInstruments.UI.WindowsForms.Gauge gauge3;
        private NationalInstruments.UI.WindowsForms.Gauge gauge2;
        private NationalInstruments.UI.WindowsForms.Gauge gauge1;
        private System.Windows.Forms.TableLayoutPanel FormTableLayoutPanel;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Panel StatusPanel;
        private System.Windows.Forms.Button RecordButton;
        private System.Windows.Forms.Button StartStopButton;
        private System.Windows.Forms.Label ConnectionLabel;
        private NationalInstruments.UI.WindowsForms.Led ConnectionLed;
        private System.Windows.Forms.TabControl FormTabControl;
        internal System.Windows.Forms.TabPage NoiseGeneratorTabPage;
        private TechAppControls.TechTabControl TechTabControl;
        private System.Windows.Forms.TabPage IntelligenceTabPage;
        private System.Windows.Forms.TabPage CalibrationDiffTabPage;
        private System.Windows.Forms.TabPage CalibrationResultTab;
        private TechAppControls.IntelligenceTabControl IntelligenceTabControl;
        private TechAppControls.CalibrationTabControl CalibrationTabControl;
        private TechAppControls.CalibrationDifferenceControl CalibrationDiffControl;
        private System.Windows.Forms.ComboBox LanguageComboBox;
        //private System.Windows.Forms.TabPage tabPage1;
        //private TechAppControls.CorrectionTableControl CorrectionTableControl;
    }
}

