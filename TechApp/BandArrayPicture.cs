﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TechApp
{
    public class BandArrayPicture
    {
        public PictureBox Picture { get; private set; }

        public int BandCount { get; private set; }

        public IReadOnlyList<Color> Colors
        {
            get { return _colors; }
        }

        public int ActiveBandIndex
        {
            get { return _activeBandIndex; }
            set
            {
                if (_activeBandIndex == value)
                {
                    return;
                }
                _activeBandIndex = value;
                Invalidate();
            }
        }

        public event EventHandler<int> PictureClickEvent;

        private readonly Brush[] _brushes;
        private readonly Pen _blackPen;
        private readonly Pen _activePen;

        private readonly Color[] _colors;

        private readonly int _rectangleHeight;
        private int _activeBandIndex;

        public BandArrayPicture(PictureBox picture, int bandCount)
        {
            Picture = picture;
            BandCount = bandCount;
            _colors = new Color[BandCount];
            _brushes = new Brush[BandCount];
            for (var i = 0; i < BandCount; ++i)
            {
                SetColor(i, Picture.BackColor);
            }
            _rectangleHeight = Picture.Height - 2;
            _blackPen = new Pen(Color.Black);
            _activePen = new Pen(Color.DodgerBlue, 2f);

            picture.Paint += OnPaint;
            picture.Click += OnClick;
        }

        public void ClearAllRectangles()
        {
            for (var i = 0; i < BandCount; ++i)
            {
                SetColor(i, Color.Transparent);
            }
            Invalidate();
        }

        public void SetColor(int index, Color color)
        {
            _colors[index] = color;
            _brushes[index] = new SolidBrush(color);
        }

        void OnClick(object sender, EventArgs e)
        {
            var x = Picture.PointToClient(Cursor.Position).X;
            var size = 1f * (Picture.Width - 1) / BandCount;
            var index = (int) (x / size);
            ActiveBandIndex = index;
            OnPictureClickEvent(index);
        }

        public void Invalidate()
        {
            Picture.Invalidate();
        }

        private void OnPaint(object sender, PaintEventArgs e)
        {
            for (var i = 0; i < BandCount; ++i)
            {
                DrawRectangle(e.Graphics, i);
            }
        }

        private void DrawRectangle(Graphics graphics, int index)
        {
            var left = (int) (index * 1f * (Picture.Width - 1) / BandCount);
            var right = (int) ((index + 1) * 1f * (Picture.Width - 1) / BandCount);
            graphics.FillRectangle(_brushes[index], left, 0, right - left - 2, _rectangleHeight);

            var pen = index == ActiveBandIndex ? _activePen : _blackPen;
            var shift = pen.Width / 2;
            graphics.DrawRectangle(pen, left + shift, shift, right - left - 2 - shift, _rectangleHeight - shift);
        }

        protected void OnPictureClickEvent(int index)
        {
            var handler = PictureClickEvent;
            if (handler != null)
            {
                handler(this, index);
            }
        }
    }
}
