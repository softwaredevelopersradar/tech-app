﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace TechApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.ThreadException += UnhandledUIException;
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += UnhandledException;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        private static void UnhandledUIException(object sender, ThreadExceptionEventArgs e)
        {
            File.AppendAllText("log.txt", e.Exception.Message + "\n" + e.Exception.StackTrace);
        }

        private static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = (Exception) e.ExceptionObject;
            File.AppendAllText("log.txt", exception.Message + "\n" + exception.StackTrace);
        }
    }
}
