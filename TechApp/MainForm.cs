﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Correlator;
using DspDataModel.Server;
using NetLib;
using Protocols;
using TechAppControls;


namespace TechApp
{
    public partial class MainForm : Form
    {
        public DspClient.DspClient Client;

        public bool IsRecording { get; private set; }

        public bool IsSomeWorkRunning { get { return _tabs.Any(_tab => _tab.IsWorking); } }

        public event EventHandler StopAllWorkEvent; 

        private bool _isSomeWorkRunning;

        private ITab[] _tabs;

        private ITab _currentTab;

        public MainForm()
        {
            InitializeComponent();

            IsRecording = false;
            _tabs = new ITab[]
            {
                TechTabControl, IntelligenceTabControl
            };
            _currentTab = TechTabControl;
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            await InitializeClient();
            TechTabControl.Client = Client;
            try
            {
                TechTabControl.RadiopathTable = new RadioPathTable(Config.Instance.RadioPathTableFilename);
            }
            catch (Exception)
            {
                TechTabControl.RadiopathTable = new RadioPathTable();
            }
            
            TechTabControl.StartTabWork();

            IntelligenceTabControl.Client = Client;
        }

        private async Task InitializeClient()
        {
            Client = new DspClient.DspClient();
            Client.Client.StatusChangedEvent += OnClientStatusChanged;

            await Client.Connect(Config.Instance.ServerHost, Config.Instance.ServerPort);
        }

        void OnClientStatusChanged(object sender, EventArgs e)
        {
            ConnectionLed.Value = Client.Client.Status == ClientStatus.IsWorking;
        }

        private void NoiseGeneratorTabPage_Enter(object sender, EventArgs e)
        {
        }

        private void StartStopButton_Click(object sender, EventArgs e)
        {
            if (_currentTab == null)
            {
                return;
            }
            if (IsSomeWorkRunning)
            {
                _currentTab.StopTabWork();
            }
            else
            {
                _currentTab.StartTabWork();
            }
        }

        private async void RecordButton_Click(object sender, EventArgs e)
        {
            if (IsRecording)
            {
                RecordButton.Text = "Запись";
                await Client.StopRadioRecording();
                IsRecording = false;
            }
            else
            {
                if (await Client.StartRadioRecording() == RequestResult.Ok)
                {
                    RecordButton.Text = "Стоп запись";
                    IsRecording = true;
                }
            }
        }

        private void FormTabControl_TabIndexChanged(object sender, EventArgs e)
        {
        }

        private void FormTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            StopTabWork();
            switch (FormTabControl.SelectedIndex)
            {
                case 0:
                    _currentTab = TechTabControl;
                    break;
                case 1:
                    _currentTab = IntelligenceTabControl;
                    break;
            }
            _currentTab.StartTabWork();
        }

        private void StopTabWork()
        {
            foreach (var tab in _tabs)
            {
                tab.StopTabWork();
            }
        }
    }
}
